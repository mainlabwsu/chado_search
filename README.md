[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.1409187.svg)](https://doi.org/10.5281/zenodo.1409187)

# Mainab Chado Search
Mainlab Chado Search is a module that enables advanced search function for biological
data stored in a Tripal/Chado database (see http://gmod.org/wiki/Chado and
http://tripal.info). By default, a set of search interfaces are provided, such as 'Gene Search'
for searching genes and/or transcripts, 'Marker Search' for searching genetic markers, and
'Sequence Search' for searching any sequences stored in the Chado feature table. Searches
for other data types, such as QTL, Map, Trait, Stock, Organism are also provided but may
require modification to the materialized view (MView) to adjust for site-specific data storage.

The Mainlab Chado Search module has a set of drush commands to manage the MViews, It also works
with the Tripal MView system. Using MViews not only improves the search performance, but also
allows the administrator to restrict data by modifying the MView definition. This design also
allows site developers to adopt this module when data in Chado is not stored in the exact same
way (See Customization section). Data collecton templates and loader (Mainlab Chado Loader,
see https://gitlab.com/mainlabwsu/mainlab_chado_loader) are also
available as a separate module.

The Mainlab Chado Search is created by Main Bioinformatics Lab (Main Lab) at
Washington State University. Information about the Main Lab can be found at:
https://www.bioinfo.wsu.edu

## Requirement
 - Drupal 10.x or 11.x
 - PostgreSQL (Drupal in 'public' schema whereas Chado in 'chado' schema)

## Version
4.3.0

## Download
The Mainlab Chado Search module can be downloaded from GitLab:

https://gitlab.com/mainlabwsu/chado_search

## Installation
After downloading the module, extract it into your site's module directory
(e.g. /modules) then follow the instructions below:

1. Create a 'settings.conf' file in the 'chado_search/file' directory. For example,

    ```
    cd chado_search/file
    cp default.settings.txt settings.conf
    ```

    Note1: if you make changes to the 'settings.conf' after the module is enabled, you'll need
    to run the following drush command to make it effective:

    ```
    drush csreload
    ```

    Note2: Mainlab Chado Search currently provides example setting files listed below. You
    can find more information about some of these setting files in the 'Example Setting Files'
    section in this document.
      - default.settings.txt
      - cottongen.settings.txt
      - gdr.settings.txt
      - citrus.settings.txt
      - legume.settings.txt
      - vaccinium.settings.txt


2. Enable the module by using the Drupal administrative interface:

      Go to: 'Extend', check Mainlab Chado Search (under the Mainlab category) and save

    or by using the 'drush' command:

    ```
    drush pm-enable chado_search
    ```

    This will create all search interfaces listed in the 'settings.conf' and all MViews
    required for the search to function.

3. Use the following drush commands to list and populate the MViews. Note. Using Tripal MView
   system to manage and/or populate MViews is also supported. Chado Search will try Tripal MView
   API first and fall back to the drush-based approach if the Tripal MView API is not available.

    ```
      drush cslmvs                                     # List all Mviews from settings.conf
      drush cspmv --name=<chado_search_mview_name>     # Populate specified MView
      drush cspall                                     # Populate all Mviews (may take a while)

    ```
4. Visit the search page in your browse by going to the path set in your 'settings.conf' file.
    For example, the default 'Sequence Search' can be accessed by visiting:
    http://your.site/find/features

    Alternatively,  you can visit http://your.site/admin/config/user-interface/chado_search to get
    a full list of searches on your site.

    Note: you can change the path to anything you like but remember to run the 'drush csreload'
    command to make it effective:

## Administration
 - Enabling/Disabling a search:

   Go to: Configuration > Chado Search and click on either 'Disable' or 'Enable' for a search
   i.e. http://your.site/admin/config/user-interface/chado_search

     Note: Make sure your web server has write permission to the settings.conf so you can
     turn a search on or off using the web interface.

 - Restrict access by roles:

   Go to: Configuration > Chado Search and click on 'All Granted' to switch on access restriction
   (i.e.  'Restricted by Roles') for a search. Then, set the roles that are allowed for access in
   the 'Restrict Access' section on the same page. You can optionally change the access denied message.

 - Adding a header message in all download .CSV files:

   On the Chado Search configuration page (Configuration > Chado Search), you can add an optinal
   header message (e.g. Disclaimer/License) to all donwload .CSV files.

 - Adding/Deleting data for a search:

   After adding or deleting data to/from Chado, you'll need to update related MViews to reflect the
   change. You can manage the MViews using the following drush commands:

    1. Dropping an MView:
    ```
      drush csdmv --name=<chado_search_mview_name>

    ```
    2. Recreating dropped MViews based on the setting.conf:
    ```
      drush csreload

    ```
    3. Populating an MView:
    ```
      drush cspmv --name=<chado_search_mview_name>

    ```

    Alternatively, you can change the MViews using the Tripal MView interface. You can
    identify the MViews created by Mainlab Chado Search by looking for the prefix 'chado_search_'.
    If you make changes to a default MView that comes with Mainlab Chado Search and later
    decide to get it back, delete the MView and reload the 'settings.conf' with the
    'drush csreload' command to recreate it.

    Note: you'll still need to populate the MView using Tripal's administrative interface after
    bringing it back.

## Performance

- Indexes: 

  By default, Chado Search does not create indexes for its materialized views. While indexes improve
  the search performance, it also use more space and may slow down the data insertion. Therefore,
  Chado Search leaves this decision to the site administrators. Nevertheless, a couple of drush commands 
  are provided by the Chado Search to help with the index creation/deletion.
  
  For example, 
  
  To create indexes on the name and uniquenname columns (separated using a colon in the 
  command) of the MView 'chado_search_gene_search':
  
  ```
  drush csidxc --table=chado_search_gene_search --columns=name:uniquename
  ```
  
  To list all indexes of an MView:
  
  ```
  drush csidxl --table=chado_search_gene_search
  ----- 
  1. cs_chado_search_gene_search_name => CREATE INDEX cs_chado_search_gene_search_name ON chado.chado_search_gene_search USING btree (name)
  2. cs_chado_search_gene_search_uniquename => CREATE INDEX cs_chado_search_gene_search_uniquename ON chado.chado_search_gene_search USING btree (uniquename)
  ```
  
  To drop a specific MView, specify the index name showing in the MView index list:
  
  ```
  drush csidxd --index=cs_chado_search_marker_search_marker_name_marker_uniquename
  ```
  
  To drop all indexes for an MView:
  
  ```
  drush csidxda --table=chado_search_gene_search
  ```

- Table Partitioning
  
  If your PostgreSQL version is greater than 10, Chado Search also comes with a few drush commands
  to support the table partitioning which might improve the performance:
  
  To convert an existing MView into a partitioned table, specify the MView name, a column with 
  numeric value (preferably an id column), and the number of partitions:
  
  ```
  drush csmvpart --name=chado_search_gene_search --column=organism_id --num_parts=5
  ```
  
  To remove all partitioning tables for an MView
  
  ```
  drush cspartmvdrop --name=chado_search_gene_search
  ```
  
  After droping the partitioning table, you'll need to recreate the MView by running the drush command:
  
  ```
  drush csreload
  ```

## Customization
You can customize the search for your site by modifying the 'settings.conf' file and using
the Tripal MView interface.

For developers, you can also create your own search by copying/modifying, or creating
the search interface php and/or the MView definition php files. See details below in
the Create New Search section.

## Create New Search (for Developers)
For site developers, Mainlab Chado Search provides a set of APIs that'll be useful for
developing new search interfaces. An example (i.e. Node Search) below shows minimal steps
required to create a new search from scratch. A list of available form widgets can be found
in ChadoSearchAPI.md.

1. Create a stanza in the 'settings.conf'. For example, a basic stanza looks like:
  ```
  [Node Search]
  id=node_search
  path=find/node
  file=includes/search/example/node_search.php
  enabled=1
  ```
  - Note1: See 'default.settings.txt' for additional information about the configurable options.
  - Note2: Use of MView is recommended but not required.

2. Create the search interface php file 'includes/search/example/node_search.php' with the
following content:
  ```
  <?php

  use Drupal\chado_search\Core\Set;
  use Drupal\chado_search\Core\Sql;

  /*************************************************************
   * hook_form()
   */
  function chado_search_node_search_form ($form) {
    $form->addTextFilter(
        Set::textFilter()
        ->id('title')
        ->title('Title')
    );
    $form->addSubmit();
    return $form;
  }

  /*************************************************************
   * hook_form_submit()
   */
   function chado_search_node_search_form_submit ($form, &$form_state) {
    $sql = "SELECT nid, title FROM node";
    $where [0] = Sql::textFilter('title', $form_state, 'title');
    Set::result()
    ->sql($sql)
    ->where($where)
    ->execute($form, $form_state);
  }
  ```

3. Make the 'settings.conf' effective:
  ```
  drush csreload
  ```
4. Access the search page by visiting: http://your.site/find/node

## Upgrade user-defined search from Drupal 7 to Drupal 9/10 (for Developers)
  1. The API function chado_search_query in Drupal 7 will search the 'chado' schema by default which is now deprecated in Drupal 9/10. 
      If you used the API function 'chado_search_query', you'll have to change your code to accomodate the new passing arguments:
      - Drupal 7: 
      ```
        chado_search_query ($sql, $args = array(), $schema = 'chado', $append_to_path = TRUE)
      ``` 
      - Drupal 9/10:
      ```  
        chado_search_query ($sql, $args = array(), $search_path = NULL)
      ```
      
  2. Name space change for Sql and Set classes:
      - Drupal 7:
      ``` 
        use ChadoSearch\Set;
        use ChadoSearch\Sql;
      ```
      - Drupal 9/10:
      ```
        use Drupal\chado_search\Core\Set;
        use Drupal\chado_search\Core\Sql;
      ```
        
  3. Drupal Form API changes:
      - Drupal 7:
      ```
        $locus1 = $form_state['values']['between_marker_locus1'];
        form_set_error('', t('Locus1 is required.'));
      ```
      - Drupal 9/10:
      ```
        $locus1 = $form_state->getValue('between_marker_locus1');
        $form_state->setErrorByName('', 'Locus1 is required.');
      ```

  4. In Drupal 9/10, you need to include [] in the query now if you're passing array as an query argument.
     - Drupal 7:
     ```
       $sql = "SELECT distinct landmark FROM {chado_search_gene_search} WHERE analysis IN (:analysis) ORDER BY landmark";
       return chado_search_bind_dynamic_select(array(':analysis' => $val), 'landmark', $sql);
     ```
     - Drupal 9:
     ```
       $sql = "SELECT distinct landmark FROM {chado_search_gene_search} WHERE analysis IN (:analysis[]) ORDER BY landmark";
       return chado_search_bind_dynamic_select(array(':analysis[]' => $val), 'landmark', $sql);
     ```
     
  5. MView create functions now return an array of MView information for Drupal 9/Drupal 10 instead of directly creating the MView by using the 'tripal_add_mview()' function:
     - Drupal 7:
     ```
       tripal_add_mview($view_name, 'chado_search', $schema, $sql, '', FALSE);
     ```
     - Drupal 9/10:
     ```
       return array(array('view_name' => $view_name, 'schema' => $schema, 'sql' => $sql));
     ```
  
  6. SQL statements (e.g. SQL in MView definition) including semicolon ';' is not allowed in Drupal 9/10:
      - Drupal 7:
      ```
        string_agg(distinct name, ';') AS value
      ```
      - Drupal 9/10:
      ```
        string_agg(distinct name, '. ') AS value
      ```
  
## Example Setting Files
  1. default.settings.txt
      this file contains three search interfaces:
     
       * Sequence Search
       * Marker Search
       * Gene Search

  2. cottongen.settings.txt

      this file contains the following search interfaces (with URL as live example):
       * [Sequence Search]
           (https://www.cottongen.org/find/features)
  
       * [Search for Mapped Markers]
           (https://www.cottongen.org/find/mapped_markers)
  
       * [Advanced Marker Search]
           (https://www.cottongen.org/find/markers)
  
       * [Marker Source Information]
           (https://www.cottongen.org/find/marker/source)
  
       * [Search Mapped Sequence by Map Name]
           (https://www.cottongen.org/find/mapped_sequence/map)
  
       * [Search Mapped Sequence by Chromosome Number]
           (https://www.cottongen.org/find/mapped_sequence/chromosome)
  
       * [Search Mapped Sequence by Genome Group]
           (https://www.cottongen.org/find/mapped_sequence/genome)
  
       * [Search Markers on Nearby Marker Loci]
           (https://www.cottongen.org/find/nearby_markers)
  
       * [Search Markers on Nearby QTL]
           (https://www.cottongen.org/find/qtl_nearby_markers)
  
       * [Trait Evaluation Search (Qualitative Traits)]
           (https://www.cottongen.org/find/qualitative_traits)
  
       * [Trait Evaluation Search (Quantitative Traits)]
           (https://www.cottongen.org/find/quantitative_traits)
  
       * [Germplasm Search]
           (https://www.cottongen.org/find/germplasm)
  
       * [Germplasm Search (by Pedigree)]
           (https://www.cottongen.org/find/germplasm/pedigree)
  
       * [Germplasm Search (by Country)]
           (https://www.cottongen.org/find/germplasm/country)
  
       * [Germplasm Search (by Collection)]
           (https://www.cottongen.org/find/germplasm/collection)
  
       * [Germplasm Search (by Image)]
           (https://www.cottongen.org/find/germplasm/image)
  
       * [Gene Search]
           (https://www.cottongen.org/find/genes)
  
       * [QTL Search]
           (https://www.cottongen.org/find/qtl)
  
       * [ND Geolocation]
           (https://www.cottongen.org/find/nd_geolocation/list)
  
       * [Map Data Summary]package
           (https://www.cottongen.org/find/featuremap/list)
  
       * [Species Summary]
           (https://www.cottongen.org/find/species/list)

  3. gdr.settings.txt

      this file contains the following search interfaces (with URL as live example):
        * [Gene Search]
            (https://www.rosaceae.org/search/genes)
    
        * [Sequence Search]
            (https://www.rosaceae.org/search/features)
    
        * [Marker Search]
            (https://www.rosaceae.org/search/markers)
    
        * [Search Markers on Nearby Loci]
            (https://www.rosaceae.org/search/nearby_markers)
    
        * [Germplasm Image Search]
            (https://www.rosaceae.org/search/germplasm/image)
    
        * [Haplotype Block Search]
            (https://www.rosaceae.org/search/haplotype_blocks)
    
        * [QTL Search]
            (https://www.rosaceae.org/search/qtl)
    
        * [Search Maps]
            (https://www.rosaceae.org/search/featuremap)
    
        * [Species Summary]
            (https://www.rosaceae.org/search/species)
    
        * [SSR Genotype Search]
            (https://www.rosaceae.org/search/ssr_genotype)
    
        * [SNP Genotype Search]
            (https://www.rosaceae.org/search/snp_genotype)

  4. legume.settings.txt

      this file contains the following search interfaces (with URL as live example):

        * [Sequence Search]
            (https://www.coolseasonfoodlegume.org/find/features)
    
        * [Marker Search]
            (https://www.coolseasonfoodlegume.org/find/markers)
    
        * [Gene and Transcript Search]
            (https://www.coolseasonfoodlegume.org/find/genes)
    
        * [Germplasm Search]
            (https://www.coolseasonfoodlegume.org/find/germplasms)
    
        * [QTL Search]
            (https://www.coolseasonfoodlegume.org/find/qtl)

## Problems/Suggestions
Mainlab Chado Search module is still under active development. For questions or bug
report, please contact the developers at the Main Bioinformatics Lab by emailing to:
dev@bioinfo.wsu.edu

## Citation
If you use Mainlab Chado Search, please cite:

[Jung S, Lee T, Cheng CH, Ficklin S, Yu J, Humann J, Main D. 2017. Extension modules for storage, visualization and querying of genomic, genetic and breeding data in Tripal databases. Database. 10.1093/database/bax092](https://academic.oup.com/database/article/doi/10.1093/database/bax092/4718480)
