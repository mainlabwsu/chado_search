(function ($) {
  
    $(document)
    .ajaxStart(function() {
      $('.chado_search-waiting-overlay').show();
    })
    .ajaxComplete(function() {
      if ($('.chado_search-waiting-box-message').css('display') != 'block') {
        $('.chado_search-waiting-overlay').hide();
      }
    });
    
  // Toggle fieldset
  function chado_search_fieldset_toggle(object) {
    var content = '#' + $(object).parent().attr('id') + '-content';
    var status = '#' + $(object).parent().attr('id') + '-status-hidden';
    if ($(content).is(":visible")) {
      $(content).hide(300);
      $(object).parent().css('border-bottom', '0px');
      $(object).parent().css('border-left', '0px');
      $(object).parent().css('border-right', '0px');
      $(object).parent().css('padding-top', '0px');
      $(object).parent().css('padding-bottom', '0px');
      $(status).val('hide');
    } else {
      $(object).parent().css('border-bottom', '1px solid #CCCCCC');
      $(object).parent().css('border-left', '1px solid #CCCCCC');
      $(object).parent().css('border-right', '1px solid #CCCCCC');
      $(object).parent().css('padding-top', '');
      $(object).parent().css('padding-bottom', '');
      $(content).show(300);
      $(status).val('show');
    }
  }
  window.chado_search_fieldset_toggle = chado_search_fieldset_toggle;
  
})(jQuery);