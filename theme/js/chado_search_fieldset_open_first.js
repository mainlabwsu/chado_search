(function ($) {
  
  function chado_search_fieldset_ready() {
    $('.chado_search-fieldset').each(function (index, object) {
      var id = $(object).attr('id');
      var content = '#' + id + '-content';
      if (index == 0) {
        $(content).show();
        $(content).parent().css('border-bottom', '');
        $(content).parent().css('border-left', '');
        $(content).parent().css('border-right', '');
        $(content).parent().css('padding-top', '');
        $(content).parent().css('padding-bottom', '');
      }
      else {
        $(content).hide();
        $(content).parent().css('border-bottom', '0px');
        $(content).parent().css('border-left', '0px');
        $(content).parent().css('border-right', '0px');
        $(content).parent().css('padding-top', '0px');
        $(content).parent().css('padding-bottom', '0px');
      }
    });
    
  }
    
  $(document).ready(function () {
    
    // Open the first fieldset and close the others
   chado_search_fieldset_ready();
    
  });

})(jQuery);