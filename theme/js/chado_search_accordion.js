(function ($) {

  /* When a fieldset is clicked */
  function chado_search_fieldset_accordion (object) {
    var id = $(object).parent().attr('id');
    var content = '#' + id + '-content';
    if ($(content).is(":visible")) {
      $(content).hide();
      $(content).fadeIn(300);
    }
    else {
      // Hide all fieldsets
      $('.chado_search-fieldset').each(function (index, element) {
        var fsets = '#' + $(element).attr('id') + '-content';
        if ($(fsets).is(':visible')) {
          $(fsets).slideUp(500);
        }
        else {
          $(fsets).hide();
        }
        $(fsets).parent().css('border-bottom', '0px');
        $(fsets).parent().css('border-left', '0px');
        $(fsets).parent().css('border-right', '0px');
        $(fsets).parent().css('padding-top', '0px');
        $(fsets).parent().css('padding-bottom', '0px');
      });
      // Show only the clicked fieldset      
      $(content).parent().css('border-bottom', '');
      $(content).parent().css('border-left', '');
      $(content).parent().css('border-right', '');
      $(content).parent().css('padding-top', '');
      $(content).parent().css('padding-bottom', '');
      $(content).show(500);
    }
    
  }
  
  function chado_search_fieldset_ready() {
    $('.chado_search-fieldset').each(function (index, object) {
      var id = $(object).attr('id');
      var content = '#' + id + '-content';
      if (index == 0) {
        $(content).show();
        $(content).parent().css('border-bottom', '');
        $(content).parent().css('border-left', '');
        $(content).parent().css('border-right', '');
        $(content).parent().css('padding-top', '');
        $(content).parent().css('padding-bottom', '');
      }
      else {
        $(content).hide();
        $(content).parent().css('border-bottom', '0px');
        $(content).parent().css('border-left', '0px');
        $(content).parent().css('border-right', '0px');
        $(content).parent().css('padding-top', '0px');
        $(content).parent().css('padding-bottom', '0px');
      }
    });
    
  }
    
  $(document).ready(function () {
    
    // Open the first fieldset and close the others
   chado_search_fieldset_ready();
    
    // Disable fieldset toggle. Use accordion instead
    window.chado_search_fieldset_toggle = chado_search_fieldset_accordion;    
  });

})(jQuery);