<?php

use Drupal\chado_search\Core\Set;
use Drupal\chado_search\Core\Sql;

/*************************************************************
 * Search form, form validation, and submit function
 */
// Search form
function chado_search_haplotype_block_search_form ($form) {
  $icon = '/' . \Drupal::service('extension.list.module')->getPath('chado_search') . '/theme/images/question.gif';
  $form->addSelectFilter(
      Set::selectFilter()
      ->id('project')
      ->title('Dataset  <a href="/search/project/summary?type=genotype&sub_type=haplotype"><img src="' . $icon . '"></a>')
      ->column('project')
      ->table('chado_search_haplotype_block_search')
      ->cache(TRUE)
      ->labelWidth(140)
      ->newLine()
      );
  $form->addSelectFilter(
      Set::selectFilter()
      ->id('organism')
      ->title('Species')
      ->column('organism')
      ->table('chado_search_haplotype_block_location')
      ->multiple(TRUE)
      ->labelWidth(140)
      ->newLine()
      );
  $form->addSelectFilter(
      Set::selectFilter()
      ->id('stock')
      ->title('Germplasm Name')
      ->column('stock')
      ->table('chado_search_haplotype_block_search')
      ->multiple(TRUE)
      ->labelWidth(140)
      );
  $form->addFile(
      Set::file()
      ->id('stock_name_file')
      ->title('')
      ->labelWidth(1)
      ->newLine()
      );
  $form->addTextFilter(
      Set::textFilter()
      ->id('feature_uniquename')
      ->title('Haplotype Block')
      ->labelWidth(140)
      ->newLine()
      );
  // Restricted by Location
  $form->addSelectFilter(
      Set::selectFilter()
      ->id('genome')
      ->title('Genome')
      ->column('genome')
      ->table('chado_search_haplotype_block_location')
      ->cache(TRUE)
      ->labelWidth(140)
      ->newLine()
      );
  $form->addDynamicSelectFilter(
      Set::dynamicSelectFilter()
      ->id('location')
      ->title('Chr/Scaffold')
      ->dependOnId('genome')
      ->callback('chado_search_haplotype_block_search_ajax_location')
      ->labelWidth(140)
      );
  $form->addBetweenFilter(
      Set::betweenFilter()
      ->id('fmin')
      ->title("between")
      ->id2('fmax')
      ->title2("and")
      ->labelWidth2(50)
      ->size(10)
      );
  $form->addMarkup(
      Set::markup()
      ->id('location_unit')
      ->text("<strong>bp</strong>")
      ->newLine()
      );
  $form->addLabeledFilter(
      Set::LabeledFilter()
      ->id('gene_model')
      ->title('Gene Model')
      ->labelWidth(140)
      ->newLine()
      );
  $form->addSelectFilter(
      Set::selectFilter()
      ->id('trait')
      ->title('Trait')
      ->column('trait')
      ->table('chado_search_haplotype_block_location')
      ->labelWidth(140)
      );
  $form->addSubmit();
  $form->addReset();
  $desc = "Search Haplotype Block is a page where users can search for the Haplotype Block
      based on the species and germplasm, as well as the genome position. Click the question mark next to ‘Dataset’ to view the details of the dataset. ".
      " | <b>" . '<a href=/tutorial/search_haplotype_block>Text tutorial</a>' . ' | ' .
      '<a href=/contact>Email us with problems and suggestions</a>' . '</b>';
      $form->addFieldset(
          Set::fieldset()
          ->id('haplotype_block_search')
          ->startWidget('project')
          ->endWidget('reset')
          ->description($desc)
          );
      return $form;
}

// Submit the form
function chado_search_haplotype_block_search_form_submit ($form, &$form_state) {

  // Add conditions
  $where = array();
  $where [] = Sql::selectFilter('project', $form_state, 'project');
  $where [] = Sql::selectFilter('organism', $form_state, 'organism');
  $where [] = Sql::selectFilter('stock', $form_state, 'stock');
  $where [] = Sql::file('stock_name_file', 'stock');
  $where [] = Sql::textFilter('feature_uniquename', $form_state, 'HBS.haplotype_block');
  $where [] = Sql::selectFilter('genome', $form_state, 'genome');
  $where [] = Sql::selectFilter('location', $form_state, 'landmark');
  $where [] = Sql::betweenFilter('fmin', 'fmax', $form_state, 'fmin', 'fmax');
  $where [] = Sql::selectFilter('trait', $form_state, 'trait');

  // If there is gene model, convert it into positions first
  $gene_model = $form_state->getValue('gene_model');
  if ($gene_model) {
    $sql = "SELECT F.feature_id, srcfeature_id, fmin, fmax FROM {feature} F INNER JOIN {featureloc} FL ON F.feature_id = FL.feature_id WHERE lower(name) = :name OR lower(uniquename) = :uniquename";
    $alignment = chado_search_query($sql, array(":name" => strtolower($gene_model), ":uniquename" => strtolower($gene_model)), 'chado')->fetchObject();
    if (!$alignment) {
      $form_state->setErrorByName('gene_model', 'Gene model\'s position not available.');
    }
    else {
      $where [] = 'landmark_id = ' . $alignment->srcfeature_id;
      $where [] = 'fmin < ' . $alignment->fmin;
      $where [] = 'fmax > ' . $alignment->fmax;
    }
  }

  $sql = "
    SELECT
      string_agg(stock_id, '&') AS stock_id,
      string_agg(stock, '&') AS stock,
      max(HBS.hb_feature_id) AS hb_feature_id,
      HBS.haplotype_block,
      string_agg(haplotype, '&') AS haplotype,
      string_agg(values, '&') AS values,
      string_agg(marker_feature_ids, '&') AS marker_feature_ids,
      string_agg(markers, '&') AS markers,
      max(analysis_id) AS analysis_id,
      max(genome) AS genome,
      max(landmark_id) AS landmark_id,
      max(landmark) || ':' || max(fmin) || '-' || max(fmax) AS location,
      max(landmark) AS landmark,
      max(fmin) AS fmin,
      max(fmax) AS fmax,
      string_agg(description, '&') AS description,
      max(project_id) AS project_id,
      max(project) AS project
    FROM {chado_search_haplotype_block_search} HBS
    LEFT JOIN {chado_search_haplotype_block_location} HBL ON HBS.hb_feature_id = HBL.hb_feature_id";
  Set::result()
    ->sql($sql)
    ->where($where)
    ->append('GROUP BY HBS.haplotype_block')
    ->tableDefinitionCallback('chado_search_haplotype_block_search_table_definition')
    ->rewriteCols('values=chado_search_haplotype_block_search_rewrite_values*')
    ->customDownload(array('disable_default' => TRUE, 'chado_search_haplotype_block_search_download' => 'Table'))
    ->disableCols('row-counter')
    ->execute($form, $form_state);
}


/*************************************************************
 * Build the search result table
*/
// Define the result table
function chado_search_haplotype_block_search_table_definition () {
  $headers = array(
      // 'haplotype_block:s:chado_search_link_feature:hb_feature_id' => 'Haplotype Block',
      'values' => 'Haplotype Blocks',
      // 'genome' => 'Genome',
      // 'location' => 'Genome position',
  );
  return $headers;
}

function chado_search_haplotype_block_search_rewrite_values($obj) {

  // Transform data into output format
  $data = chado_search_haplotype_block_search_prepare_output($obj);
  $data_stock = $data->stock;
  $data_stock_markers = $data->stock_markers;
  $data_stock_htype = $data->stock_htype;
  $data_stock_htype_values = $data->stock_htype_values;

  // Retrieve marker positions
  $marker_ids = array();
  $marker_id_arr =  explode('&', $obj->marker_feature_ids);
  foreach ($marker_id_arr AS $arr) {
    $marker_ids += explode('|', $arr);
  }
  $locations = chado_search_haplotype_block_search_get_marker_genome_location($marker_ids, $obj->project_id);
  $positions = chado_search_haplotype_block_search_get_marker_map_position($marker_ids, $obj->project_id);

  $marker_genome = '';
  $link_genome = '';
  foreach ($marker_ids AS $mid) {
    if (isset($locations[$mid])) {
      $marker_genome = $locations[$mid]->genome;
      $link_genome = chado_search_link_entity('analysis', $locations[$mid]->genome_analysis_id);
    }
    if ($marker_genome) {
      break;
    }
  }
  $marker_map = '';
  $link_map = '';
  foreach ($marker_ids AS $mid) {
    if (isset($positions[$mid])) {
      $marker_map = $positions[$mid]->map;
      $link_map = chado_search_link_entity('featuremap', $positions[$mid]->featuremap_id);
    }
    if (trim($marker_map)) {
      break;
    }
  }
  // Output
  // output stocks as the header row
  $project_link = chado_search_link_entity('project', $obj->project_id);
  $hb_link = chado_search_link_entity('feature', $obj->hb_feature_id);
  asort($data_stock);
  $output = '<div>Project: <a href="' . $project_link . '">' . $obj->project . '</a><div>';
  $output .= '<div>Genome: <a href="' . $link_genome . '">' . $marker_genome . '</a></div>';
  $output .= '<div>Map: <a href="' . $link_map . '">' . $marker_map . '</a></div>';
  $output .=
    '<table style="border-width:0px;border-top: 1px solid #CCC;;border-bottom: 1px solid #CCC;width:auto;margin-left:-3px">
       <tr>
         <td colspan=4>Haplotype Block: <a href="' . $hb_link . '">'. $obj->haplotype_block. '</a></td>';
  foreach($data_stock AS $stock_id => $s) {
    $slink = chado_search_link_entity('stock', $stock_id);
    $output .= "<td style=\"min-width: 110px;\"><a href=$slink>" . $s . '</a></td>';
  }

  // output hyplotype
  $output .= '<tr style="border-bottom: 1px solid #CCC;"><td>Marker</td><td>SSID</td><td>Genome location</td><td>Map position</td>';
  foreach($data_stock AS $stock_id => $s) {
    $output .= "<td style=font-family:monospace;font-weight:bold nowrap=nowrap>" . $data_stock_htype[$stock_id] . '</td>';
  }
  $output .= '</tr>';

  // output markers
  $mks = $data_stock_markers[$stock_id];
  // order markers by position
  $order_position = array();
  foreach ($mks AS $marker_id => $mk) {
    $order_position [$marker_id] = isset($locations[$marker_id]) ? $locations[$marker_id]->fmin : '';
  }
  asort($order_position);
  $order_marker = array();
  foreach ($order_position AS $marker_id => $pos) {
    $order_marker[$marker_id] = $mks[$marker_id];
  }
  //asort($mks); // order marker by name
  $output .= '<tr><td nowrap=nowrap>';
  foreach ($order_marker AS $marker_id => $mk) {
    $mlink = chado_search_link_entity('feature', $marker_id);
    $output .= "<a href=$mlink>" . $mk . '</a></br>';
  }
  $output .= '</td>';

  // output marker ss_id
  $output .= '<td nowrap=nowrap>';
  foreach ($order_marker AS $marker_id => $mk) {
    if (isset($locations[$marker_id])) {
      $mobj = $locations[$marker_id];
      $output .=  chado_search_haplotype_block_search_get_ssid($marker_id);
    }
    $output .=  '</br>';
  }
  $output .= '</td>';

  // output marker rs_id
/*   $output .= '<td nowrap=nowrap>';
  foreach ($order_marker AS $marker_id => $mk) {
    $mobj = $locations[$marker_id];
    $output .=  $mobj->rs_id . '</br>';
  }
  $output .= '</td>'; */

  // output marker genome locations
  $output .= '<td nowrap=nowrap><div style="width:140px;overflow:hidden;"  onmouseover="this.style.overflow=\'auto\';" onmouseout="this.style.overflow=\'hidden\';">';
  foreach ($order_marker AS $marker_id => $mk) {
    if (isset($locations[$marker_id])) {
      $mobj = $locations[$marker_id];
      $jbrowse = chado_search_link_jbrowse(array($mobj->landmark_id,$mobj->location));
      $output .= '<a href="' . $jbrowse . '">' . $mobj->location . '</a>';
    }
    $output .=  '</br>';
  }
  $output .= '<div></td>';

  // output marker map positions
  $output .= '<td nowrap=nowrap><div style="width:100px;overflow:hidden;"  onmouseover="this.style.overflow=\'auto\';" onmouseout="this.style.overflow=\'hidden\';">';
  foreach ($order_marker AS $marker_id => $mk) {
    if (isset($positions[$marker_id])) {
      $mobj = $positions[$marker_id];
      $mviewer = '/mapviewer/' . $mobj->featuremap_id . '/' . $mobj->lg . '/' .  $marker_id;
      $output .= '<a href="' . $mviewer . '">' . $mobj->position . '</a>';
    }
    $output .= '<div></td>';
  }
  $output .= '</div></td>';
  // output haplotypes
  foreach ($data_stock AS $stock_id => $s) {
    $htypes = $data_stock_htype_values[$stock_id];
    $output .= '<td nowrap=nowrap style=font-family:monospace>';
    foreach ($order_marker AS $marker_id => $mk) {
      $output .= $htypes[$marker_id] . '</br>';
    }
    $output .= '</td>';
  }

  $output .= '</tr></tr></table>';
  return $output;
}

function chado_search_haplotype_block_search_prepare_output($obj) {
  $stock_ids =  explode('&', $obj->stock_id);
  $stocks =  explode('&', $obj->stock);
  $marker_id_arr =  explode('&', $obj->marker_feature_ids);
  $marker_arr =  explode('&', $obj->markers);
  $value_arr = explode('&', $obj->values);
  $haplotype = explode('&', $obj->haplotype);
  $desc = explode('&', $obj->description);

  //preparing data
  $data_stock = array();
  $data_stock_markers = array();
  $data_stock_htype = array();
  $data_stock_htype_values = array();

  // associate marker data for each stock
  foreach ($stocks AS $idx => $stock) {
    $data_stock[$stock_ids[$idx]] = $stock;
    $data_stock_markers[$stock_ids[$idx]] = array();
    $data_stock_htype[$stock_ids[$idx]] = str_replace('|', ' ', $desc[$idx]);
    $data_stock_htype_values[$stock_ids[$idx]] = array();
    // associate marker with its haplotype value
    $data_m = explode('|', $marker_arr[$idx]);
    $data_mid = explode('|', $marker_id_arr[$idx]);
    $data_value = explode('|', $value_arr[$idx]);
    $description = $desc[$idx];
    // rewrite $data_value according to the $description
    $key_haplotype = explode(' ', $haplotype[$idx]);
    $key_description = explode('|', $description);
    foreach ($data_value AS $k=> $v) {
      $vs = explode(' ', $v);
      $kv = array();
      foreach ($vs AS $k1 => $val) {
        $kv [$key_haplotype[$k1]] = $val;
      }
      $new_value = '';
      foreach ($key_description AS $k2 => $v2) {
        $new_value .= isset($kv[$v2]) ? $kv[$v2] : '';
        if ($k2 < count($key_description) - 1) {
          $new_value .= ' ';
        }
      }
      $data_value[$k] = $new_value;
    }
    // finish rewriting values
    foreach ($data_m AS $idx2 => $m) {
      $data_stock_markers[$stock_ids[$idx]][$data_mid[$idx2]] = $m;
      $data_stock_htype_values[$stock_ids[$idx]][$data_mid[$idx2]] = $data_value[$idx2];
    }
  }
  $prepared_data = new stdClass();
  $prepared_data->stock = $data_stock;
  $prepared_data->stock_markers = $data_stock_markers;
  $prepared_data->stock_htype = $data_stock_htype;
  $prepared_data->stock_htype_values = $data_stock_htype_values;

  return $prepared_data;
}

function chado_search_haplotype_block_search_get_marker_genome_location($marker_ids, $project_id) {

  $genomes = \Drupal::state()->get('chado_search_haplotype_search_genomes', array());
  $default_genomes = isset($genomes[$project_id]) ? $genomes[$project_id] : array();
  if (count($default_genomes) == 0) {
    return array();
  }
  // Get genome location
  $sql = "
    SELECT marker_feature_id, location, genome_analysis_id, genome , landmark_id, fmin, ss_id, rs_id
    FROM chado_search_haplotype_block_marker_location
    WHERE marker_feature_id IN (:feature_id[])
    AND genome_analysis_id IN (:analysis_id[])";
  $location = array();
  $result = chado_search_query($sql, array(':feature_id[]' => $marker_ids, ':analysis_id[]' => $default_genomes), 'chado');
  while ($obj = $result->fetchObject()) {
    $location[$obj->marker_feature_id] = $obj;
  }
  return $location;
}

function chado_search_haplotype_block_search_get_marker_map_position($marker_ids, $project_id) {

  $maps = \Drupal::state()->get('chado_search_haplotype_search_maps', array());
  $default_maps = isset($maps[$project_id]) ? $maps[$project_id] : array();
  if (count($default_maps) == 0) {
    return array();
  }
  // Get map position
  $sql =  "
    SELECT marker_feature_id, position, featuremap_id, map, lg
    FROM chado_search_haplotype_block_marker_position
    WHERE marker_feature_id IN (:feature_id[])
    AND featuremap_id IN (:featuremap_id[])";
  $position = array();
  $result = chado_search_query($sql, array(':feature_id[]' => $marker_ids, ':featuremap_id[]' => $default_maps), 'chado');
  while ($obj = $result->fetchObject()) {
    $position[$obj->marker_feature_id] = $obj;
  }
  return $position;
}

// Define call back to link the featuremap to its  node for result table
function chado_search_haplotype_block_search_link_feature ($feature_id) {
  return chado_search_link_entity('feature', $feature_id);
}

function chado_search_haplotype_block_search_download($handle, $result, $sql, $total_items, $progress_var) {
  // Write header
  fwrite($handle, "\"Haplotype Blocks\"\n");
  while ($obj = $result->fetchObject()) {
    // Transform data into output format
    $data = chado_search_haplotype_block_search_prepare_output($obj);
    $data_stock = $data->stock;
    $data_stock_markers = $data->stock_markers;
    $data_stock_htype = $data->stock_htype;
    $data_stock_htype_values = $data->stock_htype_values;

    // Retrieve marker positions
    $marker_ids = array();
    $marker_id_arr =  explode('&', $obj->marker_feature_ids);
    foreach ($marker_id_arr AS $arr) {
      $marker_ids += explode('|', $arr);
    }
    $locations = chado_search_haplotype_block_search_get_marker_genome_location($marker_ids, $obj->project_id);
    $positions = chado_search_haplotype_block_search_get_marker_map_position($marker_ids, $obj->project_id);

    $marker_genome = '';
    foreach ($marker_ids AS $mid) {
      $marker_genome = $locations[$mid]->genome;
      if ($marker_genome) {
        break;
      }
    }
    $marker_map = '';
    foreach ($marker_ids AS $mid) {
      $marker_map = $positions[$mid]->map;
      if (trim($marker_map)) {
        break;
      }
    }

    // Output
    // output stocks as the header row
    asort($data_stock);
    fwrite($handle, '"Genome: ' . $marker_genome . '"'. "\n");
    fwrite($handle, '"Map: ' . $marker_map . '"' . "\n");
    fwrite($handle, '"Haplotype Block: ' . $obj->haplotype_block . '",,,');
    foreach($data_stock AS $stock_id => $s) {;
    fwrite($handle, ',"' . $s . '"');
    }
    // output hyplotype
    fwrite($handle, "\n");
    fwrite($handle, '"Marker","SSID","Genome location","Map position"');
    foreach($data_stock AS $stock_id => $s) {
      fwrite($handle, ',"' . $data_stock_htype[$stock_id] . '"');
    }
    fwrite($handle, "\n");

    // output markers
    $mks = $data_stock_markers[$stock_id];
    // order markers by position
    $order_position = array();
    foreach ($mks AS $marker_id => $mk) {
      $order_position [$marker_id] = $locations[$marker_id]->fmin;
    }
    asort($order_position);
    $order_marker = array();
    foreach ($order_position AS $marker_id => $pos) {
      $order_marker[$marker_id] = $mks[$marker_id];
    }
    //asort($mks); // order marker by name
    foreach ($order_marker AS $marker_id => $mk) {
      fwrite($handle, '"' . $mk . '","' . chado_search_haplotype_block_search_get_ssid($marker_id) . '","' . $locations[$marker_id]->location . '","' . $positions[$marker_id]->position . '"');
      // output haplotypes
      foreach ($data_stock AS $stock_id => $s) {
        $htypes = $data_stock_htype_values[$stock_id];
        fwrite($handle, ',"' . $htypes[$marker_id] . '"');
      }
      fwrite($handle, "\n");
    }
  }
}

/*************************************************************
 * AJAX callbacks
*/
// User defined: Populating the landmark for selected organism
function chado_search_haplotype_block_search_ajax_location ($val) {
  $sql = "SELECT distinct landmark, CASE WHEN regexp_replace(landmark, E'\\\D','','g') = '' THEN 999999 ELSE regexp_replace(landmark, E'\\\D','','g')::numeric END AS lnumber FROM {chado_search_haplotype_block_location} WHERE genome = :genome ORDER BY lnumber";
  return chado_search_bind_dynamic_select(array(':genome' => $val), 'landmark', $sql);
}

function chado_search_haplotype_block_search_get_ssid($feature_id) {
  $sql = "
  SELECT name
  FROM chado.synonym S
  WHERE S.synonym_id IN (SELECT synonym_id FROM chado.feature_synonym WHERE feature_id = :feature_id)
  AND (SELECT name FROM chado.cvterm WHERE cvterm_id = S.type_id) = ('ss_id') LIMIT 1";
  return chado_search_query($sql, array(':feature_id' => $feature_id))->fetchField();
}