<?php

use Drupal\chado_search\Core\Set;
use Drupal\chado_search\Core\Sql;

/*************************************************************
 * Search form, form validation, and submit function
 */
// Search form
function chado_search_trait_descriptor_form ($form) {
  $table = "
    (SELECT
       CVP.value AS cv
     FROM chado.cv CV
     INNER JOIN chado.cvprop CVP on CVP.cv_id = CV.cv_id
     INNER JOIN chado.cvterm S on S.cvterm_id = CVP.type_id
     INNER JOIN chado.cv S_CV on S_CV.cv_id = S.cv_id
     INNER JOIN chado.cvterm D ON D.cv_id = CV.cv_id
     WHERE S_CV.name = 'MAIN' AND S.name = 'descriptor_search'
     ORDER BY CV.name
    ) T
  ";
  $form->addSelectFilter(
      Set::selectFilter()
      ->id('group')
      ->title('Group')
      ->table($table)
      ->column('cv')
      ->newline()
      );
  $cat = "
   (SELECT
        CAT.name AS category
      FROM cvterm C
      LEFT JOIN (
        SELECT (SELECT name FROM cv WHERE cv_id = C1.cv_id) AS cv, C1.name, C1.cvterm_id, CR.subject_id
          FROM cvterm C1
          INNER JOIN cvterm_relationship CR on CR.object_id = C1.cvterm_id
          INNER JOIN cvterm C2 on C2.cvterm_id = CR.type_id
          INNER JOIN cv CV2 on CV2.cv_id = C2.cv_id
          WHERE CV2.name = 'MAIN' AND C2.name = 'belongs_to'
      ) TRAIT on TRAIT.subject_id = C.cvterm_id
      LEFT JOIN (
        SELECT C1.name, C1.cvterm_id, CR.subject_id
        FROM cvterm C1
        INNER JOIN cvterm_relationship CR on CR.object_id = C1.cvterm_id
		INNER JOIN cv CV1 on CV1.cv_id = C1.cv_id
        INNER JOIN cvterm C2 on C2.cvterm_id = CR.type_id
        INNER JOIN cv CV2 on CV2.cv_id = C2.cv_id
        WHERE CV2.name = 'relationship' AND C2.name = 'is_a'
		  AND CV1.name ~ 'trait_class_PTO$'
      ) CAT on CAT.subject_id = TRAIT.cvterm_id
      WHERE TRAIT.cv LIKE '%_trait_ontology') T
  ";
  $form->addSelectFilter(
      Set::selectFilter()
      ->id('category')
      ->title('Category')
      ->table($cat)
      ->column('category')
      ->newline()
      );
  $form->addTextFilter(
      Set::textFilter()
      ->id('keyword')
      ->title('Keyword')
      ->newline()
      );
  $form->addSubmit();
  $form->addReset();
  $desc = 'Search for Trait Descriptors in phenotyping datasets from publications or NPGS GRIN. Trait Descriptors are associated the highest category of Plant Trait Ontology.';
  $form->addFieldset(
      Set::fieldset()
      ->id('trait_search_fields')
      ->startWidget('group')
      ->endWidget('reset')
      ->description($desc)
      );
  return $form;
}

// Submit the form
function chado_search_trait_descriptor_form_submit ($form, &$form_state) {
  $where = array();
  // Get base sql
  $sql = chado_search_trait_descriptor_base_query();
  $group = $form_state->getValue('group');
  $group_cv = \Drupal::database()->query("SELECT name FROM chado.cv WHERE cv_id = (SELECT cv_id FROM chado.cvprop WHERE value = :group)", array(':group' => $group))->fetchField();
  if ($group_cv) {
    $where [] = "cv.name = '$group_cv'";
  }
  $cat_val = $form_state->getValue('category');
  if ($cat_val) {
    $where [] = "category ~* '" . $cat_val . "'";
  }
  $where [] = Sql::textFilterOnMultipleColumns('keyword', $form_state, array('C.name', 'TRAIT.name', 'C.definition'));
  Set::result()
  ->sql($sql)
  ->where($where)
  ->tableDefinitionCallback('chado_search_trait_descriptor_table_definition')
  ->execute($form, $form_state);
}

function chado_search_trait_descriptor_base_query () {
  $sql = "
    SELECT C.cvterm_id AS descriptor_id,
      CVP.value AS cv,
      CAT.category,
      TRAIT.cvterm_id AS trait_id,
      TRAIT.name AS trait,
      C.name AS descriptor,
      C.definition
    FROM cvterm C
    INNER JOIN cv CV on CV.cv_id = C.cv_id
    INNER JOIN chado.cvprop CVP on CVP.cv_id = CV.cv_id
    INNER JOIN chado.cvterm S on S.cvterm_id = CVP.type_id AND S.name = 'descriptor_search'
    INNER JOIN chado.cv S_CV on S_CV.cv_id = S.cv_id AND S_CV.name = 'MAIN'
    LEFT JOIN (
      SELECT (SELECT name FROM cv WHERE cv_id = C1.cv_id) AS cv, C1.name, C1.cvterm_id, CR.subject_id
        FROM cvterm C1
        INNER JOIN cvterm_relationship CR on CR.object_id = C1.cvterm_id
        INNER JOIN cvterm C2 on C2.cvterm_id = CR.type_id
        INNER JOIN cv CV2 on CV2.cv_id = C2.cv_id
        INNER JOIN cvprop CV3P on CV3P.cv_id = C1.cv_id
        INNER JOIN cvterm C3 on C3.cvterm_id = CV3P.type_id
        INNER JOIN cv CV3 on CV3.cv_id = C3.cv_id
        WHERE CV2.name = 'MAIN' AND C2.name = 'belongs_to'
          AND CV3.name = 'MAIN' AND  C3.name = 'type' AND CV3P.value = 'trait'
    ) TRAIT on TRAIT.subject_id = C.cvterm_id
    LEFT JOIN (
        SELECT CR.subject_id, STRING_AGG(C1.name, ', ') AS category
      FROM cvterm C1
      INNER JOIN cvterm_relationship CR on CR.object_id = C1.cvterm_id
      INNER JOIN cvterm C2 on C2.cvterm_id = CR.type_id
      INNER JOIN cv CV2 on CV2.cv_id = C2.cv_id
      WHERE CV2.name = 'relationship' AND C2.name = 'is_a'
       GROUP BY CR.subject_id
    ) CAT on CAT.subject_id = TRAIT.cvterm_id
    LEFT JOIN (
      SELECT CP.cvterm_id, CP.value AS abbreviation
      FROM chado.cvtermprop CP
      WHERE CP.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'abbreviation' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
    ) ABB on ABB.cvterm_id = C.cvterm_id
    LEFT JOIN (
      SELECT CR.subject_id, M.definition, M.name, CV_M.name AS cv_name
      FROM chado.cvterm_relationship CR
        INNER JOIN chado.cvterm M on M.cvterm_id = CR.object_id
        INNER JOIN chado.cv CV_M on CV_M.cv_id = M.cv_id
          WHERE CV_M.name ~ 'method' AND CR.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'belongs_to' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
    ) METHOD on METHOD.subject_id = C.cvterm_id
      ";
  return $sql;
}
/*************************************************************
 * Build the search result table
 */
// Define the result table
function chado_search_trait_descriptor_table_definition () {
  $headers = array(
    'cv:s' => 'Group',
    'descriptor:s:chado_search_trait_descriptor_link_descriptor:descriptor_id' => 'Descriptor',
    'category:s' => 'Category',
    'trait:s:chado_search_trait_descriptor_link_trait:trait_id' => 'Trait',
    'definition:s' => 'Trait Definition',
    'abbreviation:s:chado_search_trait_descriptor_link_abbreviation:abbreviation' => 'Crop Ontology Variable',
    'method:s' => 'Method'
  );
  return $headers;
}

// Define call back to link the trait to its  node for the result table
function chado_search_trait_descriptor_link_trait ($cvterm_id) {
  return "/display/trait/$cvterm_id";
}

function chado_search_trait_descriptor_link_descriptor ($cvterm_id) {
  return "/display/trait_descriptor/$cvterm_id";
}

function chado_search_trait_descriptor_link_abbreviation ($abbreviation) {
  return "https://cropontology.org/search?q=$abbreviation&facet_ontology_name.keyword=Strawberry";
}