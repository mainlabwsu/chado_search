<?php

use Drupal\chado_search\Core\Set;
use Drupal\chado_search\Core\Sql;

/*************************************************************
 * Search form, form validation, and submit function
 */
// Search form
function chado_search_orthologs_form ($form) {
    // Genome
    $form->addSelectFilter(
        Set::selectFilter()
        ->id('genome')
        ->title('Genome')
        ->table('chado_search_orthologs')
        ->column('l_genome')
        ->cache(TRUE)
        ->labelWidth(163)
        ->newline()
        );
    
    // Chr/scaffold
    $form->addDynamicSelectFilter(
        Set::dynamicSelectFilter()
        ->id('chromosome')
        ->title('Chromosome/Scaffold')
        ->dependOnId('genome')
        ->callback('chado_search_orthologs_ajax_chromosomes')
        ->labelWidth(163)
        ->cache('chado_search_orthologs',['l_genome', 'l_landmark'])
        ->newline()
        );
    
    // Left feature names
    $form->addTextFilter(
        Set::textFilter()
        ->id('feature_name')
        ->title('Gene/Transcript Name')
        ->labelWidth(163)
        );
    $form->addFile(
        Set::file()
        ->id('feature_name_file_inline')
        ->labelWidth(1)
        ->newLine()
        );
    
    // Comparing genomes
    $form->addDynamicSelectFilter(
        Set::dynamicSelectFilter()
        ->id('compare')
        ->title('Compare to')
        ->dependOnId('genome')
        ->callback('chado_search_orthologs_ajax_comparing_genomes')
        ->labelWidth(163)
        ->multiple(TRUE)
        ->cache('chado_search_orthologs',['l_genome', 'r_genome'])
        ->newline()
        );
    
    // Comparing Chr/scaffold
    $form->addDynamicSelectFilter(
        Set::dynamicSelectFilter()
        ->id('cp_chromosome')
        ->title('Chromosome/Scaffold')
        ->dependOnId('compare')
        ->callback('chado_search_orthologs_ajax_cp_chromosomes')
        ->labelWidth(163)
        ->cache('chado_search_orthologs',['r_genome', 'r_landmark'])
        ->newline()
        );
    
    $form->addSubmit();
    $form->addReset();
    $desc =
    'Retrieve orthologs/paralogs that are detected using MCScanX
    (<a target=_blank href=https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3326336/>Wang et al. 2012</a>)
    using default settings. Sequences in ortholog/paralog columns between different assemblies/annotations
    of the same species represents potentially the same genes. In most cases, mRNA transcripts were used in
    the analysis and genes were used only when mRNAs are not available. The result table provides associated
    gene names as well.';
    $form->addFieldset(
        Set::fieldset()
        ->id('gene_search_fields')
        ->startWidget('genome')
        ->endWidget('reset')
        ->description($desc)
        );
    return $form;
}

function chado_search_orthologs_form_validate ($form, &$form_state) {
    
    if ($form_state->getValue('genome') == '0') {
        $form_state->setErrorByName('genome', 'Please select a genome');
    }
}

// Submit the form
function chado_search_orthologs_form_submit ($form, &$form_state) {
    $sql = "SELECT * FROM {chado_search_orthologs}";
    // Add conditions
    $where = array();
    $where [] = Sql::selectFilter('genome', $form_state, 'l_genome');
    $where [] = Sql::selectFilter('chromosome', $form_state, 'l_landmark');
    $where [] = Sql::textFilter('feature_name', $form_state, 'l_feature');
    $where [] = Sql::file('feature_name_file_inline', 'l_feature');
    $where [] = Sql::selectFilter('compare', $form_state, 'r_genome');
    $where [] = Sql::selectFilter('cp_chromosome', $form_state, 'r_landmark');

    Set::result()
    ->sql($sql)
    ->where($where)
    ->tableDefinitionCallback('chado_search_orthologs_table_definition')
    ->rewriteCols('r_feature_id=chado_search_orthologs_rewrite_feature_id')
    ->execute($form, $form_state);
}

/*************************************************************
 * Build the search result table
*/
// Define the result table

function chado_search_orthologs_table_definition () {
  $headers = array(
      'l_genome:s:chado_search_link_analysis:l_analysis_id' => 'Genome1',
      'l_landmark:s' => 'Chromosome/Scaffold1',
      'l_feature:s:chado_search_link_feature:l_feature_id' => 'Ortholog/Paralog1 ',
      'r_genome:s:chado_search_link_analysis:r_analysis_id' => 'Genome2',
      'r_landmark:s' => 'Chromosome/Scaffold2',
      'r_feature:s:chado_search_link_feature:r_feature_id' => 'Ortholog/Paralog2',
      'r_feature_id::chado_search_ortholog_link_gene:r_feature_id' => 'Associated Gene'
  );
  return $headers;
}

function chado_search_orthologs_rewrite_feature_id ($feature_id) {
  $sql = "
      SELECT
        object_id,
        (SELECT name FROM chado.feature WHERE feature_id = object_id) AS name
      FROM chado.feature_relationship FR
      WHERE FR.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'part_of' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'sequence'))
      AND subject_id = :subject_id LIMIT 1";
  $db = \Drupal::database();
  $obj = $db->query($sql, array(':subject_id' => $feature_id))->fetchObject();
  //$ln = chado_search_link_feature($obj->object_id);
  if ($obj) {
    return $obj->name;
  } else {
    return '';
  }
}

function chado_search_ortholog_link_gene ($feature_id) {
  $sql = "
      SELECT
        object_id
      FROM chado.feature_relationship FR
      WHERE FR.type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'part_of' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'sequence'))
      AND subject_id = :subject_id LIMIT 1";
  $db = \Drupal::database();
  $object_id = $db->query($sql, array(':subject_id' => $feature_id))->fetchField();
  $ln = chado_search_link_feature($object_id);
  return $ln;
}

function chado_search_orthologs_ajax_chromosomes($val) {
  $table = \Drupal::database()->schema()->tableExists('cache_orthologs_chromosome') ? 'cache_orthologs_chromosome' : 'chado_search_orthologs';
  $sql = "SELECT DISTINCT l_landmark FROM {$table} WHERE l_genome = :l_genome ORDER BY l_landmark";
  return chado_search_bind_dynamic_select(array(':l_genome' => $val), 'l_landmark', $sql);
}

function chado_search_orthologs_ajax_cp_chromosomes($val) {
  $table = \Drupal::database()->schema()->tableExists('cache_orthologs_cp_chromosome') ? 'cache_orthologs_cp_chromosome' : 'chado_search_orthologs';
    $sql = "SELECT DISTINCT r_landmark FROM {$table} WHERE r_genome IN (:r_genome[]) ORDER BY r_landmark";
    return chado_search_bind_dynamic_select(array(':r_genome[]' => $val), 'r_landmark', $sql);
}

function chado_search_orthologs_ajax_comparing_genomes ($val) {
  $table = \Drupal::database()->schema()->tableExists('cache_orthologs_compare') ? 'cache_orthologs_compare' : 'chado_search_orthologs';
    $sql = "SELECT DISTINCT r_genome FROM {$table} WHERE l_genome = :l_genome ORDER BY r_genome";
    return chado_search_bind_dynamic_select(array(':l_genome' => $val), 'r_genome', $sql);
}