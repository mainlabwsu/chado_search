<?php

use Drupal\chado_search\Core\Set;
use Drupal\chado_search\Core\Sql;

/*************************************************************
 * Search form, form validation, and submit function
 */
// Search form
function chado_search_nearby_markers_form ($form) {
  /*$form->addTabs(
      Set::tab()
      ->id('nearby_marker_tabs')
      ->items(array('/search/markers' => 'Marker Search', '/search/snp_markers' => 'SNP Marker Search', '/search/nearby_markers' => 'Search Nearby Markers'))
  );*/
  // Basic
  $form->addLabeledFilter(
      Set::labeledFilter()
      ->id('nearby_marker_locus')
      ->title('Locus')
      ->labelWidth(120)
  );
  $form->addMarkup(
      Set::markup()
      ->id('marker_example')
      ->text("(eg. AG51, Hi04e04, CPPCT016, UFFxa16H07)")
  );
  $form->addFile(
      Set::file()
      ->id('feature_name_file_inline')
      ->labelWidth(1)
      );
  $form->addMarkup(
      Set::markup()
      ->id('file_limit')
      ->text("(upload limit: 10000 lines)")
      ->newLine()
      );
  $form->addLabeledFilter(
      Set::labeledFilter()
      ->id('nearby_marker_distance')
      ->title('Distance')
      ->labelWidth(120)
  );
  $form->addMarkup(
      Set::markup()
      ->id('nearby_marker_unit')
      ->text("cM")
      ->newline()
  );
  // marker type
  $icon = '/' . \Drupal::service('extension.list.module')->getPath('chado_search') . '/theme/images/question.gif';
  $form->addSelectFilter(
      Set::selectFilter()
      ->id('marker_type')
      ->title('Marker Type <a href="/marker_type"><img src="' . $icon . '"></a>')
      ->column('value')
      ->table("(SELECT value FROM featureprop WHERE type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'marker_type' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))) T")
      ->labelWidth(120)
      ->newLine()
      );
  
  $form->addSubmit();
  $form->addReset();
  $desc =
  'Search for markers in GDR. In search nearby markers site, users can obtain a list of all loci that
      are within a specified distance of the particular locus on any genetic map.
     <b>| ' . '<a href=/tutorial/search_nearby_markers>Text tutorial</a>' . ' | ' .
       '<a href=/contact>Email us with problems and suggestions</a>' . '</b>';
  $form->addFieldset(
      Set::fieldset()
      ->id('nearby_markers_fieldset')
      ->startWidget('nearby_marker_locus')
      ->endWidget('reset')
      ->description($desc)
  );
  return $form;
}
// Validate the form
function chado_search_nearby_markers_form_validate ($form, &$form_state) {
  $locus = $form_state->getValue('nearby_marker_locus');
  $file = $_FILES['files']['tmp_name']['feature_name_file_inline'];
  if (!$locus && !$file) {
    $form_state->setErrorByName('nearby_marker_locus', 'Locus name is required.');
  }
  $distance = $form_state->getValue('nearby_marker_distance');
  if (!is_numeric($distance)) {
    $form_state->setErrorByName('nearby_marker_distance', 'Please input a number for the distance.');
  }
}
// Submit the form
function chado_search_nearby_markers_form_submit ($form, &$form_state) {
  // Get base sql
  $sql = chado_search_nearby_markers_base_query();
  // Add conditions
  $uploaded_names = '';
  $file = $_FILES['files']['tmp_name']['feature_name_file_inline'];
  if ($file) {
    $limit = 10000;
    $handle = fopen($file, 'r');
    $counter = 1;
    while ($line = fgets($handle)) {
      $name = trim(strtolower($line));
      if ($name) {
        $uploaded_names .= "'$name',";
      }
      if ($limit && $counter >= $limit) {
        break;
      }
      $counter ++;
    }
  }
  if ($file && $uploaded_names) {
    $sql .= " AND lower(F.name) IN (" . trim($uploaded_names, ',') . ")";
  }
  else {
    $sql .= " AND " . Sql::labeledFilter('nearby_marker_locus', $form_state, 'F.name');
  }
  $distance = $form_state->getValue('nearby_marker_distance');
  $sql = "
      SELECT * FROM (" . $sql . ") A
      INNER JOIN (
      SELECT 
        map_feature_id, 
        feature_id AS nearby_feature_id, 
        (select name FROM {feature} where feature_id = FP.feature_id) AS nearby_marker, 
        round(cast(START.value as numeric), 2) AS nearby_start,
        (SELECT upper(value) FROM {featureprop} MT 
          WHERE MT.type_id = 
            (SELECT cvterm_id FROM {cvterm} WHERE name = 'marker_type' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN')) 
          AND MT.feature_id = (SELECT object_id FROM {feature_relationship} WHERE subject_id = FP.feature_id AND type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'instance_of' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'relationship')) LIMIT 1) LIMIT 1) 
        AS nearby_mtype 
      FROM {featurepos} FP
      INNER JOIN (SELECT featurepos_id, value FROM {featureposprop} FPP WHERE type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'start' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))) START ON START.featurepos_id = FP.featurepos_id
      WHERE ((SELECT name FROM cvterm WHERE cvterm_id = (SELECT type_id FROM feature WHERE feature_id = FP.feature_id)) = 'marker_locus')
      ) B ON A.map_feature_id = B.map_feature_id
      WHERE A.feature_id <> B.nearby_feature_id AND abs(B.nearby_start - A.start) <= $distance";
  $mtype = $form_state->getValue('marker_type');
  if ($mtype) {
    $sql .= " AND nearby_mtype = '$mtype'";
  }
  Set::result()
    ->sql($sql)
    ->tableDefinitionCallback('chado_search_nearby_markers_table_definition')
    ->execute($form, $form_state);
}

/*************************************************************
 * SQL
*/
// Define query for the base table. Do not include the WHERE clause
function chado_search_nearby_markers_base_query() {
  $query = "
      SELECT
      featuremap_id,
      (SELECT name FROM {featuremap} WHERE featuremap_id = FP.featuremap_id) AS featuremap,
      FP.feature_id,
      F.name AS locus,
      FP.map_feature_id,
      (SELECT name FROM {feature} WHERE feature_id = FP.map_feature_id) AS linkage_group,
      round(cast(START.value as numeric), 2) AS start
      FROM {featurepos} FP
      INNER JOIN (SELECT featurepos_id, value FROM {featureposprop} FPP WHERE type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'start' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))) START ON START.featurepos_id = FP.featurepos_id
      INNER JOIN {feature} F ON F.feature_id = FP.feature_id
      WHERE F.type_id = (SELECT cvterm_id FROM {cvterm} V WHERE name = 'marker_locus' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))";
  return $query;
}

/*************************************************************
 * Build the search result table
*/
// Define the result table
function chado_search_nearby_markers_table_definition () {
  $headers = array(
    'locus:u:chado_search_link_genetic_marker:feature_id' => 'Locus',
    'featuremap:s:chado_search_link_featuremap:featuremap_id' => 'Map',
    'linkage_group:s' => 'Linkage Group',
    'start:s' => 'Position',
    'nearby_marker:s:chado_search_link_genetic_marker:nearby_feature_id' => 'Neighbor',
    'nearby_start:s' => 'Position',
    'nearby_mtype:s' => 'Marker Type'
  );
  return $headers;
}
