<?php

use Drupal\chado_search\Core\Set;
use Drupal\chado_search\Core\Sql;
use Drupal\chado_search\Core\SessionVar;

/*************************************************************
 * Search form, form validation, and submit function
 */
// Search form
function chado_search_snp_genotype_search_form ($form) {
  $form->addTabs(
      Set::tab()
      ->id('snp_genotype_tabs')
      ->items(array('/search/snp/genotype' => 'SNP Genotype', '/search/ssr_genotype' => 'SSR Genotype'))
      );
  $icon = '/' . \Drupal::service('extension.list.module')->getPath('chado_search') . '/theme/images/question.gif';
  $form->addSelectFilter(
      Set::selectFilter()
      ->id('project_name')
      ->title('Dataset <a href="/search/project/summary?sub_type=SNP"><img src="' . $icon . '"></a>')
      ->column('project_name')
      ->table('chado_search_snp_genotype_search')
      ->cache(TRUE)
      ->labelWidth(140)
      ->newLine()
      );
  $form->addDynamicSelectFilter(
      Set::dynamicSelectFilter()
      ->id('organism')
      ->title('Species')
      ->dependOnId('project_name')
      ->callback('chado_search_snp_genotype_search_ajax_dynamic_organism')
      ->multiple(TRUE)
      ->labelWidth(140)
      ->newLine()
      );
  $form->addDynamicSelectFilter(
      Set::dynamicSelectFilter()
      ->id('stock_uniquename')
      ->title('Germplasm Name')
      ->dependOnId('project_name')
      ->callback('chado_search_snp_genotype_search_ajax_dynamic_stock')
      ->alsoDependOn(array('organism'))
      ->multiple(TRUE)
      ->labelWidth(140)
      );
  $form->addFile(
      Set::file()
      ->id('stock_name_file')
      ->title('')
      ->labelWidth(1)
      ->newLine()
      );
  $form->addTextFilter(
      Set::textFilter()
      ->id('feature_uniquename')
      ->title('SNP')
      ->labelWidth(140)
      ->newLine()
      );
  // Restricted by Location
  $form->addSelectFilter(
      Set::selectFilter()
      ->id('genome')
      ->title('Genome')
      ->column('genome')
      ->table('chado_search_snp_genotype_location')
      ->disable(array('Malus x domestica Whole Genome v1.0 Assembly & Annotation', 'Prunus persica Whole Genome v1.0 Assembly & Annotation'))
      ->cache(TRUE)
      ->labelWidth(140)
      ->newLine()
      );
  $form->addDynamicSelectFilter(
      Set::dynamicSelectFilter()
      ->id('location')
      ->title('Chr/Scaffold')
      ->dependOnId('genome')
      ->callback('chado_search_snp_genotype_search_ajax_location')
      ->labelWidth(140)
      );
  $form->addBetweenFilter(
      Set::betweenFilter()
      ->id('fmin')
      ->title("between")
      ->id2('fmax')
      ->title2("and")
      ->labelWidth2(50)
      ->size(10)
      );
  $form->addMarkup(
      Set::markup()
      ->id('location_unit')
      ->text("<strong>bp</strong>")
      ->newLine()
      );
  $form->addLabeledFilter(
      Set::LabeledFilter()
      ->id('gene_model')
      ->title('Gene Model')
      ->labelWidth(140)
      );
  $form->addLabeledFilter(
      Set::LabeledFilter()
      ->id('range')
      ->title('+/-')
      ->labelWidth(30)
      );
  $form->addMarkup(
      Set::markup()
      ->id('range_unit')
      ->text("<strong>bp</strong>")
      ->newLine()
      );

  $form->addSubmit();
  $form->addReset();
  $desc = "Search SNP Genotype is a page where users can search for the SNP genotyope
      dataset based on the germplasm and SNP markers used in the dataset. Click the next tab
      to search for SSR Genotype. To search for SNP genotype data only for cultivars and
      breeding selections please visit the <a href=\"/legacy/bt_search_genotype/by_variety\">
      'Search Genotyping Data'</a> page in the <a href=\"/legacy/breeders_toolbox\">Breeders Toolbox</a>.
      Click the question mark next to ‘Dataset’ to view the details of the dataset. ".
      " | <b>" . '<a href=/tutorial/search_snp_genotype>Text tutorial</a>' . ' | ' .
      '<a href=/contact>Email us with problems and suggestions</a>' . '</b>';
      $form->addFieldset(
          Set::fieldset()
          ->id('snp_genotype_search')
          ->startWidget('project_name')
          ->endWidget('reset')
          ->description($desc)
          );
      return $form;
}

function chado_search_snp_genotype_search_form_validate ($form, &$form_state) {
  $dataset = $form_state->getValue('project_name');
  if (!$dataset) {
    $form_state->setErrorByName('project_name', 'Please select a dataset.');
  }
}

// Submit the form
function chado_search_snp_genotype_search_form_submit ($form, &$form_state) {

  // If there is gene model, convert it into positions first
  $gene_model = $form_state->getValue('gene_model');
  if ($gene_model) {
    $sql = "SELECT feature_id FROM {feature} WHERE lower(name) = :name OR lower(uniquename) = :uniquename";
    $feature_id = chado_search_query($sql, array(":name" => strtolower($gene_model), ":uniquename" => strtolower($gene_model)), 'chado')->fetchField();
    if (!$feature_id) {
      $form_state->setErrorByName('gene_model', 'Gene model not found.');
    }
    $sql = "SELECT srcfeature_id, fmin, fmax FROM {featureloc} WHERE feature_id = :feature_id";
    $alignment = chado_search_query($sql, array(":feature_id" => $feature_id), 'chado')->fetchObject();
    if (!$alignment) {
      $form_state->setErrorByName('gene_model', 'Gene model\'s position not available.');
    }
    $form_state->setValue('srcfeature_id', $alignment->srcfeature_id);
    if ($form_state->getValue('range')) {
      $form_state->setValue('srcfmin', $alignment->fmin - $form_state->getValue('range'));
      $form_state->setValue('srcfmax', $alignment->fmax + $form_state->getValue('range'));
    }
    else {
      $form_state->setValue('srcfmin', $alignment->fmin);
      $form_state->setValue('srcfmax', $alignment->fmax);
    }
  }

  // Get base sql
  $sql = "SELECT * FROM {chado_search_snp_genotype_cache} GL";
  $disableCols = "";

  // Get selected stocks
  $selStocks = $form_state->getValue('stock_uniquename'); //stocks from selection

  // Get selected organisms
  $selOrgs = $form_state->getValue('organism');

  // Convert selected ogranisms into stock selection ONLY IF no stock is selected
  $org_stocks = array();
  if (!key_exists('0', $selOrgs) && count($selOrgs) != 0 && (key_exists('0', $selStocks) || count($selStocks) == 0)) {
    $organisms = \Drupal::state()->get('chado_search_snp_genotype_search_organisms');
    $index = 0;
    foreach($selOrgs AS $o) {
      foreach($organisms[$o] AS $stk) {
        $org_stocks["o$index"] = $stk;
        $index ++;
      }
    }
    unset($selStocks['0']); // make sure organism will be filtered if no stock is selected
  }
  $selStocks += $org_stocks;

  // Add uploaded file into stock selection
  $file = $_FILES['files']['tmp_name']['stock_name_file'];
  $file_stocks = array();
  if ($file) {
    $handle = fopen($file, 'r');
    $index = 0;
    while ($line = fgets($handle)) {
      $name = trim($line);
      $file_stocks ["f$index"] = $name;
      $index ++;
    }
  }
  $selStocks += $file_stocks;

  $stocks = \Drupal::state()->get('chado_search_snp_genotype_search_stocks');
  // Finally, filter on stocks IF 'Any' is not selected or there is at least one stock to filter
  $notNullStocks = array();
  if (!key_exists('0', $selStocks) && count($selStocks) != 0) {
    foreach ($selStocks AS $s) {
      $id = array_search($s, $stocks); // Convert selected stock uniquename into stock_id
      if ($id !== FALSE) {
        $notNullStocks [] = $id;
      }
    }
    $where = array();
    $where [] = Sql::hstoreHasValue('genotypes', $notNullStocks);
    // Keep only the not null stocks to show
    $stock_keys = array_keys($stocks);
    foreach ($stock_keys AS $stock_id) {
      if (!in_array($stock_id, $notNullStocks)) {
        unset($stocks[$stock_id]);
      }
    }
  }
  asort($stocks);

  // Add conditions
  $where [] = Sql::selectFilter('project_name', $form_state, 'project_name');
  $where [] = Sql::textFilter('feature_uniquename', $form_state, 'feature_uniquename');

  // Filter the genome position
  $sub = array();
  $sub [] = Sql::selectFilter('genome', $form_state, 'genome');
  $sub [] = Sql::selectFilter('location', $form_state, 'landmark');
  $sub [] = Sql::betweenFilter('fmin', 'fmax', $form_state, 'fmin', 'fmax');
  if ($form_state->getValue('srcfeature_id')) {
    $sub [] = "landmark_feature_id = " . $form_state->getValue('srcfeature_id');
    $sub [] = "fmin >= " . $form_state->getValue('srcfmin') . " AND fmax <= " . $form_state->getValue('srcfmax');
  }
  $con = " WHERE ";
  for ($i = 0; $i < count($sub); $i ++) {
    if ($sub[$i] != "") {
      if ($i > 0 && $con != " WHERE ") {
        $con .= " AND ";
      }
      $con .= $sub[$i];
    }
  }
  if($con != " WHERE ") {
    $where [] = "GL.feature_id IN (SELECT feature_id FROM {chado_search_snp_genotype_location} $con)";
  }

  $result = Set::result()
  ->sql($sql)
  ->tableDefinitionCallback('chado_search_snp_genotype_search_table_definition')
  ->where($where)
  ->disableCols($disableCols)
  ->customDownload(array('chado_search_snp_genotype_search_download_hapmap*' => 'Hapmap', 'chado_search_snp_genotype_search_download_polymorphic' => 'Table (Polymorphic)'))
  ->hstoreToColumns('genotypes', $stocks, TRUE, 'stock')
  ->defaultOrder('split_part(location,\':\',1),split_part(split_part(location,\':\',2),\'..\',1)::int')
  ->rewriteCols('srcfeature_id=chado_search_snp_genotype_search_rewrite_srcfeature_id*');
  
  $genome = $form_state->getValue('genome');
  if ($genome) {
    $result->rewriteCols(';srcfeature_id=chado_search_snp_genotype_search_rewrite_assembly;location=chado_search_snp_genotype_search_rewrite_location*');
    SessionVar::setSessionVar('snp_genotype_search', 'genome', $genome);
  }
  
  $landmark = $form_state->getValue('location');
  if ($landmark) {
    SessionVar::setSessionVar('snp_genotype_search', 'landmark', $landmark);
  }
  $result->execute($form, $form_state);
}

function chado_search_snp_genotype_search_rewrite_stock_header($k, $v, $html = TRUE) {
  $h_link = chado_search_link_entity('stock', $k);
  $inames = chado_search_snp_genotype_search_get_stock_names($k);
  $display_names = '';
  while ($iname = $inames->fetchField()) {
    if ($iname != $v) {
      if ($html) {
        $display_names .= $iname . '<br>';
      }
      else {
        $display_names .= ' | ' . $iname;
      }
    }
  }
  $snames = chado_search_snp_genotype_search_get_stock_names($k, 'sample_name');
  while ($sname = $snames->fetchField()) {
    if ($sname != $v) {
      if ($html) {
        $display_names .= $sname . '<br>';
      }
      else {
        $display_names .= ' | ' . $sname;
      }
    }
  }
  if ($html) {
    if ($h_link) {
      return "<th><a href=$h_link>$v</a><br>$display_names</th>";
    }
    else {
      return "<th>$v<br>$display_names</th>";
    }
  }
  else {
    return $v . $display_names;
  }
}

function chado_search_snp_genotype_search_get_stock_names ($stock_id, $type = 'institutional_name') {
  return \Drupal::database()->query(
    "SELECT value
      FROM chado.stockprop
      WHERE type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = :name_type AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN'))
      AND stock_id = :stock_id",
      [':name_type' => $type, ':stock_id' => $stock_id]
  );
}

function chado_search_snp_genotype_search_rewrite_assembly ($val, $hyperlink = TRUE) {
  $genome = SessionVar::getSessionVar('snp_genotype_search', 'genome');
  $analysis_id = \Drupal::database()->query("SELECT analysis_id FROM chado.analysis WHERE name = :genome LIMIT 1", [':genome' => $genome])->fetchField();
  if ($hyperlink && $analysis_id) {
    $link = chado_search_link_analysis($analysis_id, $genome);
    return "<a href=$link>" . $genome . '</a>';
  }
  else {
    return $genome;
  }
}

function chado_search_snp_genotype_search_rewrite_location ($obj) {
  $landmark = SessionVar::getSessionVar('snp_genotype_search', 'landmark');
  if ($landmark) {
    return \Drupal::database()->query("SELECT location FROM chado.chado_search_snp_genotype_location WHERE feature_id = :feature_id AND landmark = :landmark LIMIT 1", [':feature_id' => $obj->feature_id, ':landmark' => $landmark])->fetchField();
  }
  else {
    $genome = SessionVar::getSessionVar('snp_genotype_search', 'genome');
    return \Drupal::database()->query("SELECT location FROM chado.chado_search_snp_genotype_location WHERE feature_id = :feature_id AND genome = :genome LIMIT 1", [':feature_id' => $obj->feature_id, ':genome' => $genome])->fetchField();
  }
}

function chado_search_snp_genotype_search_rewrite_srcfeature_id ($obj, $hyperlink = TRUE) {
  $assembly =\Drupal::database()->query(
      "SELECT AF.analysis_id, (SELECT name FROM chado.analysis WHERE analysis_id = AF.analysis_id) 
        FROM chado.analysisfeature AF 
        WHERE feature_id = :srcfeature_id AND analysis_id IN (SELECT analysis_id FROM chado.analysisprop WHERE value = 'whole_genome') LIMIT 1", 
      [':srcfeature_id' => $obj->srcfeature_id])
    ->fetchObject();
    if ($hyperlink) {
    $link = chado_search_link_analysis($assembly->analysis_id, $assembly->name);
    return "<a href=$link>" . $assembly->name . '</a>';
  }
  else {
    return $assembly->name;
  }
}

/*************************************************************
 * Build the search result table
 */
// Define the result table
function chado_search_snp_genotype_search_table_definition () {
  $headers = array(
    //'array_name:s' => 'Array ID',
    'feature_name:s:chado_search_link_feature:feature_id' => 'Marker',
    'srcfeature_id' => 'Assembly',
    'location::chado_search_link_jbrowse:srcfeature_id,location' => 'Location',
    'allele' => 'Allele',
    'genotypes' => 'Genotypes'
  );
  return $headers;
}

// User defined: Populating the landmark for selected organism
function chado_search_snp_genotype_search_ajax_location ($val) {
  $sql = "SELECT distinct landmark FROM {chado_search_snp_genotype_location} WHERE genome = :genome ORDER BY landmark";
  return chado_search_bind_dynamic_select(array(':genome' => $val), 'landmark', $sql);
}

function chado_search_snp_genotype_search_ajax_dynamic_organism ($val) {
  if ($val && chado_search_table_exists('chado_search_snp_genotype_cache_project', 'chado')) {
    $val = is_array($val) ? $val : [$val];
    $sql = "SELECT distinct organism FROM {chado_search_snp_genotype_cache_project} WHERE project_name IN (:project_name[]) ORDER BY organism";
    return chado_search_bind_dynamic_select(array(':project_name[]'=> $val), 'organism', $sql);
  }
}

function chado_search_snp_genotype_search_ajax_dynamic_stock ($val) {
  $val = is_array($val) ? $val : [$val];
  if ($val && chado_search_table_exists('chado_search_snp_genotype_cache_project', 'chado')) {
    $orgs = isset($_POST['organism']) ? $_POST['organism'] : array();
    foreach($orgs AS $idx => $o) {
      if ($o == '0') {
        unset ($orgs[$idx]);
      }
    }
    if (count($orgs) > 0) {
      $sql = "SELECT distinct stock_uniquename FROM {chado_search_snp_genotype_cache_project} WHERE project_name IN (:project_name[]) AND organism IN (:organism[]) ORDER BY stock_uniquename";
      return chado_search_bind_dynamic_select(array(':project_name[]'=> $val, ':organism[]' => $orgs), 'stock_uniquename', $sql);
    }
    else {
      $sql = "SELECT distinct stock_uniquename FROM {chado_search_snp_genotype_cache_project} WHERE project_name IN (:project_name[]) ORDER BY stock_uniquename";
      return chado_search_bind_dynamic_select(array(':project_name[]'=> $val), 'stock_uniquename', $sql);
    }
  }
}

function chado_search_snp_genotype_search_get_rewrite_callbacks() {
  $rewriteCols = SessionVar::getSessionVar('snp_genotype_search', 'rewrite-columns');
  $rewriteCallback = array();
  $passObj = FALSE;
  if ($rewriteCols) {
    $rwcols = explode(';', $rewriteCols);
    foreach ($rwcols AS $rwc) {
      $rewrite = explode('=', $rwc);
      $func_name = $rewrite[1];
      if (preg_match('/\*$/', $func_name)) {
        $func_name = trim($func_name, '*');
        $passObj = TRUE;
      }
      if (count($rewrite) == 2 && function_exists($func_name) ) {
        $rewriteCallback[$rewrite[0]] = $func_name;
      }
    }
  }
  return $rewriteCallback;
}
/**
 * Custom download only polymorphic data
 */
function chado_search_snp_genotype_search_download_polymorphic ($handle, $result, $sql, $total_items, $progress_var, $headers, $hstoreCol, $hstoreToColumns) {
  set_time_limit(6000);
  fwrite($handle, "\"#\",");
  $col = 0;
  foreach ($headers AS $k => $v) {
    // handle the hstore column
    if ($k == $hstoreCol) {
      $counter_hs = 0;
      $total_hs = count($hstoreToColumns['data']);
      foreach ($hstoreToColumns['data'] AS $hsk => $hsv) {
        $hsv = chado_search_snp_genotype_search_rewrite_stock_header($hsk, $hsv, FALSE);
        fwrite($handle, "\"". $hsv . "\"");
        if ($counter_hs < $total_hs - 1) {
          fwrite($handle, ",");
        }
        $counter_hs ++;
      }
    }
    else {
      fwrite($handle, "\"". $v . "\"");
    }
    $col ++;
    if ($col < count($headers)) {
      fwrite($handle, ",");
    } else {
      fwrite($handle, "\n");
    }
  }
  $progress = 0;
  $counter = 1;
  $line_no = 1;
  $rewriteCallbacks = chado_search_snp_genotype_search_get_rewrite_callbacks();
  while ($row = $result->fetchObject()) {
    $current = round ($counter / $total_items * 100);
    if ($current != $progress) {
      $progress = $current;
      \Drupal::state()->set($progress_var, $progress);
    }
    $line = "\"$line_no\",";
    $col = 0;
    $total_hs = count($hstoreToColumns['data']);
    $polymorphic = $total_hs == 1 ? TRUE : FALSE;
    $gtype = NULL;
    foreach ($headers AS $k => $v) {
      $value = $row->$k;
      if (key_exists($k, $rewriteCallbacks)) {
        $rewriteFunc = $rewriteCallbacks[$k];
        $value = $rewriteFunc($row, FALSE);
      }
      if ($k == $hstoreCol) {
        $values = chado_search_hstore_to_assoc($value);
        $counter_hs = 0;
        foreach ($hstoreToColumns['data'] AS $hsk => $hsv) {
          $display_val = key_exists($hsk, $values) ? $values[$hsk] : '';
          $gtype = $gtype == NULL ? $display_val : $gtype;
          if ($display_val != '' && $display_val != '-' && $display_val != '-|-' && $display_val != '- | -' && $gtype != '' && $gtype != '-' && $gtype != '-|-' && $gtype != '- | -' && $display_val != $gtype) {
            $polymorphic = TRUE;
          }
          $line .= '"' . str_replace('"', '""', $display_val) . '"';
          if ($counter_hs < $total_hs - 1) {
            $line .= ",";
          }
          $counter_hs ++;
        }
      }
      else {
        $line .= '"' . str_replace('"', '""', $value) . '"';
      }
      $col ++;
      if ($col < count($headers)) {
        $line .= ",";
      } else {
        $line .= "\n";
      }
    }
    if ($polymorphic) {
      fwrite($handle, $line);
      $line_no ++;
    }
    $counter ++;
  }
}

/**
 * Custom download in Hapmap format
 */
function chado_search_snp_genotype_search_download_hapmap (&$path, $result, $sql, $total_items, $progress_var, $headers, $hstoreCol, $hstoreToColumns) {
  $path = str_replace('.csv', '.zip', $path);
  $file = '/tmp/hapmap_' . time();
  $handle = fopen($file , 'w');
  set_time_limit(6000);
  $col = 0;
  fwrite($handle, "rs#\talleles\tchrom\tpos\tstrand\tassembly#\tcenter\tprotLSID\tassayLSID\tpanelLSID\tQCcode\t");
  foreach ($headers AS $k => $v) {
    // handle the hstore column
    if ($k == $hstoreCol) {
      $counter_hs = 0;
      $total_hs = count($hstoreToColumns['data']);
      foreach ($hstoreToColumns['data'] AS $hsk => $hsv) {
        $hsv = chado_search_snp_genotype_search_rewrite_stock_header($hsk, $hsv, FALSE);
        fwrite($handle, $hsv);
        if ($counter_hs < $total_hs - 1) {
          fwrite($handle, "\t");
        }
        $counter_hs ++;
      }
      if ($col < count($headers) - 1) {
        fwrite($handle, "\t");
      }
    }
    $col ++;
  }
  fwrite($handle, "\n");
  $progress = 0;
  $counter = 1;
  $rewriteCallbacks = chado_search_snp_genotype_search_get_rewrite_callbacks();
  while ($row = $result->fetchObject()) {
    $current = round ($counter / $total_items * 100);
    if ($current != $progress) {
      $progress = $current;
      \Drupal::state()->set($progress_var, $progress);
    }
    $col = 0;
    $line = '';
    $total_hs = count($hstoreToColumns['data']);
    
    $gtype = NULL;
    $rs = NULL;
    $alleles = NULL;
    $chrom = NULL;
    $pos = NULL;
    $na = 'NA';
    $assembly = NULL;
    foreach ($headers AS $k => $v) {
      $value = $row->$k;
      if (key_exists($k, $rewriteCallbacks)) {
        $rewriteFunc = $rewriteCallbacks[$k];
        $value = $rewriteFunc($row, FALSE);
      }
      if ($k == $hstoreCol) {
        $values = chado_search_hstore_to_assoc($value);
        $counter_hs = 0;
        foreach ($hstoreToColumns['data'] AS $hsk => $hsv) {
          $display_val = key_exists($hsk, $values) ? $values[$hsk] : '';
          $gtype = $gtype == NULL ? $display_val : $gtype;
          $line .= $display_val;
          if ($counter_hs < $total_hs - 1) {
            $line .= "\t";
          }
          $counter_hs ++;
        }
      }
      else if ($k == 'feature_name') {
        $rs = $value;
      }
      else if ($k == 'location') {
        $token_value = explode(':', $value);
        $chrom = $token_value [0];
        if (isset($token_value[1])) {
          $pos = explode('..', $token_value[1])[0];
        }
      }
      else if ($k == 'allele') {
        $alleles = $value;
      }
      else if ($k == 'srcfeature_id') {
        $assembly = $value;
      }
      $col ++;
    }

    fwrite($handle, "$rs\t$alleles\t$chrom\t$pos\t$na\t$assembly\t$na\t$na\t$na\t$na\t$na\t" . $line . "\n");

    $counter ++;
  }
  fclose($handle);
  $zip = new ZipArchive();
  $zip->open($path, ZipArchive::CREATE| ZIPARCHIVE::OVERWRITE);
  $zip->addFile($file, 'hapmap.txt');
  $zip->close();
  unlink($file);
}

/**
 * Check cache MView and populate its data when 'drush csrun' is issued
 */
function chado_search_snp_genotype_search_drush_run() {

  print "Checking Required MViews...\n";
  chado_search_snp_genotype_cache_mview();
  print "Populating chado_search_snp_genotype_cache...\n";
  $exist_search = chado_search_table_exists('chado_search_snp_genotype_search', 'chado');
  $exist_cache = chado_search_table_exists('chado_search_snp_genotype_cache', 'chado');
  if ($exist_search && $exist_cache) {
    // Populate stocks cache
    chado_search_snp_genotype_cache_get_stocks();
    // Populate the cache table
    $sql = "SELECT * FROM chado_search_snp_genotype_search";
    $results = chado_search_query($sql, array(), 'chado');
    while ($r = $results->fetchObject()) {
      $exists =
      chado_search_query(
          "SELECT feature_id, project_id FROM chado_search_snp_genotype_cache WHERE feature_id = :feature_id AND project_id = :project_id",
          array(':feature_id' => $r->feature_id, ':project_id' => $r->project_id), 'chado')->fetchField();
          // Insert if not exist
          if (!$exists) {
            $sql =
            "INSERT INTO chado_search_snp_genotype_cache
               (feature_id,
                feature_name,
                feature_uniquename,
                array_id,
                array_name,
                srcfeature_id,
                location,
                project_id,
                project_name,
                allele,
                genotypes)
              VALUES (
                $r->feature_id,
                '$r->feature_name',
                '$r->feature_uniquename',
                '$r->array_id',
                '$r->array_name',
                (SELECT max(landmark_feature_id) FROM (SELECT * FROM chado_Search_snp_genotype_location WHERE feature_id = $r->feature_id ORDER BY char_length(landmark), landmark) T),
                (SELECT max(location) FROM (SELECT * FROM chado_Search_snp_genotype_location WHERE feature_id = $r->feature_id ORDER BY char_length(landmark), landmark) T),
                $r->project_id,
                '$r->project_name',
                '$r->allele',
                '\"$r->stock_id\" => \"$r->genotype\"'
              )";
              chado_search_query($sql, array(), 'chado');
          }
          // Update if exists
          else {
              // Only update when the stock's genotype does not exist
              $stock_exists =
                chado_search_query(
                  "SELECT feature_id FROM chado_search_snp_genotype_cache WHERE feature_id = :feature_id AND project_id = :project_id AND exist(genotypes, '" . $r->stock_id . "')",
                  array(':feature_id' => $r->feature_id, ':project_id' => $r->project_id), 'chado')->fetchField();
              if (!$stock_exists) {
                $sql =
                  "UPDATE chado_search_snp_genotype_cache
                   SET genotypes = genotypes || '\"$r->stock_id\" => \"$r->genotype\"' :: hstore
                   WHERE feature_id = $r->feature_id
                   AND project_id = $r->project_id
                  ";
                chado_search_query($sql, array(), 'chado');
              }
          }
    }
    // Populate the project cache table
    print "Populating chado_search_snp_genotype_cache_project...\n";
    chado_search_query("TRUNCATE chado_search_snp_genotype_cache_project", array(), 'chado');
    $sql =
      "INSERT INTO chado_search_snp_genotype_cache_project
         (SELECT DISTINCT project_name, organism, stock_uniquename FROM chado_search_snp_genotype_search)";
    chado_search_query($sql, array(), 'chado');
  }
}

/**
 * Before creating the cache table, we need to know how many stocks are there so we can use them to create columns
 */
function chado_search_snp_genotype_cache_get_stocks() {

  // Get all distinct stocks in the search mview
  $sql = "SELECT DISTINCT stock_id, stock_uniquename FROM {chado_search_snp_genotype_search}";
  $results = chado_search_query($sql, array(), 'chado');
  $stocks = array(); // array(stock_id, stock_uniquename)
  $organisms = array(); // array(organism, stock_uniquename). For converting a list of organisms into corresponding stocks
  while ($stock = $results->fetchObject()) {
    $stocks [$stock->stock_id] = $stock->stock_uniquename;
    $organism =
    chado_search_query(
        "SELECT (SELECT genus || ' ' || species FROM {organism} WHERE organism_id = S.organism_id) AS organism
              FROM {stock} S
              WHERE stock_id = :stock_id",
        array(':stock_id' => $stock->stock_id), 'chado')->fetchObject();
        if (isset($organisms[$organism->organism])) {
          array_push($organisms[$organism->organism], $stock->stock_uniquename);
        }
        else {
          $organisms[$organism->organism] = array($stock->stock_uniquename);
        }
  }
  // Store all stocks and their organism mapping for the search
  \Drupal::state()->set('chado_search_snp_genotype_search_stocks', $stocks);
  \Drupal::state()->set('chado_search_snp_genotype_search_organisms', $organisms);
  return $stocks;
}

/**
 * Create cache MView
 */
function chado_search_snp_genotype_cache_mview() {

  // Check if the cache table exists
  $exist_search = chado_search_table_exists('chado_search_snp_genotype_search', 'chado');
  $exist_cache = chado_search_table_exists('chado_search_snp_genotype_cache', 'chado');
  $exist_cache_project = chado_search_table_exists('chado_search_snp_genotype_cache_project', 'chado'); // A table that stores DISTINCT project, organism, stock_uniquename for fast dropdown population

  if ($exist_search) {
    // Make sure hstore extension is enabled
    print "Checking Postgres for hstore extension...\n";
    $sql = "SELECT extname FROM pg_extension e LEFT JOIN pg_namespace n ON n.oid = e.extnamespace  WHERE extname = 'hstore' AND nspname = 'chado'";
    $ext_exists = chado_search_query($sql)->fetchField();
    if (!$ext_exists) {
      chado_search_query("CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA chado");
    }
    // if chado_search_snp_genotype_cache not exists, create it
    if (!$exist_cache) {
      $stocks = chado_search_snp_genotype_cache_get_stocks();
      if (count($stocks) != 0) {
        print "Create MView chado_search_snp_genotype_cache...\n";
        // Create the cache table
        $sql =
        "CREATE TABLE IF NOT EXISTS chado_search_snp_genotype_cache (
             feature_id integer,
             feature_name varchar(510),
             feature_uniquename text,
             array_id text,
             array_name text,
             srcfeature_id integer,
             location varchar(510),
             project_id integer,
             project_name varchar(255),
             allele text,
             genotypes hstore,
             CONSTRAINT feature_project_uniq UNIQUE(feature_id, project_id))";
        chado_search_query($sql, array(), 'chado');
      }
    }

    // if chado_search_snp_genotype_cache_project not exists, create it
    if (!$exist_cache_project) {
      print "Create MView chado_search_snp_genotype_cache_project...\n";
      $sql =
      "SELECT DISTINCT project_name, organism, stock_uniquename
       INTO chado_search_snp_genotype_cache_project
       FROM {chado_search_snp_genotype_search}";
      chado_search_query($sql, array(), 'chado');
    }
  }
}
