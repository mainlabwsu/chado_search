<?php

use Drupal\chado_search\Core\Set;
use Drupal\chado_search\Core\Sql;

/*************************************************************
 * hook_form()  
 */
function chado_search_tripal_entity_search_form ($form) {
  $form->addSelectFilter(
      Set::selectFilter()
      ->id('bundle')
      ->title('Type')
      ->column('label')
      ->table('tripal_bundle')
      ->newLine()
      );
  $form->addTextFilter(
      Set::textFilter()
      ->id('title')
      ->title('Title')
  );
  $form->addSubmit();    
  return $form;
}

/*************************************************************
 * hook_form_submit()
 */
 function chado_search_tripal_entity_search_form_submit ($form, &$form_state) {
  $sql = "SELECT id, title, bundle FROM tripal_entity";
  $where = array();
  $where [] = Sql::textFilter('title', $form_state, 'title');
  
  $type = 
  db_select('tripal_bundle', 'tb')
  ->fields('tb', array('name'))
  ->condition('label', $form_state->getValue('bundle'))
  ->execute()
  ->fetchField();
  if ($type) {
    $where [] = "bundle = '$type'";
  }
  Set::result()
  ->sql($sql)
  ->where($where)
  ->tableDefinitionCallback('chado_search_tripal_entity_search_table_definition')
  ->rewriteCols('bundle=chado_search_tripal_entity_search_rewrite_bundle')
  ->execute($form, $form_state);
}

// Define the result table
function chado_search_tripal_entity_search_table_definition () {
  $headers = array(
    'bundle:s' => 'Type',
    'title:s:chado_search_tripal_entity_search_link_entity:id' => 'Title'
  );
  return $headers;
}

function chado_search_tripal_entity_search_link_entity ($id) {
  return 'bio_data/' . $id;
}

function chado_search_tripal_entity_search_rewrite_bundle($bundle_id) {
  $type = 
  db_select('tripal_bundle', 'tb')
  ->fields('tb', array('label'))
  ->condition('name', $bundle_id)
  ->execute()
  ->fetchField();
  return $type;
}