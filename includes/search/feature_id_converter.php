<?php

use Drupal\chado_search\Core\Set;
use Drupal\chado_search\Core\Sql;

/*************************************************************
 * hook_form()  
 */
function chado_search_feature_id_converter_form ($form) {
  $form->addLabeledFilter(
      Set::labeledFilter()
      ->id('feature_id_converter_synonym')
      ->title('Synonym')
      ->newline()
      );
  $form->addFile(
      Set::file()
      ->title('Upload Synonyms')
      ->id('feature_id_converter_file_inline')
      ->description('Upload a file with feature synonyms seperated by a new line. (limit to 100,000 lines)')
      ->labelWidth(160)
      );
  $form->addSubmit();
  $form->addReset();
  
  $form->addFieldset(
      Set::fieldset()
      ->id('feature_id_converter')
      ->startWidget('feature_id_converter_synonym')
      ->endWidget('reset')
      ->description($desc)
      );
  
  return $form;
}

/*************************************************************
 * hook_form_submit()
 */
 function chado_search_feature_id_converter_form_submit ($form, &$form_state) {
   
   $file = $_FILES['files']['tmp_name']['feature_id_converter_file_inline'];
   $limit = 100000;
   $input = $form_state->getValue('feature_id_converter_synonym');
   
   if ($file) {
     $input = '';
     $handle = fopen($file, 'r');
     $counter = 1;  
     
     while ($line = fgets($handle)) {
       $value = trim($line);
       if ($value) {
         $input .= $value . "\t";
       }
       if ($limit && $counter >= $limit) {
         break;
       }
       $counter ++;
     }
     fclose($handle);
   }
   
   if ($input) {
     $sql = "
        SELECT DISTINCT
          id,
          SYN.feature_id,
          SYN.name, 
          SYN.uniquename, 
          SYN.organism_id,
          SYN.organism,
          SYN.type
        FROM (
          SELECT string_to_table('" . str_replace("'", "''", trim($input, "\t")) . "', E'\t') AS id FROM (SELECT * FROM chado.analysis LIMIT 1) T
          ) BASE 
          LEFT JOIN (
            SELECT 
              S.name AS synonym,
              FS.feature_id,
              F.name,
              F.uniquename,
              F.organism_id,
              (SELECT genus || ' ' || species FROM chado.organism WHERE organism_id = F.organism_id) AS organism,
              (SELECT name FROM chado.cvterm WHERE cvterm_id = F.type_id) AS type
            FROM chado.synonym S
            INNER JOIN chado.feature_synonym FS ON FS.synonym_id = S.synonym_id
            INNER JOIN chado.feature F ON F.feature_id = FS.feature_id
          ) SYN ON SYN.synonym = BASE.id
     ";
  
     $where = array();
     //$where [] = Sql::textFilter('name', $form_state, 'title');
     Set::result()
     ->sql($sql)
     ->where($where)
     ->tableDefinitionCallback('chado_search_feature_id_converte_table_definition')
     ->defaultOrder('id')
     ->execute($form, $form_state);
   }
}


function chado_search_feature_id_converte_table_definition () {
  $headers = array(
    'id:s' => 'Synonym',
    'feature_id:s' => 'feature_id',
    'name:s:chado_search_link_feature:feature_id' => 'Name',
    'uniquename:s:chado_search_link_feature:feature_id' => 'Uniquename',
    'organism:s:chado_search_link_organism:organism_id' => 'Species',
    'type:s' => 'Type',
    );
  return $headers;
}
