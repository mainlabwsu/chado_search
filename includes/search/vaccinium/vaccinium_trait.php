<?php

use Drupal\chado_search\Core\Set;
use Drupal\chado_search\Core\Sql;

/*************************************************************
 * Search form, form validation, and submit function
 */
// Search form
function chado_search_trait_form ($form) {
  $table = "
    (SELECT
      TC.name AS category
    FROM chado.cvterm TC
    INNER JOIN chado.cv CV on CV.cv_id = TC.cv_id
    WHERE CV.name ~ 'trait_class_PTO$'
    ORDER BY TC.name) T
  ";
  $form->addSelectFilter(
      Set::selectFilter()
      ->id('category')
      ->title('Category')
      ->table($table)
      ->column('category')
      ->newline()
      );
  $form->addTextFilter(
      Set::textFilter()
      ->id('keyword')
      ->title('Keyword')
      ->newline()
      );
  $form->addSubmit();
  $form->addReset();
  return $form;
}

// Submit the form
function chado_search_trait_form_submit ($form, &$form_state) {
    $where = array();
  // Get base sql
  $sql = chado_search_trait_base_query();
  $where [] = Sql::selectFilter('category', $form_state, 'cat.name');
  $where [] = Sql::textFilterOnMultipleColumns('keyword', $form_state, array('C.name', 'A.value', 'C.definition'));
  $where [] = "cv.name LIKE '%_trait_ontology'";
  $where [] = "(CAT.cv IS NULL OR CAT.cv NOT LIKE '%__trait_class_CO')";
  Set::result()
    ->sql($sql)
    ->where($where)
    ->tableDefinitionCallback('chado_search_trait_table_definition')
    ->defaultOrder('category,abbreviation')
    ->execute($form, $form_state);
}

function chado_search_trait_base_query () {
  $sql = "
    SELECT 
      C.cvterm_id,
      CAT.name AS category, 
      C.name AS trait, 
      A.value AS abbreviation, 
      C.definition 
    FROM cvterm C
    INNER JOIN cv ON C.cv_id = cv.cv_id
    LEFT JOIN (
      SELECT cvterm_id, value
      FROM cvtermprop
      WHERE type_id = (
        SELECT cvterm_id
        FROM cvterm C
        INNER JOIN cv on CV.cv_id = C.cv_id
        WHERE C.name = 'abbreviation' AND cv.name = 'MAIN')
    ) A on A.cvterm_id = C.cvterm_id
    LEFT JOIN (
        SELECT C1.name, C1.cvterm_id, CR.subject_id, (SELECT name FROM cv WHERE cv_id = C1.cv_id) AS cv
      FROM cvterm C1
      INNER JOIN cvterm_relationship CR on CR.object_id = C1.cvterm_id
      INNER JOIN cvterm C2 on C2.cvterm_id = CR.type_id
      INNER JOIN cv CV2 on CV2.cv_id = C2.cv_id
      WHERE CV2.name = 'relationship' AND C2.name = 'is_a'
    ) CAT on CAT.subject_id = C.cvterm_id
      ";
  return $sql;
}
/*************************************************************
 * Build the search result table
*/
// Define the result table
function chado_search_trait_table_definition () {
  $headers = array(
      'category:s' => 'Category',
      'abbreviation:s' => 'Abbreviation',
      'trait:s:chado_search_trait_link_trait:cvterm_id' => 'Trait',
      'definition:s' => 'Definition'
  );
  return $headers;
}

// Define call back to link the trait to its  node for the result table
function chado_search_trait_link_trait ($cvterm_id) {
  return "/display/trait/$cvterm_id";
}
