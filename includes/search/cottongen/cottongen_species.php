<?php

use Drupal\chado_search\Core\Set;
use Drupal\chado_search\Core\Sql;

/*************************************************************
 * Search form, form validation, and submit function
 */
// Search form
function chado_search_species_form ($form) {
  $form->addSelectFilter(
      Set::selectFilter()
      ->id('genome')
      ->title('Genome Group')
      ->column('genome')
      ->table('chado_search_species')
      ->labelWidth(120)
  );
  $form->addSelectFilter(
      Set::selectFilter()
      ->id('species')
      ->title('Species')
      ->column('organism')
      ->table('chado_search_species')
  );
  $form->addSubmit();
  $form->addReset();
  $form->addFieldset(
      Set::fieldset()
      ->id('species_summary')
      ->startWidget('genome')
      ->endWidget('reset')
  );
  return $form;
}

// Submit the form
function chado_search_species_form_submit ($form, &$form_state) {
  // Get base sql
  $sql = "
    SELECT * FROM {chado_search_species} CSS 
    LEFT JOIN (
      SELECT organism_id, value AS genome_name 
      FROM organismprop OP 
      WHERE type_id = 
        (SELECT cvterm_id FROM cvterm WHERE name = 'genome_name' 
         AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
        ) GG ON GG.organism_id = CSS.organism_id";
  $where = array();
  $where [] = Sql::selectFilter('genome', $form_state, 'genome');
  $where [] = Sql::selectFilter('species', $form_state, 'organism');
  $where [] = 'genome IS NOT NULL';
  Set::result()
    ->sql($sql)
    ->where($where)
    ->tableDefinitionCallback('chado_search_species_table_definition')
    ->defaultOrder('haploid_chromosome_number NULLS LAST,genome_name')
    ->execute($form, $form_state);
}

/*************************************************************
 * Build the search result table
*/
// Define the result table
function chado_search_species_table_definition () {
  $headers = array(
      'organism:s:chado_search_link_organism:organism_id' => 'Species',
      'genome:s' => 'Genome Group',
      'genome_name:s' => 'Genome',
      'haploid_chromosome_number:s' => 'Haploid <i>n</i>',
      'geographic_origin:s' => 'Geographic Origin',
      'num_germplasm:s' => 'Num Germplasm',
      'num_sequences:s' => 'Num Sequences',
      'num_libraries:s' => 'Num Libraries'
  );
  return $headers;
}