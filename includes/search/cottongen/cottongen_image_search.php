<?php

use Drupal\chado_search\Core\Set;
use Drupal\chado_search\Core\Sql;

/*************************************************************
 * Search form, form validation, and submit function
 */
// Search form
function chado_search_image_search_form ($form) {
    $form->addSelectFilter(
        Set::selectFilter()
        ->id('keyword_type')
        ->title('Type')
        ->column('keyword_type')
        ->table('chado_search_image_search')
        ->cache(TRUE)
        );
  $form->addTextFilter(
      Set::textFilter()
      ->id('keyword')
      ->title('')
      ->newLine()
      ->labelWidth(1)
  );
  $form->addTextFilter(
      Set::textFilter()
      ->id('legend')
      ->title('Legend')
      ->newLine()
      );
  $form->addSelectOptionFilter(
      Set::selectOptionFilter()
      ->id('project_op')
      ->title('Dataset')
      ->options(array('-', 'in', 'not in'))
      ->required(TRUE)
      ->ajax(array(
          'callback' => 'chado_search_image_search_ajax_project',
          'wrapper' => 'chado_search_form',
          'effect' => 'fade')
          )
      );
  $form_state = $form->form_state;
  if ($form_state->getValues()) {
      if ($form_state->getValue('project_op') == 'in' ) {
          $form->addSelectFilter(
              Set::selectFilter()
              ->id('project')
              ->column('project')
              ->table('chado_search_image_search')
              ->labelWidth(1)
              ->multiple(TRUE)
              ->cache(TRUE)
              ->newLine()
              );
      }
      else if ($form_state->getValue('project_op') == 'not in' ) {
          $form->addSelectOptionFilter(
              Set::selectOptionFilter()
              ->id('project')
              ->newLine()
              );
      }
  }
  $form->addSubmit();
  $form->addReset();
  $form->addFieldset(
      Set::fieldset()
      ->id('image_search')
      ->startWidget('keyword_type')
      ->endWidget('reset')
  );
  return $form;
}

// Submit the form
function chado_search_image_search_form_submit ($form, &$form_state) {
  // Get base sql
  $sql = chado_search_image_search_base_query();
  // Add conditions
  $where = array();
  $where [] = Sql::selectFilter('keyword_type', $form_state, 'keyword_type');
  $where [] = Sql::textFilter('keyword', $form_state, 'keyword');
  $where [] = Sql::textFilter('legend', $form_state, 'legend');
  $project_op = $form_state->getValue('project_op');
  $project_con = Sql::selectFilter('project', $form_state, 'project');
  if ($project_op == 'in') {
      if ($project_con) {
        $where [] = $project_con;
      }
      else {
          $where [] = 'project IS NOT NULL';
      }
  }
  else if ($project_op == 'not in') {
      if ($project_con) {
        $where [] = str_replace('project = ', 'project != ', $project_con);
      }
      else {
          $where [] = 'project IS NULL';
      }
  }
  $groupby = "eimage_id:chado_search_image_search";
  Set::result()
    ->sql($sql)
    ->where($where)
    ->tableDefinitionCallback('chado_search_image_search_table_definition')
    ->groupby($groupby)
    ->execute($form, $form_state);
}

/*************************************************************
 * SQL
*/
// Define query for the base table. Do not include the WHERE clause
function chado_search_image_search_base_query() {
  $query =
    "SELECT * FROM {chado_search_image_search}";
  return $query;
}

/*************************************************************
 * Build the search result table
*/
// Define the result table
function chado_search_image_search_table_definition () {
  $headers = array(
    'eimage_data:s:chado_search_link_eimage:eimage_id' => 'File',
    'legend:s' => 'Legend'
  );
  return $headers;
}

function chado_search_image_search_ajax_project($form, &$form_state) {
    return $form;
}