<?php

use Drupal\chado_search\Core\Set;
use Drupal\chado_search\Core\Sql;

/*************************************************************
 * Search form, form validation, and submit function
 */
// Search form
function chado_search_featuremap_form ($form) {
  $form->addSelectFilter(
      Set::selectFilter()
      ->id('organism')
      ->title('Species')
      ->column('organism')
      ->table('chado_search_featuremap')
      ->labelWidth(163)
      ->cache(TRUE)
      ->newLine()
      );
  $form->addSelectFilter(
      Set::selectFilter()
      ->id('genome')
      ->title('Genome Group')
      ->column('genome')
      ->table('chado_search_featuremap')
      ->labelWidth(163)
      ->cache(TRUE)
      ->newLine()
      );
  $form->addSelectFilter(
      Set::selectFilter()
      ->id('map_type')
      ->title('Map Type')
      ->column('map_type')
      ->table('chado_search_featuremap')
      ->labelWidth(163)
      ->cache(TRUE)
      ->newLine()
      );
  $form->addSubmit();
  $form->addReset();
  return $form;
}

// Submit the form
function chado_search_featuremap_form_submit ($form, &$form_state) {
  // Get base sql
  $sql = "SELECT * FROM {chado_search_featuremap}";
  $where = array();
  $genus = key_exists('genus', $_GET) ? $_GET['genus'] : NULL;
  $species = key_exists('species', $_GET) ? $_GET['species'] : NULL;
  if ($genus) {
    $where [1] = "genus = '$genus'";
  }
  if ($species) {
    $where [2] = "species = '$species'";
  }
  $where [] = Sql::selectFilter('organism', $form_state, 'organism');
  $where [] = Sql::selectFilter('genome', $form_state, 'genome');
  $where [] = Sql::selectFilter('map_type', $form_state, 'map_type');
  Set::result()
    ->sql($sql)
    ->where($where)
    ->tableDefinitionCallback('chado_search_featuremap_table_definition')
    ->execute($form, $form_state);
}

/*************************************************************
 * Build the search result table
*/
// Define the result table
function chado_search_featuremap_table_definition () {
  $headers = array(
      'featuremap:s:chado_search_link_featuremap:featuremap_id' => 'Map Name',
      'genome:s' => 'Genome Group',
      'maternal_stock_uniquename:s:chado_search_link_stock:maternal_stock_id' => 'Maternal Parent',
      'paternal_stock_uniquename:s:chado_search_link_stock:paternal_stock_id' => 'Paternal Parent',
      'pop_size:s' => 'Pop Size',
      'pop_type:s' => 'Pop Type',
      'num_of_lg:s' => 'Num LG',
      'num_of_loci:s' => 'Num Loci',
      'num_of_qtl:s' => 'Num QTL'
  );
  return $headers;
}