<?php
// Create 'germplasm_search' MView
function chado_search_create_orthologs_mview() {
  $view_name = 'chado_search_orthologs';

  $schema = array (
  'table' => $view_name,
  'fields' => array (
    'l_analysis_id' => array(
      'type' => 'int',
      'not null' => TRUE,
    ),
    'l_genome' => array(
      'type' => 'varchar',
      'length' => 256,
      'not null' => FALSE,
    ),
    'l_landmark_id' => array(
      'type' => 'int',
      'not null' => TRUE,
    ),
    'l_landmark' => array(
      'type' => 'varchar',
      'length' => 256,
      'not null' => FALSE,
    ),
    'l_feature_id' => array(
      'type' => 'int',
      'not null' => TRUE,
    ),
    'l_feature' => array(
      'type' => 'varchar',
      'length' => 256,
      'not null' => FALSE,
    ),
    'l_organism_id' => array(
        'type' => 'int',
        'not null' => TRUE,
    ),
    'r_analysis_id' => array(
      'type' => 'int',
      'not null' => TRUE,
    ),
    'r_genome' => array(
      'type' => 'varchar',
      'length' => 256,
      'not null' => FALSE,
    ),
    'r_landmark_id' => array(
      'type' => 'int',
      'not null' => TRUE,
    ),
    'r_landmark' => array(
      'type' => 'varchar',
      'length' => 256,
      'not null' => FALSE,
    ),
    'r_feature_id' => array(
      'type' => 'int',
      'not null' => TRUE,
    ),
    'r_feature' => array(
      'type' => 'varchar',
      'length' => 256,
      'not null' => FALSE,
    ),
    'r_organism_id' => array(
        'type' => 'int',
        'not null' => TRUE,
    ),
  ),
);
  $sql = "
    SELECT DISTINCT * FROM (
    SELECT
        A1.analysis_id AS L_analysis_id,
        A1.name AS L_genome,
        L1.srcfeature_id AS L_landmark_id,
        (SELECT name FROM feature WHERE feature_id = L1.srcfeature_id) AS L_landmark,
        F1.feature_id AS L_feature_id,
        F1.name AS L_feature,
        F1.organism_id AS L_organism_id,
        A2.analysis_id AS R_analysis_id,
        A2.name AS R_genome,
        L2.srcfeature_id AS R_landmark_id,
        (SELECT name FROM feature WHERE feature_id = L2.srcfeature_id) AS R_landmark,
        F2.feature_id AS R_feature_id,
        F2.name AS R_feature,
        F2.organism_id AS R_organism_id
    FROM feature_relationship FR
    INNER JOIN cvterm V ON V.cvterm_id = FR.type_id
    INNER JOIN feature F1 ON F1.feature_id = FR.subject_id
    INNER JOIN featureloc L1 ON L1.feature_id = F1.feature_id
    INNER JOIN analysisfeature AF1 ON AF1.feature_id = L1.srcfeature_id
    INNER JOIN analysis A1 ON A1.analysis_id = AF1.analysis_id
    INNER JOIN feature F2 ON F2.feature_id = FR.object_id
    INNER JOIN featureloc L2 ON L2.feature_id = F2.feature_id
    INNER JOIN analysisfeature AF2 ON AF2.feature_id = L2.srcfeature_id
    INNER JOIN analysis A2 ON A2.analysis_id = AF2.analysis_id
    WHERE V.name IN ('orthologous_to', 'paralogous_to')
    AND F1.type_id IN (SELECT cvterm_id FROM cvterm WHERE name IN ('mRNA', 'gene'))
    AND F2.type_id IN (SELECT cvterm_id FROM cvterm WHERE name IN ('mRNA', 'gene'))
    UNION
    SELECT
        A2.analysis_id AS L_analysis_id,
        A2.name AS L_genome,
        L2.srcfeature_id AS L_landmark_id,
        (SELECT name FROM feature WHERE feature_id = L2.srcfeature_id) AS L_landmark,
        F2.feature_id AS L_feature_id,
        F2.name AS L_feature,
        F2.organism_id AS L_organism_id,
        A1.analysis_id AS R_analysis_id,
        A1.name AS R_genome,
        L1.srcfeature_id AS R_landmark_id,
        (SELECT name FROM feature WHERE feature_id = L1.srcfeature_id) AS R_landmark,
        F1.feature_id AS R_feature_id,
        F1.name AS R_feature,
        F1.organism_id AS R_organism_id
    FROM feature_relationship FR
    INNER JOIN cvterm V ON V.cvterm_id = FR.type_id
    INNER JOIN feature F1 ON F1.feature_id = FR.subject_id
    INNER JOIN featureloc L1 ON L1.feature_id = F1.feature_id
    INNER JOIN analysisfeature AF1 ON AF1.feature_id = L1.srcfeature_id
    INNER JOIN analysis A1 ON A1.analysis_id = AF1.analysis_id
    INNER JOIN feature F2 ON F2.feature_id = FR.object_id
    INNER JOIN featureloc L2 ON L2.feature_id = F2.feature_id
    INNER JOIN analysisfeature AF2 ON AF2.feature_id = L2.srcfeature_id
    INNER JOIN analysis A2 ON A2.analysis_id = AF2.analysis_id
    WHERE V.name IN ('orthologous_to', 'paralogous_to')
    AND F1.type_id IN (SELECT cvterm_id FROM cvterm WHERE name IN ('mRNA', 'gene'))
    AND F2.type_id IN (SELECT cvterm_id FROM cvterm WHERE name IN ('mRNA', 'gene'))
    ) T
  ";
  return array(array('view_name' => $view_name, 'schema' => $schema, 'sql' => $sql));
}
