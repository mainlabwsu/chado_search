<?php
// Create 'image_search' MView
function chado_search_create_image_search_mview() {
  $view_name = 'chado_search_image_search';

  $schema = array (
    'table' => $view_name,
    'fields' => array (
      'eimage_id' => array (
      'type' => 'int',
      ),
      'eimage_data' => array (
        'type' => 'text',
      ),
      'type' => array (
        'type' => 'text',
      ),
      'project_id' => array (
        'type' => 'int',
      ),
      'project' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'legend' => array (
        'type' => 'text',
      ),
      'keyword_type' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'keyword' => array (
        'type' => 'text',
      ),
    )
  );
  $sql = "
    SELECT
  I.eimage_id,
  eimage_data,
  TYPE.value AS type,
  P.project_id,
  P.name AS project,
  LEGEND.value AS legend,
  KEYWORDS.type AS keyword_type,
  KEYWORDS.keyword
FROM eimage I
LEFT JOIN eimageprop LEGEND ON I.eimage_id = LEGEND.eimage_id
AND LEGEND.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'legend' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
LEFT JOIN eimageprop TYPE ON I.eimage_id = TYPE.eimage_id
AND TYPE.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'image_type' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
LEFT JOIN project_image PI ON PI.eimage_id = I.eimage_id
LEFT JOIN project P ON P.project_id = PI.project_id
LEFT JOIN (
  SELECT eimage_id, 'species' AS type, species AS keyword
  FROM organism_image OI INNER JOIN organism O ON OI.organism_id = O.organism_id
  UNION
  SELECT eimage_id, 'germplasm' AS type, uniquename AS keyword
  FROM stock_image SI INNER JOIN stock S ON SI.stock_id = S.stock_id
  UNION
  SELECT eimage_id, 'marker' AS type, name AS keyword
  FROM feature_image FI INNER JOIN feature F ON FI.feature_id = F.feature_id
  WHERE F.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'genetic_marker'
  AND cv_id = (SELECT cv_id FROM cv WHERE name = 'sequence'))
  UNION
  SELECT eimage_id, 'MTL' AS type, name AS keyword
  FROM feature_image FI INNER JOIN feature F ON FI.feature_id = F.feature_id
  WHERE F.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'heritable_phenotypic_marker'
  AND cv_id = (SELECT cv_id FROM cv WHERE name = 'sequence'))
  UNION
  SELECT eimage_id, 'trait' AS type, V.name AS keyword
  FROM cvterm_image VI INNER JOIN cvterm V ON V.cvterm_id = VI.cvterm_id
  INNER JOIN cvterm_relationship CR ON CR.subject_id = V.cvterm_id
  INNER JOIN cvterm TYPE on TYPE.cvterm_id = CR.type_id
  WHERE TYPE.name = 'is_a'
  UNION
  SELECT eimage_id, 'descriptor' AS type, V.name AS keyword
  FROM cvterm_image VI INNER JOIN cvterm V ON V.cvterm_id = VI.cvterm_id
  INNER JOIN cvterm_relationship CR ON CR.subject_id = V.cvterm_id
  INNER JOIN cvterm TYPE on TYPE.cvterm_id = CR.type_id
  WHERE TYPE.name = 'belongs_to'
  UNION
  SELECT eimage_id, 'publication' AS type, uniquename AS keyword
  FROM pub_image PI INNER JOIN pub P ON PI.pub_id = P.pub_id
) KEYWORDS ON KEYWORDS.eimage_id = I.eimage_id
  ";
  return array(array('view_name' => $view_name, 'schema' => $schema, 'sql' => $sql));
}
