<?php
// Create 'germplasm_search_by_country' MView
function chado_search_create_germplasm_search_by_country_mview() {
  $view_name = 'chado_search_germplasm_search_by_country';

  $schema = array (
    'table' => $view_name,
    'fields' => array (
      'stock_id' => array (
        'type' => 'int'
      ),
      'name' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'uniquename' => array (
        'type' => 'text'
      ),
      'organism_id' => array (
        'type' => 'int'
      ),
      'organism' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'stock_type' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'country' => array (
        'type' => 'text'
      ),
      'origin_detail' => array (
        'type' => 'text'
      )
    )
  );
  $sql = "
  SELECT DISTINCT
    S.stock_id,
    S.name,
    S.uniquename,
    S.organism_id,
    O.genus || ' ' || o.species AS organism,
    V.name AS stock_type,
    (SELECT value FROM stockprop WHERE type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'origin_country' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN')) AND stock_id = S.stock_id) AS country,
        (SELECT value FROM stockprop WHERE type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'origin_detail' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN')) AND stock_id = S.stock_id) AS origin_detail
  FROM stock S
  INNER JOIN organism O ON O.organism_id = S.organism_id
  INNER JOIN cvterm V ON V.cvterm_id = S.type_id
  WHERE S.type_id <> (SELECT cvterm_id FROM cvterm WHERE name = 'sample' AND cv_id =(SELECT cv_id FROM cv WHERE name = 'MAIN'))
  ";
  return array(array('view_name' => $view_name, 'schema' => $schema, 'sql' => $sql));
}
