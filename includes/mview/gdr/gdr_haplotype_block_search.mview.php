<?php
// Create 'gene_search' MView
function chado_search_create_haplotype_block_search_mview() {
  // haplotype block seach mview
  $view_name1 = 'chado_search_haplotype_block_search';
  $schema1 =  array (
    'table' => $view_name1,
    'fields' => array (
      'stock_id' => array (
        'type' => 'text'
      ),
      'stock' => array (
        'type' => 'text'
      ),
      'hb_feature_id' => array (
        'type' => 'int'
      ),
      'haplotype_block' => array (
        'type' => 'text'
      ),
      'haplotype' => array (
        'type' => 'text'
      ),
      'values' => array (
        'type' => 'text'
      ),
      'marker_feature_ids' => array (
        'type' => 'text'
      ),
      'markers' => array (
        'type' => 'text'
      ),
      'description' => array (
        'type' => 'text'
      ),
      'project_id' => array(
        'type' => 'int'
      ),
      'project' => array(
        'type' => 'varchar',
        'length' => '255'
      )
    )
  );
  $sql1 =
  "
SELECT
  stock_id::text,
  stock,
  feature_id,
  haplotype_block,
  lower(haplotype),
  string_agg(value, '|') AS values,
  string_agg(marker_feature_id::text, '|') AS mfeature_ids,
  string_agg(marker, '|') AS markers,
  max(description) AS description,
  max(project_id) AS project_id,
  max(project) AS project
FROM
(SELECT * FROM
(SELECT
  stock_id,
  max(stock) AS stock,
  feature_id,
  max(haplotype_block) AS haplotype_block,
  string_agg(haplotype, ' ') AS haplotype,
  string_agg(value, ' ') AS value,
  marker_feature_id,
  max(marker) AS marker,
  max(description) AS description,
  max(project_id) AS project_id,
  max(project) AS project
FROM
(SELECT
  S.stock_id,
  S.uniquename AS stock,
  HB.feature_id,
  HB.name AS haplotype_block,
  H.name AS haplotype,
  FR2P.value,
  MARKER.feature_id AS marker_feature_id,
  MARKER.uniquename AS marker,
  lower(G.description) AS description,
  P.project_id,
  P.name AS project
  FROM feature HB
  INNER JOIN feature_relationship FR ON HB.feature_id = FR.object_id AND FR.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'variant_of' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'sequence'))
  INNER JOIN feature_relationship FR2 ON FR2.subject_id = FR.subject_id AND FR2.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'contains' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'sequence'))
  INNER JOIN feature_relationshipprop FR2P ON FR2.feature_relationship_id = FR2P.feature_relationship_id
  INNER JOIN feature MARKER ON MARKER.feature_id = FR2.object_id
  INNER JOIN feature H ON H.feature_id = FR.subject_id
  INNER JOIN feature_genotype FG ON FG.feature_id = HB.feature_id
  INNER JOIN nd_experiment_genotype NEG ON NEG.genotype_id = FG.genotype_id
  INNER JOIN genotype G ON G.genotype_id = NEG.genotype_id
  INNER JOIN nd_experiment_stock NES ON NEG.nd_experiment_id = NES.nd_experiment_id
  INNER JOIN stock S ON S.stock_id = NES.stock_id
  INNER JOIN nd_experiment_project NEP ON NEP.nd_experiment_id = NEG.nd_experiment_id
  INNER JOIN project P ON P.project_id = NEP.project_id
WHERE lower(H.name) = ANY(string_to_array(lower(G.description), '|'))
ORDER BY HB.feature_id, MARKER.feature_id, S.stock_id, H.name)
T
GROUP BY feature_id, marker_feature_id, stock_id
) T2
ORDER BY stock_id
) T3
GROUP BY  stock_id,  stock, feature_id, haplotype_block,  haplotype
";
  $view1 = array('view_name' => $view_name1, 'schema' => $schema1, 'sql' => $sql1);

  // haplotype block location mview
  $view_name2 = 'chado_search_haplotype_block_location';
  $schema2 =  array (
    'table' => $view_name2,
    'fields' => array (
      'hb_feature_id' => array (
        'type' => 'int'
      ),
      'haplotype_block' => array (
        'type' => 'text'
      ),
      'organism_id' => array (
        'type' => 'int'
      ),
      'organism' => array (
        'type' => 'varchar',
        'length' => '510'
      ),
      'analysis_id' => array (
        'type' => 'int'
      ),
      'genome' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'landmark_id' => array(
        'type' => 'int'
      ),
      'landmark' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'fmin' => array (
        'type' => 'int',
      ),
      'fmax' => array (
        'type' => 'int'
      ),
      'trait' => array (
        'type' => 'text'
      ),
    )
  );
  $sql2 =
  "SELECT
    HB.feature_id AS hb_feature_id,
    HB.uniquename AS haplotype_block,
    HB.organism_id,
    (SELECT genus || ' ' || species FROM organism O WHERE O.organism_id = HB.organism_id) AS organism,
    GENOME.analysis_id,
    GENOME.name AS genome,
    FL.srcfeature_id AS landmark_id,
    (SELECT uniquename FROM feature WHERE feature_id = FL.srcfeature_id) AS landmark,
    FL.fmin,
    FL.fmax,
    V.name AS trait
  FROM feature HB
  LEFT JOIN featureloc FL ON FL.feature_id = HB.feature_id
  LEFT JOIN feature_cvterm FC ON FC.feature_id = HB.feature_id
  LEFT JOIN cvterm V ON V.cvterm_id = FC.cvterm_id
  LEFT JOIN (SELECT AF.feature_id, A.analysis_id, A.name FROM analysis A INNER JOIN analysisprop AP ON A.analysis_id = AP.analysis_id INNER JOIN analysisfeature AF ON AF.analysis_id = A.analysis_id WHERE AP.value = 'whole_genome') GENOME ON GENOME.feature_id = FL.srcfeature_id
  WHERE HB.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'haplotype_block' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'sequence'))";
  $view2 = array('view_name' => $view_name2, 'schema' => $schema2, 'sql' => $sql2);

  // marker genome location mview
  $view_name3 = 'chado_search_haplotype_block_marker_location';
  $schema3 =  array (
    'table' => $view_name3,
    'fields' => array (
      'marker_feature_id' => array (
        'type' => 'int'
      ),
      'genome_analysis_id' => array (
        'type' => 'int'
      ),
      'genome' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'landmark' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'fmin' => array (
        'type' => 'int',
      ),
      'fmax' => array (
        'type' => 'int'
      ),
      'landmark_id' => array (
        'type' => 'int'
      ),
      'location' => array(
        'type' => 'text'
      ),
      'ss_id' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'rs_id' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
    )
  );
  $sql3 =
  "SELECT DISTINCT
      MARKER.feature_id AS marker_feature_id,
      A.analysis_id AS genome_analysis_id,
      A.name AS genome,
      (SELECT name FROM feature WHERE feature_id = FL.srcfeature_id) AS landmark,
      FL.fmin,
      FL.fmax,
      FL.srcfeature_id AS landmark_id,
      (SELECT name FROM feature WHERE feature_id = FL.srcfeature_id) || ':' || fmin +1 AS location,
      (SELECT name
       FROM synonym S
       WHERE S.synonym_id IN (SELECT synonym_id FROM feature_synonym WHERE feature_id = MARKER.feature_id)
       AND (SELECT name FROM cvterm WHERE cvterm_id = S.type_id) = ('ss_id') LIMIT 1
      ) AS ss_id,
      (SELECT name
       FROM synonym S
       WHERE S.synonym_id IN (SELECT synonym_id FROM feature_synonym WHERE feature_id = MARKER.feature_id)
       AND (SELECT name FROM cvterm WHERE cvterm_id = S.type_id) = ('rs_id') LIMIT 1
      ) AS rs_id
    FROM
      (SELECT DISTINCT FR.object_id AS feature_id
       FROM feature_relationship FR
       INNER JOIN feature_relationship FR2 ON FR.subject_id = FR2.object_id
       INNER JOIN feature H ON H.feature_id = FR2.subject_id
       WHERE FR.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'contains' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'sequence'))
       AND H.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'haplotype' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'sequence'))
       ) MARKER
    INNER JOIN featureloc FL ON MARKER.feature_id = FL.feature_id
    INNER JOIN analysisfeature AF ON AF.feature_id = FL.srcfeature_id
    INNER JOIN analysis A ON A.analysis_id = AF.analysis_id
    INNER JOIN analysisprop AP ON AP.analysis_id = A.analysis_id
    WHERE AP.value = 'whole_genome'
    ORDER BY A.analysis_id,fmin";
  $view3 = array('view_name' => $view_name3, 'schema' => $schema3, 'sql' => $sql3);

  // marker map position mview
  $view_name4 = 'chado_search_haplotype_block_marker_position';
  $schema4 =  array (
    'table' => $view_name4,
    'fields' => array (
      'marker_feature_id' => array (
        'type' => 'int'
      ),
      'featuremap_id' => array (
        'type' => 'int'
      ),
      'map' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'lg' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'start' => array (
        'type' => 'text',
      ),
      'stop' => array (
        'type' => 'text'
      ),
      'position' => array(
        'type' => 'text'
      )
    )
  );
  $sql4 =
  "SELECT DISTINCT
      LOCUS.marker_feature_id,
      FM.featuremap_id,
      FM.name AS map,
      F.name AS lg,
      START.value AS start,
      STOP.value AS stop,
      F.name || ':' || START.value AS position
    FROM
      (SELECT DISTINCT L.feature_id, FR.object_id AS marker_feature_id FROM feature L
       INNER JOIN feature_relationship FR ON FR.subject_id = L.feature_id
       INNER JOIN feature_relationship FR2 ON FR.object_id = FR2.object_id
       INNER JOIN feature HB ON HB.feature_id = FR2.subject_id
       WHERE L.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'marker_locus' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
       AND FR.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'instance_of' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'relationship'))
       AND FR2.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'contains' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'sequence'))
       AND HB.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'haplotype' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'sequence'))
       ) LOCUS
    INNER JOIN featurepos FP ON FP.feature_id = LOCUS.feature_id
    INNER JOIN featuremap FM ON FM.featuremap_id = FP.featuremap_id
    INNER JOIN feature F ON F.feature_id = FP.map_feature_id
    LEFT JOIN
      (SELECT featurepos_id, value
       FROM featureposprop FPP
       WHERE type_id =
        (SELECT cvterm_id
         FROM cvterm
         WHERE name = 'start'
         AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
      ) START ON START.featurepos_id = FP.featurepos_id
    LEFT JOIN
      (SELECT featurepos_id, value
       FROM featureposprop FPP
       WHERE type_id =
        (SELECT cvterm_id
         FROM cvterm
         WHERE name = 'stop'
         AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
      ) STOP ON STOP.featurepos_id = FP.featurepos_id
    ORDER BY FM.featuremap_id";
  $view4 = array('view_name' => $view_name4, 'schema' => $schema4, 'sql' => $sql4);

  return array($view1, $view2, $view3, $view4);
}
