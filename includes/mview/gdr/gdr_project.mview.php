<?php
// Create 'germplasm_search' MView
function chado_search_create_project_mview() {
  $view_name = 'chado_search_project';

  $schema = array (
  'table' => $view_name,
  'fields' => array (
    'project_id' => array(
      'type' => 'int',
      'not null' => TRUE,
    ),
    'name' => array(
      'type' => 'varchar',
      'length' => 256,
      'not null' => FALSE,
    ),
    'type' => array(
      'type' => 'text',
    ),
    'sub_type' => array(
      'type' => 'text',
    ),
    'description' => array(
      'type' => 'text',
    ),
  ),
);
  $sql = "
  SELECT
    P.project_id,
    P.name,
    TYPE.value AS type,
    SUBTYPE.value AS sub_type,
    DES.value AS description
  FROM project P
  INNER JOIN projectprop TYPE ON P.project_id = TYPE.project_id
    AND TYPE.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'project_type' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
  LEFT JOIN projectprop SUBTYPE ON P.project_id = SUBTYPE.project_id
    AND SUBTYPE.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'sub_type' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
  LEFT JOIN projectprop DES ON P.project_id = DES.project_id
    AND DES.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'description' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))";

  return array(array('view_name' => $view_name, 'schema' => $schema, 'sql' => $sql));
}
