# Csform API document v1.1
For creating a custom search form, see 'Create New Search' section in the README.md to learn the steps.
This document lists all widgets that can be added to a custom search form. A custom search form object
is passed in from the 'hook_search_form ($form)' function. To add a new widget onto the form, call
$form->addWidget() function and set up the widget by calling Set::widget() with desired configurations
as shown in the following paradigm:
  ```
    $form->addWidget(
      Set::widget()
      ->configuration1()
      ->configuration2()
      ...
    )
  ```
Note 1. Configurations are orderless and mostly optional except for the ->id('unique_id'). Examples are shown below.

Note 2. The custom search $form object is not the same as the Drupal Form API's $form array.

Listed below are two types of widgets, 'Combo filters' (used more often) and 'Basic form elements' with example configurations.

# Functions for common settings of all form elements
  ```
  ->id()                        // specify a unique ID across the form
  ->fieldset()               // specify a fieldset_id to group the elemens
  ->defaultValue()      // passing this value to Drupal form's #default_value property
  ->ajax()                    // passing this value to Drupal form's #ajax property
  ->newLine()             // create a new line for the next element to layout
  ->attributes()           // passing this value to Drupal form's #attributes property
  ->title()                    // title for the Combo filters
  ->display()               // optional value of 'block' or 'none'
  ```

# Combo filters (i.e. combination of basic form elements)

1. BetweenFilter (Two sets of LabeledFilter)
  ```
    $form->addBetweenFilter(
      Set::betweenFilter()
      ->id('unique_id')
      ->id2('unique_id')
      ->title('some_title')
      ->title2('some_title')
      ->labelWidth(100)
      ->labelWidth2(120)
      ->size(20)
      ->newLine()
      ->fieldset('add_to_this_fieldset')
    )
  ```

2. LabeledFilter (A markup label and a textfield)
  ```
    $form->addLabeledFilter(
      Set::labeledFilter()
      ->id('unique_id')
      ->title('some_title')
      ->required(FALSE)
      ->size(20)
      ->labelWidth(100)
      ->newLine()
      ->maxlength(256)
      ->autoCompletePath('/chado_search_autocomplete/feature/name')    // Drupal path for the autocomplete callback
      ->fieldset('add_to_this_fieldset')
    )
  ```

3. TextFilter (A markup label, a select with values (contains/exactly/starts with/ends with/dose not contain), and a textfield)
  ```
    $form->addTextFilter(
      Set::textFilter()
      ->id('unique_id')
      ->title('some_title')
      ->required(FALSE)
      ->labelWidth(100)
      ->size(20)
      ->maxlength(128)
      ->newLine()
      ->autoCompletePath('/chado_search_autocomplete/feature/name')
      ->fieldset('add_to_this_fieldset')
      ->numeric(TRUE)  // For numeric values. Change operator options from 'contains, exactly, starts, ends, not_contain' to '>, =, <'
      ->defaultOp('=') // Set default operator
    )
  ```

4. TextareaFilter (A markup label, a select with values (contains/exactly/starts with/ends with/dose not contain), and a textarea)
  ```
    $form->addTextareaFilter(
      Set::textareaFilter()
      ->id('unique_id')
      ->title('some_title')
      ->required(FALSE)
      ->labelWidth(100)
      ->cols(5)
      ->rows(10)
      ->newLine()
      ->fieldset('add_to_this_fieldset')
    )
  ```

5. SelectFilter (A markup label and a select). The available values are retrieved from database column and table.
   The available values are the distinct values of specified column in specified table.
   Multiple columns are allowed by passing an array of columns for which values are
   concatenated by a comma (,).
   Common selection values can be passed in as an array to the $optgroup variable.
   Alternatively, the selection values can be grouped by passing in a $optgroup_by_pattern
   variable consisting of array('Display Group' => 'pattern').
   If $cache is TRUE, a cacahe table will be created and this will greatly improve the
   performance for rendering the search form. The cache will automatically refresh when
   new data are added and you will never need to clear it by hand.
  ```
    $form->addSelectFilter(
      Set::selectFilter()
      ->id('unique_id')
      ->title('some_title')
      ->column('name')
      ->table('feature')
      ->condition('feature_id > 100')
      ->required(FALSE)
      ->multiple(TRUE)
      ->columnNaturalSort(TRUE)
      ->optGroup(array('frequently','used','options'))
      ->optGroupByPattern(array('prunus' => 'Peach Genome', 'malus' => 'Apple Genome'))
      ->cache(TRUE)
      ->label_width(100)
      ->size(20)
      ->newLine()
      ->disable(array('disable', 'these', 'options'))
      ->only(array('show', 'only', 'these', 'options'))
      ->searchBox(TRUE)          // Add a search box to filter the options by typing
      ->nullable(TRUE)
      ->fieldset('add_to_this_fieldset')
    )
  ```

6. SelectOptionsFilter (A markup label and a select). The available values are passed in as an array().
  ```
    $form->addSelectOptionsFilter(
      Set::selectOptionsFilter()
      ->id('unique_id')
      ->title('some_title')
      ->options(array('k1' => 'option1', 'k2' => 'option2',... ))
      ->required(FALSE)
      ->multiple(TRUE)
      ->noKeyConversion(TRUE)
      ->labelWidth(100)
      ->newLine()
      ->size(20)
      ->fieldset('add_to_this_fieldset')
      ->ajax = array(
          'callback' => 'some_callback'',
          'wrapper' => 'some_css_id',
          'effect' => 'fade'
        )
      )
    )
  ```

7. DynamicMarkup (A markup whose value was derived from a select element). A $value variable will be passing into your AJAX function
  ```
    $form->addDynamicMarkup(
      Set::dynamicMarkup()
      ->id('unique_id')
      ->dependOnId('a_SelectFilter_id');
      ->callback('come_callback')
      ->newLine()
      ->fieldset('add_to_this_fieldset')
    )
  ```

8. DynamicFieldset (A fieldset displayed when a value is choosen from a select box)
  ```
    $form->addDynamicFieldset(
      Set::dynamicFieldset()
      ->id('unique_id')
      ->dependOnId();
      ->title('some_title')
      ->description('some_description')
      ->collapsible(TRUE)
      ->collapsed(FALSE)
      ->width(400)
      ->display('block')
      ->newLine()
      ->fieldset('add_to_this_fieldset')
    )
  ```

9. DynamicSelectFilter (A computed select whose value was derived from another select element). A $value variable will be passing into your AJAX function
  ```
    $form->addDynamicSelectFilter(
      Set::dynamicSelectFilter()
      ->id('unique_id')
      ->title('some_title')
      ->dependOnId('another_SelectFilter_id')
      ->callback('some_callback')
      ->newLine()
      ->label_width(100)
      ->cache('some_table', array('column1', 'column2', ...))
      ->resetOnChange('another_SelectFilter_id')
      ->multiple(TRUE)
      ->alsoDependOn('another_SelectFilter_id')
      ->hideIfNoOption(TRUE)
      ->fieldset('add_to_this_fieldset')
      ->searchBox(TRUE)              // Add a search box to filter the options by typing
      ->cachedOpts($col_val, $col_filter, $sort = TRUE, $default_empty = FALSE, $also_depend_filters = [])    // Alternative to ->callback(), getting options from the cache table.
    )
  ```

10. DynamicTextFields. Upon select status changes, a couple of text fields can be populated with values. A $value variable will be passing into your AJAX function
  ```
    $form->addDynamicTextFields(
      Set::dynamicTextFields()
      ->id('unique_id')
      ->targetIds('another_SelectFilter_id')
      ->callback('some_callback')
      ->newLine()
      ->resetOnChange_id('another_SelectFilter_id')
      ->fieldset('add_to_this_fieldset')
    )
  ```

11. Fieldset (Grouping widgets, between a start and an end widgets,  into a fieldset)
  ```
  $form->addFieldset(
      Set::fieldset()
      ->id('unique_id')
      ->startWidget('strat_widget_id')
      ->endWidget('end_widget_id')
      ->description('some_description)
      ->collapsed(FALSE)
      );
  ```

12. Tabs (Horizontal tabs with hyperlink)
  ```
   $form->addTabs(
    Set::tabs()
      ->id('unique_id')
      ->items(array('path1' => 'Item1', 'path2' => 'Item2'))
      ->newLine()
      ->fieldset('add_to_this_fieldset')
    )
  ```

13. SelectShortCut (hyperlink for quick selection on a Select box)
  ```
    $form->addSelectShortCut(
      Set::selectShortCut(
      ->id('unique_id')
      ->selectbox_id('another_SelectFilter_id')
      ->value('select_this_value')
      ->pretext('some_text')
      ->postext('some_text')
      ->newLine()
      ->fieldset('add_to_this_fieldset')
    )
  ```

14. CustomOutput. (Expose output fields to the users)
  ```
    $form->addCustomOutput(
      Set::customOutput()
      ->id('unique_id')
      ->options(array('option1', 'option2', 'option3'))
      ->defaults('option1')
      ->title('some_title')
      ->description('some_description')
      ->collapsible(TRUE)
      ->collapsed(FALSE)
      ->groupSelection= (TRUE);
      ->maxColumns(TRUE)
      ->rowCounter(TRUE)
      ->fieldset('add_to_this_fieldset')
    )
  ```

15. RepeatableText (A select, a textfield, and a repeat button that will add another set of filter widgets upon clicked)
  ```
    $form->addRepeatableText(
      Set::repeatableText()
      ->id('unique_id')
      ->required(FALSE)
      ->size(20)
      ->newLine()
      ->fieldset('add_to_this_fieldset')
      ->items(array('name', 'uniquename'))
      ->itemTypes(array('varchar', 'text'))
      ->maxlength()
      ->autocomplete_path('/chado_search_autocomplete/feature/name')    // Drupal path for the autocomplete callback
    )
  ```
16. SequenceRetrieval (A tool that allows users to download sequences with upstream/downstream bases for limited output. Limits can be set in the administrative interface.)
  ```
  $form->addSequenceRetrieval (
    Set::sequenceRetrieval()
    ->id('seuqence_retrieval')
    ->title('some_title')
    ->description('some_description')
    ->collapsible(TRUE)
    ->collapsed(TRUE)
  )
  ```
  In the hook_form_submit(), ChadoSearch needs to know about the columns in the $sql when calling the ->seqeunceRetrieval(<column_feature_id>, <column_feature_name>, <column_srcfeature_id>, <column_fmin>, <column_fmax>) function. Arguments thereafter are optional, they are <colunm_strand>, <columns_unique_for_removing_duplicates>, <fmin_coordinate_is_1_based>
  ```
   Set::result()
    ->sql($sql)
    ->sequenceRetrieval('feature_id', 'uniquename', 'srcfeature_id', 'fmin', 'fmax', NULL, 'feature_id,srcfeature_id,fmin,fmax', 1)
    ->execute($form, $form_state);
  ```

# Basic form elements (corresponds to Drupal's form elements)

1. Hidden (a hidden value that passed along when the form is submitted)
  ```
    $form->addHidden(
      Set::hidden()
      ->id('unique_id')
      ->value('some_value')
      ->defaultValue('some_value')
      ->fieldset('add_to_this_fieldset')
      ->newLine()
    )
  ```

2. TextField
  ```
    $form->addTextField(
      Set::textField()
      ->id('unique_id')
      ->title('some_title')
      ->required(FALSE)
      ->size(100)
      ->maxlength(256)
      ->defaultValue('some_value')
      ->display('block')
      ->fieldset('add_to_this_fieldset')
      ->autoCompletePath('/chado_search_autocomplete/feature/name')
      ->newLine())
  ```

3. TextArea
  ```
    $form->addTextArea(
      Set::textArea()
      ->id('unique_id')
      ->title('some_title')
      ->required(FALSE)
      ->cols(20)
      ->rows(10)
      ->defaultValue('some_value')
      ->display('block')
      ->fieldset('add_to_this_fieldset')
      ->newLine())
  ```

4. Select (a drop-down with options)
  ```
    $form->addSelect(
      Set::select()
      ->id('unique_id')
      ->title('some_title')
      ->options(array('key1' => 'option1', 'key2' => 'option2'))
      ->multiple(FALSE)
      ->size(100)
      ->defaultValue(array('key1'))
      ->display('block')
      ->fieldset('add_to_this_fieldset')
      ->ajax(
        array(
          'callback' => 'some_callback'',
          'wrapper' => 'some_css_id',
          'effect' => 'fade'
        )
      )
      ->newLine())
  ```

5. Markup (HTML markup)
  ```
    $form->addMarkup(
      Set::markup()
      ->id('unique_id')
      ->text('some_markup')
      ->display('block')
      ->fieldset('add_to_this_fieldset')
      ->newLine()
    )
  ```

6. File
  ```
    $form->addFile(
      Set::file()
      ->id('unique_id')
      ->title('some_titile')
      ->description('some_description')
      ->size(20)
      ->labelWidth(100)
      ->display('block')
      ->fieldset('add_to_this_fieldset')
      ->newLine()
   )
  ```

7. Checkboxes
  ```
    $form->addCheckboxes(
      Set::checkboxes()
      ->id('unique_id')
      ->title('some_title')
      ->options(array('key1' => 'option1', 'key2' => 'option2'))
      ->defaultValue(array('key1'))
      ->fieldset('add_to_this_fieldset')
      ->ajax(
        array(
          'callback' => 'some_callback'',
          'wrapper' => 'some_css_id',
          'effect' => 'fade'
        )
      )
      ->newLine()
      )
  ```

8. Button
  ```
    $form->addButton(
      Set::button()
      ->id('unique_id')
      ->value('Submit')
      ->name('some_name')
      ->buttonType('button')
      ->attributes(array('onMouseDown' => 'some_js_code'))
      ->display('block')
      ->ajax(
        array(
          'callback' => 'some_callback'',
          'wrapper' => 'some_css_id',
          'effect' => 'fade'
        )
      )
      ->fieldset('add_to_this_fieldset')
      ->newLine())
  ```

9. ClearButton (a clear form button)
  ```
    $form->addClearButton(
      Set::clearButton()
      ->id('unique_id')
      ->value('Clear')
      ->name('some_name')
      ->button_type('button')
      ->attributes(array('onMouseDown' => 'some_js_code'))
      ->display('block')
      ->ajax(
        array(
          'callback' => 'chado_search_ajax_form_clear_values'',
          'wrapper' => 'chado_search_form',
          'effect' => 'fade'
        )
      )
      ->fieldset('add_to_this_fieldset')
      ->newLine())
  ```

10. Submit (a submit button)
  ```
    $form->addSubmit(
      Set::submit()
      ->id('unique_id')
      ->value('Submit')
      ->name('some_name')
      ->attributes(array('onMouseDown' => 'some_js_code'))
      ->ajax(
        array(
          'callback' => 'chado_search_ajax_form_clear_values'',
          'wrapper' => 'chado_search_form',
          'effect' => 'fade'
        )
      )
      ->fieldset('add_to_this_fieldset')
      ->newLine())
  ```

11. Reset (a reset button)
  ```
    $form->addReset(
      Set::reset()
      ->id('unique_id')
      ->newLine())
  ```

12. Throbber (a throbber progress indicator)
  ```
    $form->addThrobber(
      Set::throbber()
      ->id('unique_id')
      ->fieldset('add_to_this_fieldset')
      ->newLine()
    )
  ```