<?php 

function chado_search_get_table_indexes($table, $schema = 'public', $tablespace = NULL) {
  $sql = "SELECT indexname,  indexdef FROM pg_indexes WHERE tablename = '" . $table . "'";
  $sql = $schema ? $sql . " AND schemaname = '" . $schema . "'" : $sql;
  $sql = $tablespace ? $sql . " AND tablespace = '" . $tablespace . "'" : $sql;
  $result = \Drupal::database()->query($sql);
  $idx = [];
  while ($i = $result->fetchObject()) {
    $idx [$i->indexname] = $i->indexdef;
  }
  return $idx;
}

function chado_search_create_table_index($table, $columns = []) {
  $idx = [];
  foreach ($columns AS $column) {
    $idx_name = substr('cs_' . preg_replace('/^.+?\./', '', $table) . '_' . str_replace(',', '_', $column), 0, 63);
    $sql = 'CREATE INDEX ' . $idx_name . ' ON ' . $table . ' USING btree (' . $column . ')';
    try {
      \Drupal::database()->query($sql);
      $idx [] = $idx_name;
    }
    catch (Exception $e) {
      print $e->getMessage() . "\n";
    }    
  }
  return $idx;
}

function chado_search_drop_table_index($index, $schema = 'public') {
  $sql = "DROP index $schema.$index";
  try {
    \Drupal::database()->query($sql);
    return $index;
  } catch (Exception $e) {
    print $e->getMessage() . "\n";
  }
  return 0;
}

function chado_search_drop_all_table_indexes ($table, $schema = 'public') {
  $indexes = chado_search_get_table_indexes($table, $schema);
  $dropped = [];
  foreach ($indexes AS $index => $def) {
    $d = chado_search_drop_table_index ($index, $schema);
    $dropped [] = $d;
  }
  return $dropped;
}