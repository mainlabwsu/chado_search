<?php

/*
 * Get the search settings from the file
*/
function chado_search_settings($file, $enabled_only = TRUE) {
  $file_handle = fopen($file, "r");
  $enabledSearch = array();
  $search = NULL;
  while (!feof($file_handle)) {
    $line = trim(fgets($file_handle));
    if (!preg_match('/^#/', $line)) { // Ignore comments
      $matches = array();
      if (preg_match('/^\[(.+)\]$/', trim($line), $matches)) {
        // Save search
        if ($search) {
          if ($enabled_only && $search['enabled']) {
            array_push($enabledSearch, $search);
          }
          else if (!$enabled_only) {
            array_push($enabledSearch, $search);
          }
          $search = array();
          $ajax = array();
          $search['title'] = $matches[1];
          // Create a new search
        } else {
          $search = array();
          $ajax = array();
          $search['title'] = $matches[1];
        }
      } else if (preg_match('/^id=(.+)$/', $line, $matches)) {
        $search['id'] = $matches[1];
      } else if (preg_match('/^path=(.+)$/', $line, $matches)) {
        $search['path'] = $matches[1];
      } else if (preg_match('/^file=(.+)$/', $line, $matches)) {
        $search['file'] = $matches[1];
      } else if (preg_match('/^ajax=(.+):(.+)$/', $line, $matches)) {
        $ajax[$matches[1]] = $matches[2];
        $search['ajax'] = $ajax;
      } else if (preg_match('/^mview_file=(.+)$/', $line, $matches)) {
        $search['mview_file'] = $matches[1];
      } else if (preg_match('/^mview_callback=(.+)$/', $line, $matches)) {
        $search['mview_callback'] = $matches[1];
      } else if (preg_match('/^summary_title=(.+)$/', $line, $matches)) {
        $search['summary_title'] = $matches[1];
      } else if (preg_match('/^enabled=(.+)$/', $line, $matches)) {
        $search['enabled'] = $matches[1];
      } else if (preg_match('/^num_per_page=(.+)$/', $line, $matches)) {
        $search['num_per_page'] = $matches[1];
      } else if (preg_match('/^summary_allowed=(.+)$/', $line, $matches)) {
        $search['summary_allowed'] = $matches[1];
      }
    }
  }
  // The last search
  if ($enabled_only) {
    if ($search && $search['enabled']) {
      array_push($enabledSearch, $search);
    }
  }
  else {
    if ($search) {
      array_push($enabledSearch, $search);
    }
  }
  fclose($file_handle);
  return $enabledSearch;
}

/*
 * Get enabled search
 */
function chado_search_get_enabled_searches ($silent = TRUE) {
  return chado_search_get_all_searches($silent, TRUE);
}

/*
 * Get all search
 */
function chado_search_get_all_searches ($silent = TRUE, $enabled_only = FALSE) {
  $chado_search_conf_file = '/file/settings.conf';
  $file = \Drupal::service('extension.list.module')->getPath('chado_search') . $chado_search_conf_file;
  if (file_exists($file)) {
    return chado_search_settings($file, $enabled_only);
  } else {
    if (!$silent) {
      \Drupal::messenger()->addError(t("Fatal Error: Chado Search conf file 'chado_search$chado_search_conf_file not found. "));
    }
    return array();
  }
}

/*
 * Get search setting by id
 */
function chado_search_get_setting_by_id ($search_id, $setting) {
  $searches = chado_search_get_enabled_searches();
  for ($i = 0; $i < count($searches); $i ++) {
    if (isset($searches[$i]['id']) && $searches[$i]['id'] == $search_id) {
      if (key_exists($setting, $searches[$i])) {
      return $searches[$i][$setting];
      }
    }
  }
  return NULL;
}

/*
 * Set search setting by id
 */
function chado_search_set_setting_by_id ($search_id, $setting) {
  $chado_search_conf_file = '/file/settings.conf';
  $file = \Drupal::service('extension.list.module')->getPath('chado_search') . $chado_search_conf_file;
  $writable = is_writable($file);
  if (file_exists($file) && $writable) {
    $handle = fopen($file, 'r');
    $content = array();
    $idx_start = 0;
    $idx_end = 0;
    $idx_id = 0;
    $index = 0;
    $found = FALSE;
    while (($line = fgets($handle)) !== FALSE) {
      if (preg_match('/^\[(.+)\]$/', trim($line))) {
        if ($idx_id) {
          $found = TRUE;
          $idx_end = $idx_end > $idx_start ? $idx_end : $index;
        }
        else {
          if (!$found) {
            $idx_start = $index;
            $idx_end = $index;
          }
        }
      } else if (trim($line) == 'id=' . $search_id) {
        $idx_id = $index;
      }
      $content[$index] = $line;
      $index ++;
    }
    $idx_end = $idx_end == $idx_start ? $index : $idx_end;
    $key_value = explode('=', $setting);
    for($i = $idx_start; $i < $idx_end ; $i ++) {
      $line = explode('=', $content[$i]);
      if (trim($line[0]) == $key_value[0]) {
       $content[$i] = $setting . "\n";
      }
    }

    $succeed = file_put_contents($file, $content);
    return $succeed;
  } else {
    \Drupal::messenger()->addError(t("File \'' . $chado_search_conf_file . '\' is not writable. Nothing changed."));
  }
  return FALSE;
}