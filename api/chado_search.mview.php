<?php

// Create Table in 'chado' schema
function chado_search_create_chado_table($name, $table) {
  $schema = null;
  if (class_exists('Drupal\Core\Database\Driver\pgsql\Schema')) {
    $schema = new \Drupal\chado_search\Core\ChadoSchema ();
  }
  else {
    $schema = new \Drupal\chado_search\Core\ChadoSchema11 ();
  }
  $statements = $schema->createTableSQL($name, $table);
  foreach ($statements AS $sql) {
    chado_search_query($sql, array(), 'chado');
  }
}

// Create MView in 'chado' schema. (Use Tripal MView API if available)
function chado_search_create_chado_mview($view_name, $schema, $sql) {
  if (trim($view_name)) {
    chado_search_drop_chado_mview($view_name);
    if (function_exists('tripal_add_mview')) {
      tripal_add_mview($view_name, 'chado_search', $schema, $sql, '', FALSE);
    } else {
      $statements = chado_search_create_chado_table($view_name, $schema);
      print "- MView '$view_name' created.\n";
    }
  }
}

// Drop MView from 'chado' schema (Use Tripal MView API if available)
function chado_search_drop_chado_mview($view_name) {
  if (chado_search_table_exists($view_name, 'chado')) {
    if (function_exists('tripal_get_mview_id') && function_exists('tripal_delete_mview')) {
      $mview_id = tripal_get_mview_id($view_name);
      if($mview_id){
        tripal_delete_mview($mview_id);
      }
    }
    else {
      chado_search_query('DROP TABLE IF EXISTS ' . $view_name, array(), 'chado');
      print "- MView '$view_name' dropped.\n";
      \Drupal::state()->delete('chado_search_time_' . $view_name);
      \Drupal::state()->delete('chado_search_rows_' . $view_name);
    }
  }
}


// Populate MView (Use Tripal MView API if available)
function chado_search_populate_chado_mview($view_name) {
  if (function_exists('tripal_get_mview_id') && function_exists('chado_populate_mview')) {
    $mview_id = tripal_get_mview_id($view_name);
    if($mview_id){
      chado_populate_mview($mview_id);
    }
  }
  else {
    $enabledSearch = chado_search_get_enabled_searches(FALSE);
    $succeed = FALSE;
    foreach ($enabledSearch AS $search) {
      if (isset($search ['mview_file'])) {
        $filepath = \Drupal::service('extension.list.module')->getPath('chado_search') . '/'. $search['mview_file'];
        if (file_exists($filepath)) {
          require_once $filepath;
          $func = $search['mview_callback'];
          $mvdef = $func();
          foreach ($mvdef AS $def) {
            if($def['view_name'] == $view_name) {
              chado_search_populate_mview_transaction($view_name, $def['sql']);
              $succeed = TRUE;
            }
          }
        }
      }
    }
    if ($succeed) {
      print "  MViews '$view_name' populated.\n";
    }
    else {
      print "Can not populate MView '$view_name'. Please check the spelling of the view name.\n";
    }
  }
}

// Populate MView in a transaction
function chado_search_populate_mview_transaction($view_name, $def_sql) {
  if (chado_search_table_exists($view_name, 'chado')) {
    print "- Populating MView '$view_name'...\n";
    $transaction = \Drupal::database()->startTransaction();
    try {
      $sql = "DELETE FROM $view_name";
      chado_search_query($sql, array(), 'chado');
      $sql = "INSERT INTO $view_name (" . $def_sql . ")";
      chado_search_query($sql, array(), 'chado');
      unset($transaction);
      $rows = chado_search_query("SELECT count(*) FROM $view_name", array(), 'chado')->fetchField();
      print "  Total $rows rows\n";
      \Drupal::state()->set('chado_search_time_' . $view_name, time());
      \Drupal::state()->set('chado_search_rows_' . $view_name, $rows);
      $succeed = TRUE;
    } catch (Exception $e) {
      print $e;
      $transaction->rollBack();
    }
  }
  else {
    print
    "Error: MView '$view_name' does not exist.
    Please make sure related search is enabled in the 'settings.conf'
    and run 'drush csreload' to recreate it.\n";
  }
}

// Populate MView in a transaction
function chado_search_populate_mview_incremental ($view_name, $def_sql, $col_count, $batch_size = NULL) {
  if (chado_search_table_exists($view_name, 'chado')) {
    print "- Incrementally Populating MView '$view_name'...\n";
    $transaction = \Drupal::database()->startTransaction();
    try {
      $max_id = chado_search_query("SELECT coalesce(max( $col_count ), 0) FROM $view_name", array(), 'chado')->fetchField();
      print "  Populating MView with $col_count greater than $max_id ...\n";
      $sql = "INSERT INTO $view_name (SELECT * FROM (" . $def_sql . ") T WHERE $col_count > $max_id)";
      if (is_numeric($batch_size)) {
        $sql = "INSERT INTO $view_name (SELECT * FROM (" . $def_sql . ") T WHERE $col_count > $max_id ORDER BY $col_count LIMIT $batch_size)";
      } else if ($batch_size != NULL) {
        print "Batch size of '$batch_size' is not valid.\n";
        return;
      }
      chado_search_query($sql, array(), 'chado');
      unset($transaction);
      $rows = chado_search_query("SELECT count(*) FROM $view_name", array(), 'chado')->fetchField();
      print "  Total $rows rows\n";
      \Drupal::state()->set('chado_search_time_' . $view_name, time());
      \Drupal::state()->set('chado_search_rows_' . $view_name, $rows);
      $succeed = TRUE;
    } catch (Exception $e) {
      print $e;
      $transaction->rollBack();
    }
  }
  else {
    print
    "Error: MView '$view_name' does not exist.
    Please make sure related search is enabled in the 'settings.conf'
    and run 'drush csreload' to recreate it.\n";
  }
}

// Create MViews for all enabled searches
function chado_search_create_enabled_mviews($dropIfExists = FALSE) {
  $enabledSearch = chado_search_get_enabled_searches(FALSE);
  foreach ($enabledSearch AS $search) {
    if (isset($search ['mview_file'])) {
      $filepath = \Drupal::service('extension.list.module')->getPath('chado_search') . '/' . $search ['mview_file'];
      if (file_exists($filepath)) {
        require_once $filepath;
        $func = $search['mview_callback'];
        if (function_exists($func)) {
          $mvdef = $func();
          foreach ($mvdef AS $def) {
            if ($dropIfExists) {
              chado_search_create_chado_mview($def['view_name'], $def['schema'], $def['sql']);
            }
            else {
              if (!chado_search_table_exists($def['view_name'], 'chado')) {
                chado_search_create_chado_mview($def['view_name'], $def['schema'], $def['sql']);
              }
            }
          }
        }
      }
    }
  }
}

// Drop MViews for all enabled searches
function chado_search_drop_enabled_mviews() {
  $enabledSearch = chado_search_get_enabled_searches(FALSE);
  foreach ($enabledSearch AS $search) {
    if (isset($search ['mview_file'])) {
      $filepath = \Drupal::service('extension.list.module')->getPath('chado_search') . '/' . $search ['mview_file'];
      if (file_exists($filepath)) {
        require_once $filepath;
        $func = $search['mview_callback'];
        if (function_exists($func)) {
          $mvdef = $func();
          foreach ($mvdef AS $def) {
            chado_search_drop_chado_mview($def['view_name']);
          }
        }
      }
    }
  }
}

// Populate MViews for all enabled searches
function chado_search_populate_enabled_mviews() {
  $enabledSearch = chado_search_get_enabled_searches(FALSE);
  foreach ($enabledSearch AS $search) {
    if (isset($search ['mview_file'])) {
      $filepath = \Drupal::service('extension.list.module')->getPath('chado_search') . '/' . $search ['mview_file'];
      if (file_exists($filepath)) {
        require_once $filepath;
        $func = $search['mview_callback'];
        if (function_exists($func)) {
          $mvdef = $func();
          foreach ($mvdef AS $def) {
            chado_search_populate_chado_mview($def['view_name']);
          }
        }
      }
    }
  }
}

// Get MViews for all enabled searches
function chado_search_get_enabled_mviews() {
  $enabledSearch = chado_search_get_enabled_searches(FALSE);
  $enabledMviews = array();
  foreach ($enabledSearch AS $search) {
    if (isset($search ['mview_file'])) {
      $enabledMviews[$search['title']] = array();
      $filepath = \Drupal::service('extension.list.module')->getPath('chado_search') . '/' . $search ['mview_file'];
      if (file_exists($filepath)) {
        require_once $filepath;
        $func = $search['mview_callback'];
        if (function_exists($func)) {
          $mvdef = $func();
          foreach ($mvdef AS $def) {
            $enabledMviews[$search['title']][] = $def['view_name'];
          }
        }
      }
    }
  }
  return $enabledMviews;
}

// Get settings for a specific MView
function chado_search_get_mview_setting($def_callback, $mview, $setting) {
  $def = $def_callback();
  if (isset($def[$mview][$setting])) {
    if ($setting == 'field') {
      // Make sure field exists for the MView
      $fields = [];
      foreach ($def[$mview][$setting] AS $k => $v) {
        if (chado_search_column_exists($mview, $k, 'chado')) {
          $fields[$k] = $v;
        }
      }
      return $fields;
    }
    else {
      return $def[$mview][$setting];
    }
  }
  return NULL;
}

// Get definition for a specific MView
function chado_search_get_mview($def_callback, $mview) {
  $def = $def_callback();
  if (isset($def[$mview])) {
    return $def[$mview];
  }
  return NULL;
}

// Get specific settings for all MViews
function chado_search_get_all_mview_settings ($def_callback, $setting) {
  $def = $def_callback();
  $settings = array();
  foreach ($def AS $key => $val) {
    $settings[$key] = isset($val[$setting]) ? $val[$setting] : NULL;
  }
  return $settings;
}