<?php

require_once "chado_search.conf.php";
require_once "chado_search.mview.php";
require_once "chado_search.linker.php";
require_once "chado_search.index.php";

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Render\Markup;

// Add support to Mac End of Line format
//ini_set("auto_detect_line_endings", true);

function chado_search_api_version() {
  $ver = 4;
  return $ver;
}

// Provide an API function for updating AJAX form elements
function chado_search_ajax_form_update($form, &$form_state) {
  $triggering_element = &$form_state->getTriggeringElement();
  $update = $triggering_element['#attribute']['update'];
  if (is_array($update)) {
    $response = new AjaxResponse();
    foreach ($update AS $id => $u) {
      $form_element = find_first_form_element_by_id($form, $id);
      if (isset($u['value'])) {
        $form_element['#value'] = $u['value'];
      }
      if (isset($u['wrapper'])) {
        $response->addCommand(new ReplaceCommand('#' . $u['wrapper'], $form_element));
      }
    }
    return $response;
  }
  else {
    return find_first_form_element_by_id($form, $update);
  }
}

// Provide an API function to update repeatable text
function chado_search_ajax_form_update_repeatable_text($form, &$form_state) {
  $button = &$form_state->getTriggeringElement();
  $parents = $button['#array_parents'];
  $return = $form;
  $counter = 0;
  foreach ($parents AS $par) {
    $return =$return[$par];
    if ($counter >= count($parents) - 2) {
      break;
    }
    $counter ++;
  }
  return $return;
}

// Provide an API function to update repeatable text's operators
function chado_search_ajax_form_update_repeatable_text_dynamic_types($form, &$form_state) {
  $triggering_element = &$form_state->getTriggeringElement();
  $triggering = $triggering_element['#name'];
  $id = str_replace('_select-', '_ot_wrapper-', $triggering);
  $parents = $triggering_element['#array_parents'];
  $return = $form;
  $counter = 0;
  foreach ($parents AS $par) {
    if ($par == $triggering) {
      $par = $id;
    }
    $return =$return[$par];
    if ($counter >= count($parents) - 2) {
      break;
    }
    $counter ++;
  }
  return $return[$id];
}

//Clear all form values
function chado_search_ajax_form_clear_values($form, &$form_state) {
  return $form;
}

// Bind unique values of a certain column to the ComputedTextFields element
// This function is usually called by an AJAX function to populate the values in a select box
function chado_search_bind_dynamic_textfields($value, $column, $sql) {
  try {
    foreach($value AS $k => $v) {
      $value[$k] = urldecode($v);
    }
    $result = chado_search_query ($sql, $value, 'public,chado')->fetchObject();
    $data = array ();
    if ($result) {
      array_push ($data, $result->$column);
    }
    return $data;
  } catch (\PDOException $e) {
    \Drupal::messenger()->addError(t("Unable to bind DynamicTextFields form element. Please check your SQL statement in the AJAX callback. " . $e->getMessage()));
  }
}

// Bind unique values of a certain column to the DynamicSelect element
// This function is usually called by an AJAX function to populate the values in a select box
function chado_search_bind_dynamic_select($value, $column, $sql, $key_column = NULL) {
  try {
    $data = array(0 => 'Any');
    reset ($value);
    $key = key($value);
    if (is_scalar($value[$key]) || (is_array($value[$key]) && count($value[$key]) > 0)) {
      $pass_value = FALSE;
      foreach ($value AS $k => $v) {
        if (strpos($sql, $k) !== false) {
          $pass_value = TRUE;
        }
      }
      if ($pass_value) {
        $result = chado_search_query($sql, $value, 'public,chado');
      }
      else {
        $result = chado_search_query($sql, [], 'public,chado');
      }
    
      while ($obj = $result->fetchObject()) {
        if ($obj->$column) {
          if ($key_column) {
            $data[$obj->$key_column] = $obj->$column;
          }
          else {
            $data[$obj->$column] = $obj->$column;
          }
        }
      }
    }
    return $data;
  } catch (\PDOException $e) {
    \Drupal::messenger()->addError(t("Unable to bind DynamicSelectFilter form element. Please check your SQL statement in the AJAX callback. " . $e->getMessage()));
  }
}

function chado_search_get_class($obj = NULL) {
  $namespace = explode('\\', get_class($obj));
  $class = $namespace[count($namespace) - 1];
  return $class;
}

/**
 * Convert postgres hstore format into an associative array
 * hstore has a text format of "key1"=>"value1", "key2"=>"value2", "key3" =>NULL
 */
function chado_search_hstore_to_assoc($hstore_value) {
  $hstore = explode(',', $hstore_value);
  $assoc = array();
  foreach ($hstore AS $hs) {
    $pair = explode('=>', $hs);
    if (count($pair) == 2) {
      $assoc[trim($pair[0], '" ')] = trim($pair[1], '" ');
    }
  }
  return $assoc;
}

// Get column types for a table, $fields should be passed in as an array with keys being the column name
function chado_search_get_field_types($table, $fields, $schema = NULL) {
  $schema_table = explode('.', $table);
  if (count($schema_table) == 2) {
    $schema = $schema_table[0];
    $table = $schema_table[1];
  }
  $ftypes = array();
  if ($schema) {
    $sql = "SELECT data_type FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = :table AND column_name = :column AND table_schema = :schema";
    $field_keys = array_keys($fields);
    foreach ($field_keys AS $f) {
      $type = chado_search_query($sql, array(':table' => $table, ':column' => $f, ':schema' => $schema), 'public,chado')->fetchField();
      $ftypes[$f] = $type;
    }
  }
  else {
    $sql = "SELECT data_type FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = :table AND column_name = :column";
    $field_keys = array_keys($fields);
    foreach ($field_keys AS $f) {
      $type = chado_search_query($sql, array(':table' => $table, ':column' => $f), 'public,chado')->fetchField();
      $ftypes[$f] = $type;
    }
  }
  return $ftypes;
}

// Support $search_path only when using the postgres database driver
function chado_search_query ($sql, $args = array(), $search_path = NULL) {
  $db = \Drupal::database();
  $driver = $db->driver();
  $results = NULL;
  if ($driver == 'pgsql' && $search_path) {
    $pg_search_path = $db->query("SHOW search_path;")->fetchField();
    $db->query("SET search_path TO $search_path;");
  }

  try {
    $results = $db->query($sql, $args);
  } catch(\Exception $e) {
    $trace = debug_backtrace();
    $caller = $trace[1];
    \Drupal::logger('chado_search')->error('Error: Query failed. calling from ' . $caller['function'] . ' (line ' . $caller['line'] . ' of ' . $caller['file'] . '). ' . $e->getMessage());
    \Drupal::messenger()->addError(Markup::create('SQL Error: Failed to run query. See <a href="/admin/reports/dblog">Recent log messages</a> for details.'));
  }
  if ($driver == 'pgsql' && $search_path) {
    $db->query("SET search_path TO $pg_search_path;");
  }
  return $results;
}

function chado_search_table_exists($table, $schema_search_path = NULL) {
  $db = \Drupal::database();
  $exists = FALSE;
  if ($schema_search_path) {
    $schemas = explode(',', $schema_search_path);
    foreach ($schemas AS $schema) {
      if($db->schema()->tableExists(trim($schema) . '.' . $table)) {
        $exists = TRUE;
        break;
      }
    }
  }
  else {
    $exists = $db->schema()->tableExists($table);
  }
  return $exists;
}

function chado_search_column_exists($table, $column, $schema_search_path = NULL) {
  $db = \Drupal::database();
  $exists = FALSE;
  if ($schema_search_path) {
    $schemas = explode(',', $schema_search_path);
    foreach ($schemas AS $schema) {
      if($db->schema()->fieldExists(trim($schema) . '.' . $table, $column)) {
        $exists = TRUE;
        break;
      }
    }
  }
  else {
    $exists = $db->schema()->fieldExists($table, $column);
  }
  return $exists;
}

// Traverse through the $form tree and return the first element that matches the specified $id
function &find_first_form_element_by_id(&$form, $id) {
  $children = \Drupal\Core\Render\Element::children($form);
  foreach($children AS $key) {
    $child = &$form[$key];
    if ($key == $id) {
      return $child;
    }
    else {
      $element = &find_first_form_element_by_id($child, $id);
      if ($element) {
        return $element;
      }
    }
  }
  $null = NULL;
  return $null;
}

function chado_search_reverse_complement($seq) {
  $convert = array(
    "A" => "T",
    "T" => "A",
    "G" => "C",
    "C" => "G"
  );
  $nucleotides = str_split($seq,1);
  $complement = "";
  foreach($nucleotides as $base){
    if (key_exists($base, $convert)) {
      $complement = $complement . $convert[$base];
    }
    else {
      $complement = $complement . $base;
    }
  }

  $revcomp = strrev($complement);
  return $revcomp;
}
