Enable the default searches by copying the file 'default.settings.txt' to 'settings.conf'. 'settings.conf' is the only file Chado Search reads.
Chado Search does not provide a settings.conf which ensures your settings will never be overridden when updating the code.

