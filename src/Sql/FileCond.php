<?php

namespace Drupal\chado_search\Sql;

// Create an SQL condition that filters the result for the File widget
class FileCond extends Statement {

  public function __construct($file, $column, $case_sensitive, $contains_word, $limit = 0) {
    $default_uplimit = \Drupal::state()->get('chado_search_upload_limit', '0');
    if ($limit == 0 && $default_uplimit != 0) {
      $limit = $default_uplimit;
    }
    $this->statement = '';
    // get the file upload content if one has been provided
    if ($file) {
      if ($case_sensitive) {
        if ($contains_word) {
          $this->statement = "(";
        } else {
          $this->statement = "$column IN (";
        }
      } else {
        if ($contains_word) {
          $this->statement = "(";
        } else {
          $this->statement = "lower($column) IN (";
        }
      }
      $handle = fopen($file, 'r');
      $counter = 1;
      while ($line = fgets($handle)) {
        $name = trim($line);
        if ($name != "") {
          $name = str_replace("'", "''", $name); // escape the single quote
          if ($case_sensitive) {
            if ($contains_word) {
              if ($this->statement != "(") {
                $this->statement .= " OR ";
              }
              $this->statement .= "$column like '%%" . $name . "%%'";
            } else {
              $this->statement .= "'$name', ";
            }
          } else {
            if ($contains_word) {
              if ($this->statement != "(") {
                $this->statement .= " OR ";
              }
              $this->statement .= "lower($column) like lower('%%" . $name . "%%')";
            } else {
              $this->statement .= "lower('$name'), ";
            }
          }
        }
        if ($limit && $counter >= $limit) {
          break;
        }
        $counter ++;
      }
      fclose($handle);
      if ($this->statement != "$column IN (" && $this->statement != "(") {
        if (!$contains_word) {
          $this->statement = substr($this->statement, 0, strlen($this->statement) - 2);
        }
        $this->statement .= ")";
      }
    }
  }
}