<?php

namespace Drupal\chado_search\Sql;

// Create an SQL condition that filters the result for RepeatableText
class RepeatableTextCond extends Statement {

  public function __construct($filters) {
    $num_filters = count($filters);
    if ($num_filters > 0) {
      $this->statement = "(";
      if ($num_filters > 0) {
        $index = 0;
        foreach ($filters AS $filter) {
          $selected = $filter['select'];
          $op = $filter['op'];
          $text = str_replace("'", "''", $filter['text']);
          $andor = isset($filter['andor']) ? $filter['andor'] : NULL;
          if ($op == 'contains') {
            $this->statement .= "lower($selected) LIKE '%" . strtolower($text) . "%'";
          }
          else if ($op == 'not_contain') {
              $this->statement .= "lower($selected) NOT LIKE '%" . strtolower($text) . "%'";
          }
          else if ($op == 'exactly') {
            $this->statement .= "lower($selected) = '" . strtolower($text) . "'";
          }
          else if ($op == 'starts') {
            $this->statement .= "lower($selected) LIKE '" . strtolower($text) . "%'";
          }
          else if ($op == 'ends') {
            $this->statement .= "lower($selected) LIKE '%" . strtolower($text) . "'";
          }
          else if ($op == '>') {
            if (is_numeric($text)) {
              $this->statement .= "$selected > " . $text;
            }
            else {
              $this->statement .= '1 = 0';
            }
          }
          else if ($op == '<') {
            if (is_numeric($text)) {
              $this->statement .= "$selected < " . $text;
            }
            else {
              $this->statement .= '1 = 0';
            }
          }
          else if ($op == '=') {
            if (is_numeric($text)) {
              $this->statement .= "$selected = " . $text;
            }
            else {
              $this->statement .= '1 = 0';
            }
          }
          if ($index < $num_filters - 1 && $andor) {
            $this->statement .= " $andor ";
          }
          $index ++;
        }
      }
      $this->statement .= ")";
    }
  }
}