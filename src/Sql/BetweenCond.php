<?php

namespace Drupal\chado_search\Sql;

// Create an SQL condition that filters the result for the Between widget
class BetweenCond extends Statement {

  public function __construct($column1, $value1, $column2, $value2, $cast2real = FALSE) {
    $this->statement = '';
    if ($value1 != "" && !is_numeric($value1)) {
      \Drupal::messenger()->addError(t("'$value1' is not a number."));
      $this->statement = '(1=0)';
      return;
    }
    if ($value2 != "" && !is_numeric($value2)) {
      \Drupal::messenger()->addError(t("'$value2' is not a number."));
      $this->statement = '(1=0)';
      return;
    }
    if ($cast2real) {
      if ($value1 && $value2 && trim($value1) != "" && trim($value2) != "") {
        $this->statement = "($column1 >= CAST ($value1 AS real) AND $column2 <= CAST ($value2 AS real))";
      } else if ($value1 && trim($value1) != "") {
        $this->statement = "$column1 >= CAST($value1 AS real)";
      } else if ($value2 && trim($value2) != "") {
        $this->statement = "$column2 <= CAST($value2 AS real)";
      }
    }
    else {
      if ($value1 && $value2 && trim($value1) != "" && trim($value2) != "") {
        $this->statement = "($column1 >= $value1 AND $column2 <= $value2)";
      } else if ($value1 && trim($value1) != "") {
        $this->statement = "$column1 >= $value1";
      } else if ($value2 && trim($value2) != "") {
        $this->statement = "$column2 <= $value2";
      }  
    }
    
  }
}