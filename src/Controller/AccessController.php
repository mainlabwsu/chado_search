<?php

namespace Drupal\chado_search\Controller;

use Drupal\Core\Controller\ControllerBase;

class AccessController extends ControllerBase {

  public function denyAccess() {
    $access_denied_message = \Drupal::state()->get('chado_search_access_denied_message', 'You are not authorized to use this seach function.');
    return array('#markup' => $access_denied_message);
  }
}