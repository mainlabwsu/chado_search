<?php
namespace Drupal\chado_search\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Component\Utility\Xss;

use Drupal\Core\Controller\ControllerBase;

class AutoCompleteController extends ControllerBase {

    public function getMatch(Request $request, $table = NULL, $column = NULL) {
        $matches = array();
        $input = $request->query->get('q');
        if (!$input) {
          return new JsonResponse($matches);
        }

        $string = Xss::filter($input);
        if (chado_search_table_exists($table, 'public,chado') && chado_search_column_exists($table, $column, 'chado') && $string) {
            $return = chado_search_query("SELECT $column FROM $table WHERE $column LIKE '%$string%' LIMIT 20", array(), 'chado');
            foreach ($return as $row) {
                $matches[] = [
                    'value' => $row->$column,
                    'label' => $row->$column,
                ];
            }
        }
        return new JsonResponse($matches);
    }
}