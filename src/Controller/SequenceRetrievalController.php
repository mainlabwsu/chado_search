<?php
namespace Drupal\chado_search\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

use Drupal\Core\Controller\ControllerBase;

use Drupal\chado_search\Result\SequenceRetrieval;
use Drupal\chado_search\Core\SessionVar;

class SequenceRetrievalController extends ControllerBase {

    public function download($search_id) {
        $url = chado_search_get_setting_by_id($search_id, 'path');
        $sr = new SequenceRetrieval($search_id, $url);
        return new JsonResponse($sr->createSequenceRetrieval());
    }
}