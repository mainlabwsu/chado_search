<?php

namespace Drupal\chado_search\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;

use Drupal\Core\Controller\ControllerBase;


class AdminActionController extends ControllerBase {

  public function do($search_id, $action) {
    $succeed = 0;
    if ($action == 'enable') {
      $succeed = chado_search_set_setting_by_id($search_id, 'enabled=1');
    }
    else if ($action == 'disable'){
      $succeed = chado_search_set_setting_by_id($search_id, 'enabled=0');
    }
    else if ($action == 'restrict'){
        $access = \Drupal::state()->get('chado_search_access', array());
        $access [$search_id] = $search_id;
        \Drupal::state()->set('chado_search_access', $access);
    }
    else if ($action == 'allow'){
        $access = \Drupal::state()->get('chado_search_access', array());
        unset($access [$search_id]);
        \Drupal::state()->set('chado_search_access', $access);
    }
    if ($succeed) {
      chado_search_rebuild();
    }
    return new RedirectResponse(\Drupal\Core\Url::fromRoute('chado_search.admin_page')->toString());

  }
}