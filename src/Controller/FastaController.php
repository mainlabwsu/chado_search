<?php
namespace Drupal\chado_search\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

use Drupal\Core\Controller\ControllerBase;

use Drupal\chado_search\Result\Fasta;
use Drupal\chado_search\Core\SessionVar;

class FastaController extends ControllerBase {

    public function download($search_id) {
        $url = chado_search_get_setting_by_id($search_id, 'path');
        $func = 'chado_search_' . $search_id . '_download_fasta_definition';
        if (function_exists($func)) {
          $feauture_id_column = $func();
        } else {
          $feauture_id_column = 'feature_id';
        }
        $fdl = new Fasta($search_id, $url);
        return new JsonResponse($fdl->createFasta($feauture_id_column));
    }
}