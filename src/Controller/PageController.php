<?php
namespace Drupal\chado_search\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

use Drupal\Core\Controller\ControllerBase;

use Drupal\chado_search\Result\Pager;

class PageController extends ControllerBase {

    public function switchPage($search_id, $page, $num_per_page) {
        $url = chado_search_get_setting_by_id($search_id, 'path');
        $num_per_page = chado_search_get_setting_by_id($search_id, 'num_per_page') ? chado_search_get_setting_by_id($search_id, 'num_per_page') : $num_per_page;
        $pg = new Pager($search_id, $url);
        return new JsonResponse($pg->switchPage($page, $num_per_page));
    }
}