<?php
namespace Drupal\chado_search\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

use Drupal\Core\Controller\ControllerBase;

use Drupal\chado_search\Result\Download;
use Drupal\chado_search\Core\SessionVar;

class DownloadController extends ControllerBase {

    public function download($search_id) {
        $url = chado_search_get_setting_by_id($search_id, 'path');
        $func = 'chado_search_' . $search_id . '_download_definition';
        $func_alt = SessionVar::getSessionVar($search_id, 'headers-callback');
        $headers = array();
        if (function_exists($func)) {
          $headers = $func();
        } else if (function_exists($func_alt)) { // Try using the table difinition if download difinition does not exist.
          $tmp = $func_alt();
          foreach ($tmp AS $k => $v) {
            $key = explode(":", $k);
            $headers[$key[0]] = $v;
          }
        }
        $dl = new Download($search_id, $url);
        return new JsonResponse($dl->createDownload($headers));
    }
}