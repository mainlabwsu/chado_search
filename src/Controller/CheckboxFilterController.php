<?php
namespace Drupal\chado_search\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

use Drupal\Core\Controller\ControllerBase;

use Drupal\chado_search\Core\SessionVar;

class CheckboxFilterController extends ControllerBase {

    public function do($search_id, $action, $item) {
        $data = SessionVar::getSessionVar($search_id, 'checkbox-filter-data');
        if ($action == 'add') {
          $data[$item] = $item;
          SessionVar::setSessionVar($search_id, 'checkbox-filter-data', $data);
        } else if ($action == 'remove') {
          unset($data[$item]);
          SessionVar::setSessionVar($search_id, 'checkbox-filter-data', $data);
        }
        return new JsonResponse(['update' => count($data)]);
    }
}