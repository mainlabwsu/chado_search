<?php

namespace Drupal\chado_search\Set\Csform;

class SetFieldset extends SetElement {
  
  private $start_widget = '';
  private $end_widget = '';
  private $collapsed = FALSE;
  private $description = NULL;
  private $clearboth = FALSE;
  
  /**
   * Setters
   * @return $this
   */
  public function startWidget ($start_widget) {
    $this->start_widget = $start_widget;
    return $this;
  }
  
  public function endWidget ($end_widget) {
    $this->end_widget = $end_widget;
    return $this;
  }
  
  public function collapsed ($collapsed) {
    $this->collapsed = $collapsed;
    return $this;
  }
  
  public function description ($description) {
    $this->description = $description;
    return $this;
  }
  
  public function clearboth () {
      $this->clearboth = TRUE;
      return $this;
  }
  
  /**
   * Getters
   */
  public function getStartWidget() {
    return $this->start_widget;
  }
  
  public function getEndWidget() {
    return $this->end_widget;
  }
  
  public function getCollapsed() {
    return $this->collapsed;
  }
  
  public function getDescription () {
    return $this->description;
  }
  
  public function getClearboth () {
      return $this->clearboth;
  }
}