<?php

namespace Drupal\chado_search\Set\Csform;

class SetDynamicSelectFilter extends SetElement {

  private $depend_on_id = '';
  private $callback = '';
  private $label_width = 0;
  private $size = 0;
  private $cacheTable = '';
  private $cacheColumns = array();
  private $reset_on_change_id;
  private $multiple = FALSE;
  private $alsoDependOn = array();
  private $hidden = FALSE;
  private $cached_opts = NULL;
  private $search_box = FALSE;

  /**
   * Setters
   * @return $this
   */
  public function dependOnId ($id) {
    $this->depend_on_id = $id;
    return $this;
  }

  public function labelWidth ($label_width) {
    $this->label_width = $label_width;
    return $this;
  }
  
  public function callback ($callback) {
    $this->callback = $callback;
    return $this;
  }

  public function size ($size) {
    $this->size = $size;
    return $this;
  }
  
  public function cache ($table, $columns = array()) {
    $this->cacheTable = $table;
    $this->cacheColumns = $columns;
    return $this;
  }
  
  public function resetOnChange($id) {
    $this->reset_on_change_id = $id;
    return $this;
  }
  
  public function multiple ($multiple) {
    $this->multiple = $multiple;
    return $this;
  }
  
  public function alsoDependOn ($ids) {
    $this->alsoDependOn = $ids;
    return $this;
  }
  
  public function hideIfNoOption ($bool) {
      $this->hidden = $bool;
      return $this;
  }
   
  public function cachedOpts($col_val, $col_filter, $sort = TRUE, $default_empty = FALSE, $also_depend_filters = []) {
    $this->cached_opts = [$col_val, $col_filter, $sort, $default_empty, $also_depend_filters];
    return $this;
  }
   
  public function searchBox ($values) {
    $this->search_box = $values;
    return $this;
  }

  /**
   * Getters
   */
  public function getDependOnId () {
    return $this->depend_on_id;
  }
  
  public function getCallback() {
    return $this->callback;
  }
  
  public function getLabelWidth () {
    return $this->label_width;
  }

  public function getSize () {
    return $this->size;
  }
  
  public function getCacheTable () {
    return $this->cacheTable;
  }
  
  public function getCacheColumns () {
    return $this->cacheColumns;
  }
  
  public function getResetOnChange() {
    return $this->reset_on_change_id;
  }
  
  public function getMultiple () {
    return $this->multiple;
  }
  
  public function getAlsoDependOn () {
    return $this->alsoDependOn;
  }
  
  public function getHideIfNoOption () {
      return $this->hidden;
  }
  
  public function getCachedOpts() {
    return $this->cached_opts;
  }
  
  public function getSearchBox () {
    return $this->search_box;
  }
}