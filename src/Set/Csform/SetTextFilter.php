<?php

namespace Drupal\chado_search\Set\Csform;

class SetTextFilter extends SetElement {
  
  private $size = 0;
  private $label_width = 0;
  private $required = FALSE;
  private $autocomplete_path;
  private $maxlength;
  private $numeric = FALSE;
  private $default_op;
  
  /**
   * Setters
   * @return $this
   */
  public function size ($size) {
    $this->size = $size;
    return $this;
  }
  
  public function maxlength ($size) {
    $this->maxlength = $size;
    return $this;
  }
  
  public function labelWidth ($label_width) {
    $this->label_width = $label_width;
    return $this;
  }

  public function required ($required) {
    $this->required = $required;
    return $this;
  }
  
  public function autoCompletePath ($path) {
    $this->autocomplete_path = $path;
    return $this;
  }
  
  public function numeric ($is_numeric) {
    $this->numeric = $is_numeric;
    return $this;
  }
  
  public function defaultOp ($op) {
    $this->default_op = $op;
    return $this;
  }
  
  /**
   * Getters
   */
  public function getSize () {
    return $this->size;
  }
  
  public function getMaxLength () {
    return $this->maxlength;
  }
  
  public function getLabelWidth () {
    return $this->label_width;
  }
  
  public function getRequired () {
    return $this->required;
  }
  
  public function getAutoCompletePath () {
    return $this->autocomplete_path;
  }
  
  public function getNumeric () {
    return $this->numeric;
  }
  
  public function getDefaultOp () {
    return $this->default_op;
  }
}