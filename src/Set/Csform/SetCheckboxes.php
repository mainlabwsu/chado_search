<?php

namespace Drupal\chado_search\Set\Csform;

class SetCheckboxes extends SetElement {

  private $options = array();
  
  /**
   * Setters
   * @return $this
   */  
  public function options ($options) {
    $this->options = $options;
    return $this;
  }
  
  /**
   * Getters
   */  
  public function getOptions () {
    return $this->options;
  }
  
}