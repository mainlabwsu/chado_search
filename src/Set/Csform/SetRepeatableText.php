<?php

namespace Drupal\chado_search\Set\Csform;

class SetRepeatableText extends SetElement {
  
  private $size = 0;
  private $required = FALSE;
  private $items = array();
  private $itemTypes = array();
  private $autocomplete_path;
  private $maxlength;
  
  /**
   * Setters
   * @return $this
   */
  public function size ($size) {
    $this->size = $size;
    return $this;
  }
  
  public function maxlength ($size) {
    $this->maxlength = $size;
    return $this;
  }
  
  public function required ($required) {
    $this->required = $required;
    return $this;
  }
  
  public function items ($items) {
    $this->items = $items;
    return $this;
  }
  
  public function itemTypes ($items) {
      $this->itemTypes = $items;
      return $this;
  }
  
  public function autoCompletePath ($path) {
    $this->autocomplete_path = $path;
    return $this;
  }
  
  /**
   * Getters
   */
  public function getSize () {
    return $this->size;
  }
  
  public function getMaxLength () {
    return $this->maxlength;
  }
  
  public function getRequired () {
    return $this->required;
  }
  
  public function getItems () {
    return $this->items;
  }
  
  public function getItemTypes () {
      return $this->itemTypes;
  }
  
  public function getAutoCompletePath () {
    return $this->autocomplete_path;
  }
}