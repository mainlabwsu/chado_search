<?php

namespace Drupal\chado_search\Set\Csform;

class SetTextField extends SetElement {
  
  private $required = FALSE;
  private $size;
  private $autocomplete_path;
  private $maxlength;

  /**
   * Setters
   * @return $this
   */
  public function required ($required) {
    $this->required = $required;
    return $this;
  }
  
  public function size ($size) {
    $this->size = $size;
    return $this;
  }
  
  public function maxlength ($size) {
    $this->maxlength = $size;
    return $this;
  }
  
  public function autoCompletePath ($path) {
    $this->autocomplete_path = $path;
    return $this;
  }

  /**
   * Getters
   */
  public function getRequired () {
    return $this->required;
  }
  
  public function getSize () {
    return $this->size;
  }
  
  public function getMaxLength () {
    return $this->maxlength;
  }
  
  public function getAutoCompletePath () {
    return $this->autocomplete_path;
  }
  
}