<?php

namespace Drupal\chado_search\Set\Csform;

class SetLabeledFilter extends SetElement {
  
  private $size = 0;
  private $required = FALSE;
  private $label_width = 0;
  private $autocomplete_path;
  private $maxlength;
  
  /**
   * Setters
   * @return $this
   */
  public function size ($size) {
    $this->size = $size;
    return $this;
  }
  
  public function required ($required) {
    $this->required = $required;
    return $this;
  }  

  public function labelWidth ($label_width) {
    $this->label_width = $label_width;
    return $this;
  }
  
  public function maxlength ($size) {
    $this->maxlength = $size;
    return $this;
  }
  
  public function autoCompletePath ($path) {
    $this->autocomplete_path = $path;
    return $this;
  }
  
  /**
   * Getters
   */
  public function getSize () {
    return $this->size;
  }
  
  public function getRequired () {
    return $this->required;
  }
  
  public function getLabelWidth () {
    return $this->label_width;
  }
  
  public function getMaxLength () {
    return $this->maxlength;
  }
  
  public function getAutoCompletePath () {
    return $this->autocomplete_path;
  }
}