<?php

namespace Drupal\chado_search\Set\Csform;

class SetSequenceRetrieval extends SetBase {

  private $title;
  private $desc;
  private $collapsible;
  private $collapsed;

  /**
   * Setters
   * @return $this
   */

  public function title ($title) {
    $this->title = $title;
    return $this;
  }

  public function description ($desc) {
    $this->desc = $desc;
    return $this;
  }

  public function collapsible($sollapsible) {
    $this->collapsible = $sollapsible;
    return $this;
  }

  public function collapsed($collapsed) {
    $this->collapsed= $collapsed;
    return $this;
  }


  /**
   * Getters
   */

  public function getTitle() {
    return $this->title;
  }

  public function getCollapsible() {
    return $this->collapsible;
  }

  public function getcollapsed() {
    return $this->collapsed;
  }

  public function getDescription() {
    return $this->desc;
  }

}