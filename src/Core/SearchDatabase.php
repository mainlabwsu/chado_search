<?php

namespace Drupal\chado_search\Core;

class SearchDatabase {

  // Get all column names for a table
  public static function getColumns ($table) {

    $sql = "SELECT column_name, data_type FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = :table";
    $result = chado_search_query($sql, array(':table' => $table), 'public,chado');
    return SearchDatabase::resultToArray($result, 'column_name');
  }

  // Convert the specified column of a result table into an array
  public static function resultToArray($result) {
    $arr = array();
    while ($obj = $result->fetchObject()) {
      array_push($arr, $obj);
    }
    return $arr;
  }
}