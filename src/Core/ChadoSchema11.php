<?php
namespace Drupal\chado_search\Core;

use Drupal\pgsql\Driver\Database\pgsql\Schema;

/* This class exists to address a Drupal 9 bug that it won't create table in
   Postgres' non-public schema. The createTableSQL is instead returned so it
   can be run against the non-public schema.
*/
class ChadoSchema11 extends Schema {

  public function __construct ($connection = NULL) {
    $this->uniqueIdentifier = uniqid('', TRUE);
    $this->connection = \Drupal::database();
  }

  public function createTableSQL ($name, $table) {
    return parent::createTableSQL($name, $table);
  }
}