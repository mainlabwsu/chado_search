<?php

namespace Drupal\chado_search\Core;

class SessionVar {

  public static function setSessionVar ($search_id, $var, $value) {
    if ($value || is_array($value)) {
      $form_build_id = $_POST['form_build_id'];
      $key = "$search_id-$form_build_id-$var";
      $key = $search_id == 'tripal_megasearch' && ($var == 'checkbox-filter' || $var == 'checkbox-filter-data') ? "$search_id-$var" : $key;
      if (function_exists('session_status') && session_status() == PHP_SESSION_NONE) {
        session_start();
      }
      $_SESSION[$key] = $value;
      // session_write_close();
      // dpm('SET ' . $key . ' TO ' . $value);
    }
  }
  
  public static function getSessionVar ($search_id, $var) {
    $form_build_id = $_POST['form_build_id'];
    $key = "$search_id-$form_build_id-$var";
    $key = $search_id == 'tripal_megasearch' && ($var == 'checkbox-filter' || $var == 'checkbox-filter-data') ? "$search_id-$var" : $key;
    $value = key_exists($key, $_SESSION) ? $_SESSION[$key] : NULL;
    // dpm('RECIEVED ' . $key . ' AS ' . $value);
    return $value;
  }
  
  public static function delSessionVar ($search_id, $var) {
    $form_build_id = $_POST['form_build_id'];
    $key = "$search_id-$form_build_id-$var";
    $key = $search_id == 'tripal_megasearch' && ($var == 'checkbox-filter' || $var == 'checkbox-filter-data') ? "$search_id-$var" : $key;
    if (is_array($_SESSION) && key_exists($key, $_SESSION)) {
      unset($_SESSION[$key]);
    }
  }
}