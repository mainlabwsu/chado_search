<?php

namespace Drupal\chado_search\Core;

use Drupal\chado_search\Set\Result\SetResult;

use Drupal\chado_search\Set\Csform\SetBetweenFilter;
use Drupal\chado_search\Set\Csform\SetCustomOutput;
use Drupal\chado_search\Set\Csform\SetSequenceRetrieval;
use Drupal\chado_search\Set\Csform\SetDynamicMarkup;
use Drupal\chado_search\Set\Csform\SetDynamicFieldset;
use Drupal\chado_search\Set\Csform\SetDynamicSelectFilter;
use Drupal\chado_search\Set\Csform\SetDynamicTextFields;
use Drupal\chado_search\Set\Csform\SetFieldset;
use Drupal\chado_search\Set\Csform\SetFile;
use Drupal\chado_search\Set\Csform\SetHidden;
use Drupal\chado_search\Set\Csform\SetLabeledFilter;
use Drupal\chado_search\Set\Csform\SetMarkup;
use Drupal\chado_search\Set\Csform\SetCheckboxes;
use Drupal\chado_search\Set\Csform\SetReset;
use Drupal\chado_search\Set\Csform\SetSelect;
use Drupal\chado_search\Set\Csform\SetSelectFilter;
use Drupal\chado_search\Set\Csform\SetSelectOptionFilter;
use Drupal\chado_search\Set\Csform\SetSelectShortCut;
use Drupal\chado_search\Set\Csform\SetButton;
use Drupal\chado_search\Set\Csform\SetClearButton;
use Drupal\chado_search\Set\Csform\SetSubmit;
use Drupal\chado_search\Set\Csform\SetTab;
use Drupal\chado_search\Set\Csform\SetTextArea;
use Drupal\chado_search\Set\Csform\SetTextAreaFilter;
use Drupal\chado_search\Set\Csform\SetTextField;
use Drupal\chado_search\Set\Csform\SetTextFilter;
use Drupal\chado_search\Set\Csform\SetRepeatableText;
use Drupal\chado_search\Set\Csform\SetThrobber;

/**
 * A control class to set configuration parameters
 * @author ccheng
 *
 */
class Set {

/**
 * Check conf type
 * @param $conf
 * @param $class
 */
  static public function check ($conf, $class) {
    $cls = chado_search_get_class($conf);
    if ($cls == $class) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Set configuration for search result
   */
  static public function result () {
    return new SetResult();
  }

  /**
   * Set configuration for form elements
   */
  static public function betweenFilter () {
    return new SetBetweenFilter();
  }

  static public function customOutput () {
    return new SetCustomOutput();
  }

  static public function sequenceRetrieval () {
     return new SetSequenceRetrieval();
  }

  static public function dynamicMarkup () {
    return new SetDynamicMarkup();
  }

  static public function dynamicFieldset () {
    return new SetDynamicFieldset();
  }

  static public function dynamicSelectFilter () {
    return new SetDynamicSelectFilter();
  }

  static public function dynamicTextFields () {
    return new SetDynamicTextFields();
  }

  static public function fieldset () {
    return new SetFieldset();
  }

  static public function file () {
    return new SetFile();
  }

  static public function hidden () {
    return new SetHidden();
  }

  static public function labeledFilter () {
    return new SetLabeledFilter();
  }

  static public function markup () {
    return new SetMarkup();
  }

  static public function checkboxes () {
    return new SetCheckboxes();
  }

  static public function reset () {
    return new SetReset();
  }

  static public function select () {
    return new SetSelect();
  }

  static public function selectFilter () {
    return new SetSelectFilter();
  }

  static public function selectOptionFilter () {
    return new SetSelectOptionFilter();
  }

  static public function selectShortCut () {
    return new SetSelectShortCut();
  }

  static public function button () {
    return new SetButton();
  }

  static public function clearButton () {
      return new SetClearButton();
  }

  static public function submit () {
    return new SetSubmit();
  }

  static public function tab () {
    return new SetTab();
  }

  static public function textArea () {
    return new SetTextArea();
  }

  static public function textAreaFilter () {
    return new SetTextAreaFilter();
  }

  static public function textField () {
    return new SetTextField();
  }

  static public function textFilter () {
    return new SetTextFilter();
  }

  static public function repeatableText () {
    return new SetRepeatableText();
  }

  static public function throbber () {
    return new SetThrobber();
  }

}