<?php
namespace Drupal\chado_search\Core;

use Drupal\Core\Render\Markup;

use Drupal\chado_search\Csform\ChadoSearchForm;

use Drupal\chado_search\Result\Table;
use Drupal\chado_search\Result\Pager;
use Drupal\chado_search\Result\Download;
use Drupal\chado_search\Result\CustomDownload;
use Drupal\chado_search\Result\Fasta;
use Drupal\chado_search\Result\SequenceRetrieval;
use Drupal\chado_search\Result\ResultQuery;

class ChadoSearch {

  public $search_id, $path, $number_per_page;

  // Return a ChadoSearch object
  public function __construct ($search_id, $path = NULL, $number_per_page = 20) {
    $this->search_id = $search_id;
    if ($path != NULL) {
      $this->path = $path;
    } else {
      $this->path = "chado_search/$search_id";
    }
    $pager_setting = chado_search_get_setting_by_id($search_id, 'num_per_page');
    $this->number_per_page = $pager_setting? $pager_setting : $number_per_page;
  }

  /****************************************************
   * Generating the results
   * $sql - an SQL without any condition (i.e. the WHERE clause) that defines the base table
   * $where - an array containing SQL conditions to apply to the base table. These SQLs will be concatenated by 'AND' and placed after the 'WHERE' keyword
   * $table_definition_callback - an array containing the header information of the result table. Optionally, settings (separated by :) can be specified in the key.
   *     - key: <column>:<(s)ortable>:<callback>:<argument1>:<argument2>:<argument3>...
   *     - value: displayed column name
   * $form_state: Drupal $form_state variable
   * $groupby - group the result by column(s). format = '<column>:<table>:<separator>'. You need to have a '*' in the SQL SELECT statement in order to be replaced by the arregated version of SELECT statement. If not, $groupby will be ignored.
   * $fasta_download - create a Fasta download link
   * $append - a free SQL string that will be append to the end of the statement
   * $disableCols - hide these columns from the result table. format = '<column1>;<column2>;<column3>;...'. To disable the Row Counter, pass in a column named 'row-counter'
   * $changeHeaders - change the title for these headers. format = '<column1>=<title1>;<column2>=<title2>;<column3>=<title3>;...'
   * $rewriteCols - rewrite the value in specified columns by passing the value to the specified function($value). format = '<column1>=<callback1>;<column2>=<callback2>;<column3>=<callback3>;...'. If callback name ends with a start '*', a row object is passed in instead of the value of current column.
   * $autoscroll - automatically scroll the page to the top of the result table
   * $customDownload - an array of 'key=value' pairs where the key is the download function to which it will be passed with $handle, $result, and $sql variables for writing output. The 'value' is the 'Display Text' on the page. To disable the default download, the array needs to contain the setting: array('disable_default' => TRUE)
   * $customFasta - the function to call to modified the SQL for Fasta download. This function will be passed with the current SQL (i.e. $sql variable) and should return the modified version of SQL which retrieves all 'feature_id' for the download
   * $showDownload - add download links to the up right conner of the table
   * $showPager - add pager to the bottom right conner of the table
   * $hideNullColumns - hide columns that contains only NULL values
   * $hstoreToColumns - split the specified hstore column into multiple columns according to the keys of passed in array. The array values will be used for displaying column headers
   * $defaultOrder - default order for the result table
   * $checkboxFilter - Create checkboxes for user to choose desired rows in the result table (for Download/Fasta/Sequence Retrieval). The results will be filtered on the column specified in this variable. 
  */
  // Main Result
  public function createResult (&$form_state, $conf) {

    try {
      $build_info = $form_state->getBuildInfo();
      if (!is_object($form_state) || !isset($build_info['form_id'])) {
        \Drupal::messenger()->addError('Fail to generate results. Please check the $form_state  you passed for the createResult($form_state, $conf) function.');
        return;
      }
      if (!is_object($conf) || !method_exists($conf, 'getSql')) {
        \Drupal::messenger()->addError('Fail to generate results. Please check the $conf  you passed for the createResult($form_state, $conf) function.');
        return;
      }

      // Get parameters from $conf
      $storage = &$form_state->getStorage();
      $sql = $conf->getSql() ? $conf->getSql() : $storage['base_sql'];
      $where = $conf->getWhere();
      $table_definition_callback = $conf->getTableDefinitionCallback();
      $groupby = $conf->getGroupby();
      $fasta_download = $conf->getFastaDownload();
      $sequence_retrieval = $conf->getSequenceRetrieval();
      $append = $conf->getAppend();
      $disableCols = $conf->getDisableCols();
      $changeHeaders = $conf->getChangeHeaders();
      $rewriteCols = $conf->getRewriteCols();
      $autoscroll = $conf->getAutoscroll();
      $customDownload = $conf->getCustomDownload();
      $customFasta = $conf->getCustomFasta();
      $showDownload = $conf->getShowDownload();
      $showPager = $conf->getShowPager();
      $hideNullColumns = $conf->getHideNullColumns();
      $hstoreToColumns = $conf->getHstoreToColumns();
      $defaultOrder = $conf->getDefaultOrder();
      $checkboxFilter = $conf->getCheckboxFilter();
      $defaultOrder = $defaultOrder ? $defaultOrder : $checkboxFilter;

      $search_id = $this->search_id;

      // Get custom outputs setting if it exists
      $select_cols = '';
      if ($form_state->getValue('custom_output_options')) {
        $custom_output = $form_state->getValue('custom_output_options');
        foreach ($custom_output AS $k => $v) {
          if (!$v) {
            $disableCols .= ";$k";
          }
          else {
            if ($k != 'row-counter') {
              $select_cols .= "$k,";
            }
          }
        }
      }

      // Prepare SQL
      $result_query = new ResultQuery($search_id, $sql);
      $result_query
        ->addWhere($where)
        ->addGroupBy($groupby)
        ->appendSQL($append);
      $sql = $result_query->getSQL();

      // Call header definition callback if exists
      $headers = array();
      if (function_exists($table_definition_callback)) {
        $headers = $table_definition_callback();
        SessionVar::setSessionVar($search_id, 'headers-callback', $table_definition_callback);
      }
      // Default header if header difinition does not exist
      else {
        $hsql = "SELECT * FROM ($sql LIMIT 1) T";
        $fields = array_keys(chado_search_query($hsql, array(), 'public,chado')->fetchAssoc());
        foreach($fields AS $field) {
          $headers[$field] = $field;
        }
        SessionVar::setSessionVar($search_id, 'default-headers', $headers);
        $header_keys = array_keys($headers);
      }

      // Customize output with DISTINCT in statement for selected columns
      $storage = &$form_state->getStorage();
      if (isset($storage['#custom_output-group_selection']) && $storage['#custom_output-group_selection']) {
        $max_cols = '';
        if (isset($srorage['#custom_output-max_columns'])) {
          $max_cols = $storage['#custom_output-max_columns'];
        }
        // $group_selection contains custimizable columns. add back the non-customizable columns to DISTINCT statement
        foreach ($header_keys AS $h_key) {
          $cols = explode(':', $h_key);
          $col = $cols[0];
          if (!key_exists($col, $custom_output)) {
            $select_cols .= $col . ',';
          }
        }
        $select_cols = rtrim($select_cols, ',');
        // Store original sql for FASTA download
        SessionVar::setSessionVar($search_id, 'fasta_sql', $sql);
        $sql = "SELECT $max_cols $select_cols FROM ($sql) SQL GROUP BY $select_cols";
        $result_query->setSQL($sql);
      }

      // Hide columns that contain only NULL values
      if ($hideNullColumns) {
        $nullCols = array();
        foreach ($headers AS $key => $value) {
          $token_key = explode(':', $key);
          $nullCols [] = $token_key[0];
        }
        $results = chado_search_query($sql, array(), 'public,chado');
        $counter_row = 0;
        while ($row = $results->fetchObject()) {
          if (count($nullCols) == 0) {
            break;
          }
          foreach ($nullCols AS $id => $colname) {
            // disable columns that are not in the SELECT statement & unset them from $nullCols
            if ($counter_row == 0 && !property_exists($row, $colname)) {
              unset ($nullCols[$id]);
              $disableCols .= ";$colname";
            }
            // unset columns that have values from $nullCols
            else if ($row->$colname) {
              unset ($nullCols[$id]);
            }
          }
          $counter_row ++;
        }
        // disable NULL columns
        foreach ($nullCols AS $nc) {
          $disableCols .= ";$nc";
        }
      }

      // Also Remove NULL hstore columns
      if (isset($hstoreToColumns['remove_null_columns']) && $hstoreToColumns['remove_null_columns']) {
        $hs_col = $hstoreToColumns['column'];
        $hs_data = $hstoreToColumns['data'];
          // Algorithm 1: Use a big SQL query that returns NULL columns (may be slow)
        /*
        $hs_sql = "";
        $counter_hs_k = 0;
        $total_hs_k = count($hs_data);
        // Get a list of not-null hstore columns
        $hs_sql = "WITH T AS ($sql) ";
        foreach ($hs_data AS $hs_k => $hs_v) {
          $hs_sql .= "(SELECT $hs_k AS keys FROM  T WHERE exist($hs_col, '$hs_k') LIMIT 1)";
          if ($counter_hs_k < $total_hs_k - 1) {
            $hs_sql .= " UNION ALL ";
          }
          $counter_hs_k ++;
        }
        $hs_keys = chado_search_query($hs_sql, array(), 'public,chado');
        // Create hstore new column data
        $new_data = array();
        while ($hs_key = $hs_keys->fetchField()) {
          $new_data [$hs_key] = $hs_data[$hs_key];
        }
        asort($new_data);
        $hstoreToColumns['data'] = $new_data;
        */

        // Algorithm 2: Loop through all results to determine NULL columns
        $nullCols = array();
        foreach ($hs_data AS $key => $value) {
          $nullCols [$key] = $value;
        }
        $results = chado_search_query($sql, array(), 'public,chado');
        while ($row = $results->fetchObject()) {
          if (count($nullCols) == 0) {
            break;
          }
          $pairs = chado_search_hstore_to_assoc($row->$hs_col);
          $pair_keys = array_keys($pairs);
          foreach ($pair_keys AS $pair_k) {
            unset($nullCols[$pair_k]);
          }
        }
        // disable NULL columns
        $nullColKeys = array_keys($nullCols);
        foreach ($nullColKeys AS $nk) {
          unset($hs_data[$nk]);
        }
        $hstoreToColumns['data'] = $hs_data;
      }

      // dpm($sql);
      $total_items = $result_query->count();
      $total_pages =Pager::totalPages($total_items, $this->number_per_page);

      // Prepare the result
      $div = "";
      // Show all result instead of just creating a search form
      if(isset($storage['#show_all_results'])) {
        $div ="<style type=\"text/css\">body {display: block;}</style>";
        $autoscroll = $storage['#show_all_results_scroll'];
      }

      // Show the first page
      if ($total_items != 0) {
        $limit_download = \Drupal::state()->get('chado_search_download_limit', 0);
        $limit_fasta = \Drupal::state()->get('chado_search_fasta_download_limit', 0);
        $limit_sequence_retrieval = \Drupal::state()->get('chado_search_sequence_retrieval_limit', 50000);

        // Store settings to session variables for result table/download modification if needed
        SessionVar::setSessionVar($search_id, 'disabled-columns', $disableCols);
        SessionVar::setSessionVar($search_id, 'changed-headers', $changeHeaders);
        SessionVar::setSessionVar($search_id, 'rewrite-columns', $rewriteCols);
        SessionVar::setSessionVar($search_id, 'custom-fasta-download', $customFasta);
        SessionVar::setSessionVar($search_id, 'autoscroll', $autoscroll);
        SessionVar::setSessionVar($search_id, 'total-items', $total_items);
        SessionVar::setSessionVar($search_id, 'hstore-to-columns', $hstoreToColumns);
        SessionVar::setSessionVar($search_id, 'checkbox-filter', $checkboxFilter);
        SessionVar::setSessionVar($search_id, 'checkbox-filter-data', []);

        // Build the result
        $div .=
        "<div id=\"$search_id-result-summary\" class=\"chado_search-result-summary\">
            <div id=\"$search_id-result-count\" class=\"chado_search-result-count\">
              <strong>$total_items</strong> records were returned
            </div>";

        // Add Download(s)
        if ($showDownload) {
          $has_fasta = FALSE;
          $has_srs = FALSE;
          $has_dl = FALSE;

          // Fasta Download
          if ($fasta_download) {
            if ($limit_fasta == 0 || $total_items <= $limit_fasta) {
              $fasta = new Fasta($this->search_id, $this->path);
              $div .= $fasta->getSrc();
              $has_fasta = TRUE;
            }
            else {
              \Drupal::messenger()->addMessage('FASTA Download disabled due to large number of returning results. (Current limit: '. $limit_fasta. ' records)');
            }
          }

          // Sequence Retrieval
          if (isset($sequence_retrieval['landmark_id'])) {
            $up = $form_state->getValue('sr_upstream');
            $down = $form_state->getValue('sr_downstream');
            if ($up && $up < 0) {
              \Drupal::messenger()->addError('Negative value not allowed for upstream bases');
            }
            else if ($down && $down < 0) {
              \Drupal::messenger()->addError('Negative value not allowed for downstream bases');
            }
            else if ($up && !is_numeric($up)) {
              \Drupal::messenger()->addError('Please input a number for upstream bases');
            }
            else if ($down && !is_numeric($down)) {
              \Drupal::messenger()->addError('Please input a number for downstream bases');
            }
            else if (is_numeric($up) && is_numeric($down)) {
              if ($total_items > $limit_sequence_retrieval) {
                \Drupal::messenger()->addMessage('Sequence Retrieval disabled due to large number of returning results. (Current limit: '. $limit_sequence_retrieval. ' records)');
              }
              else {
                $sequence_retrieval['upstream'] = $up;
                $sequence_retrieval['downstream'] = $down;
                SessionVar::setSessionVar($search_id, 'sequence-retrieval', $sequence_retrieval);
                $sr = new SequenceRetrieval($this->search_id, $this->path);
                if ($has_fasta) {
                  $div .= "<div id=\"$search_id-download-separator\" class=\"chado_search-download-separator\"> | </div>";
                }
                $div .= $sr->getSrc();
                $has_srs = TRUE;
              }
            }
          }

          if ($limit_download == 0 || $total_items <= $limit_download) {
            if ($has_fasta || $has_srs) {
              $div .= "<div id=\"$search_id-download-separator\" class=\"chado_search-download-separator\"> | </div>";
            }
            // Custom Download(s)
            $custom_dl = new CustomDownload($search_id, $customDownload);
            $div .= $custom_dl->getSrc();


            // Table Download
            $dl_default = isset($customDownload['disable_default']) ? FALSE : TRUE;
            $download = new Download($this->search_id, $this->path, $dl_default);
            $div .= $download->getSrc();
            $has_dl = TRUE;
          }
          else {
            \Drupal::messenger()->addMessage('Download disabled due to large number of returning results. (Current limit: '. $limit_download. ' records)');
          }
          if ($has_dl || $has_fasta || $has_srs) {
            // Download Label
            $div .=
              "<div id=\"$search_id-download-label\" class=\"chado_search-download-label\">
                  Download
              </div>";
          }
        }

        $div .= "</div>";

        // Add Table
        if ($defaultOrder) {
          $lsql = "$sql ORDER BY $defaultOrder LIMIT $this->number_per_page;";
          SessionVar::setSessionVar($search_id, 'download-order', $defaultOrder);
        } else {
          $lsql = "$sql LIMIT $this->number_per_page;";
        }
        $result = chado_search_query($lsql, array(), 'public,chado');
        $table = new Table($this->search_id, $result, 0, $this->number_per_page, $headers, $defaultOrder, $autoscroll);
        $div .= $table->getSrc();

        // Add Pager (and code for switching pages/sorting results)
        $pager = new Pager($this->search_id, $this->path, $total_pages, $showPager);
        $div .= $pager->getSrc();

      // If there is no result, show the following message
      } else {
        $div .=
          "<div id=\"$search_id-no-result\" class=\"chado_search-no-result\">
              <strong>0</strong> records were returned.
            </div>";
      }

      // Attach the result to form
      $form_state->setValue('result', Markup::create($div));
      $form_state->setRebuild(TRUE);
    } catch (\Exception $e) {
      $trace = debug_backtrace();
      $caller = $trace[1];
      //\Drupal::logger('chado_search')->error('Error: Query failed. calling from ' . $caller['function'] . ' (line ' . $caller['line'] . ' of ' . $caller['file'] . '). ' . $e->getMessage());
      \Drupal::messenger()->addError('Error: Query failed. calling from ' . $caller['function'] . ' (line ' . $caller['line'] . ' of ' . $caller['file'] . '). ' . $e->getMessage());
    }
  }
}
