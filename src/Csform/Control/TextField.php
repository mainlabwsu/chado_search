<?php

namespace Drupal\chado_search\Csform\Control;

class TextField extends Element {
  
  public function __construct($search_name, $id) {
    parent::__construct($search_name, $id, strtolower(chado_search_get_class($this)));
  }
  
  public function setForm(&$form, &$form_state) {
    // Prevent 'Enter' from submitting the form
    $form[$this->id]['#attributes'] = array('onkeypress' => "if(window.event.keyCode == 13) {return false;}");
  }
}