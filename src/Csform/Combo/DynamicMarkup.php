<?php

namespace Drupal\chado_search\Csform\Combo;

class DynamicMarkup extends Filter {

  public $depend_on_id;
  public $callback;

  public function setForm (&$form, &$form_state) {
    $search_name = $this->search_name;
    $id = $this->id;
    $depend_on_id = $this->depend_on_id;
    $depend_on_element = &find_first_form_element_by_id($form, $depend_on_id);

    // Add Ajax to the depending element
    $selected = $form_state->getValue($depend_on_id);
    $depend_on_element['#ajax'] = array(
      'callback' => 'chado_search_ajax_form_update',
      'wrapper' => "chado_search-filter-$search_name-$id-field",
      'effect' => 'fade'
    );
    $depend_on_element['#attribute'] = array ('update' => $id);

    $callback = $this->callback;
    if (function_exists($callback)) {
      $selected_value = is_array($selected) ? array_shift($selected) : $selected;
      $markup = $callback($selected_value, $form, $form_state);
      // If Clear button is clicked to clear all values
      $triggering_element = &$form_state->getTriggeringElement();
      if (isset($triggering_element['#id']) && $triggering_element['#id'] == 'chado_search-id-clear-all-values' && $triggering_element['#type'] == 'button') {
          $markup = '';
      }

      $form [$id] = array(
        '#id' => 'chado_search-id-' . $id,
        '#markup' => $markup,
        '#prefix' => "<div id=\"chado_search-filter-$search_name-$id-field\" class=\"chado_search-filter chado_search-widget form-item\">",
        '#suffix' => "</div>"
      );
    }
    else {
      \Drupal::messenger()->addError(t("Fatal Error: DynamicMarkup ajax function not implemented"));
    }
  }

}