<?php

namespace Drupal\chado_search\Csform\Combo;

use Drupal\Core\Render\Markup;

use Drupal\chado_search\Core\Set;

class SelectFilter extends Filter {

  public $title;
  public $column;
  public $table;
  public $condition;
  public $required;
  public $multiple;
  public $column_natural_sort;
  public $optgroup;
  public $optgroup_by_pattern;
  public $cache;
  public $label_width;
  public $size;
  public $disables;
  public $only;
  public $search_box;
  public $nullable;

  public function setForm (&$form, &$form_state) {

    try {
      $search_name = $this->search_name;
      $id = $this->id;
      $title = $this->title;
      $column = $this->column;
      $table = $this->table;
      $condition = $this->condition;
      $where = '';
      if ($condition) {
        $where = 'WHERE ' . $condition;
      }
      $required = $this->required;
      $multiple = $this->multiple;
      $column_natural_sort = $this->column_natural_sort;
      $optgroup = $this->optgroup;
      $optgroup_by_pattern = $this->optgroup_by_pattern;
      $cache = $this->cache;
      $width = '';
      if ($this->label_width) {
        $width = "style=\"width:" . $this->label_width ."px\"";
      }
      $size = $this->size;

      $id_label = $id . "_label";
      $options = array();
      if (!$required) {
        $options[0] = 'Any';
      }
      if ($this->nullable) {
        $options['0_null'] = 'Not in Any';
      }
      if (is_array($column)) {
        $col = "";
        $group = "";
        for ($i = 0; $i < count($column); $i ++) {
          $col .= $column[$i];
          $group .= $column[$i];
          if ($i < count($column) - 1) {
            $col .= " || ' ' || ";
            $group .= ", ";
          }
        }
        $sql = "SELECT $col AS key FROM " . $table . " $where GROUP BY $group ORDER BY key";
        $results = chado_search_query($sql, array(), 'public,chado');
        while ($obj = $results->fetchObject()) {
          if (trim($obj->key) != "") {
            $options[$obj->key] = $obj->key;
          }
        }
      } else if ($column && $table) {
        $tname = "cache_" . $table . "_" . $column . "_" . $id;
        $tname = substr($tname, 0, 63); // Unfortunately, postgres has a table name limit of 63 chars

        if ($cache) {// If cache is on, create a cache table
          //Tripal MView last updated time
          $lastupdate = -1;
          /* Remove the dependency on Tripal
          if (chado_search_table_exists('tripal_mviews')) {
            $lastupdate = chado_search_query(
              "SELECT last_update FROM tripal_mviews WHERE mv_table = :mv_table",
              array(':mv_table' => $table)
            )->fetchField();
          }
          */
          // MView updated time using the chado_search drush command
          $csupdate = \Drupal::state()->get('chado_search_time_' . $table, 0);
          $newer = $lastupdate > $csupdate ? $lastupdate : $csupdate;
          if (!chado_search_table_exists($tname)) { // If the cache table not exists, create it
            chado_search_query("SELECT distinct $column INTO $tname FROM " . $table . " $where", array(), 'public,chado');
            \Drupal::state()->set("chado_search_cache_last_update_" . $tname . "_" . $column, $newer);
          } else { // If cache table exists, check if it's up-to-date. If it's not up-to-date, recreate the cache table
            if (\Drupal::state()->get("chado_search_cache_last_update_" . $tname . "_" . $column, "") != $lastupdate &&
                \Drupal::state()->get("chado_search_cache_last_update_" . $tname . "_" . $column, "") != $csupdate) {
              chado_search_query("DROP table $tname");
              chado_search_query("SELECT distinct $column INTO $tname FROM " . $table . " $where", array(), 'public,chado');
              \Drupal::state()->set("chado_search_cache_last_update_" . $tname . "_" . $column, $newer);
            }
          }
          $table = $tname; // Use cached table instead of the original table if cache is on.
          $where = ''; // Remove the condition if cache is TRUE since we have use the condition to populate the cache table

        } else { // If cache is off, remove the cache table
          if (chado_search_table_exists($tname)) {
            chado_search_query("DROP table $tname");
            \Drupal::state()->delete("chado_search_cache_last_update_" . $tname . "_" . $column);
          }
        }

        if ($column_natural_sort) {
          $s_sql =
            "SELECT distinct $column,
               CASE WHEN regexp_replace($column, E'\\\D','','g') = ''
               THEN 999999999
               ELSE regexp_replace($column, E'\\\D','','g')::numeric
               END AS sortkey
             FROM " . $table ." $where
             ORDER BY sortkey";
        } else {
          $s_sql = "SELECT distinct $column FROM " . $table . " $where ORDER BY $column";
        }
        $results = NULL;
        if ($cache) {
          $results = chado_search_query($s_sql);
        }
        else {
          $results = chado_search_query($s_sql, array(), 'public,chado');
        }
        while ($obj = $results->fetchObject()) {
          if (isset($obj->$column) && trim($obj->$column) != "") {
            if (!is_array($this->disables) || !in_array($obj->$column, $this->disables)){
              if (!is_array($this->only) || in_array($obj->$column, $this->only)) {
                $options[$obj->$column] = $obj->$column;
              }
            }
          }
        }
      }
      if ($optgroup) {
        $opts = array();
        $opts ['Common Selections'] = array();
        foreach ($optgroup AS $k => $val) {
          if ($val == 'Any' || $val == 'Not in Any') {
            $opts['Common Selections'][$k] = $val;
            unset ($options[$k]);
          }
          else {
            $opts['Common Selections'][$val] = $val;
          }
        }
        $opts ['All Options'] = $options;
        $options = $opts;
      } else if ($optgroup_by_pattern) {
        $groupedopt = array();
        $other = array();
        foreach ($options AS $k_op => $v_op) {
          $found = 0;
          if ($v_op == 'Any' || $v_op == 'Not in Any') {
            $groupedopt[$k_op] = $v_op;
            continue;
          }
          foreach ($optgroup_by_pattern AS $k_group => $v_group) {
            if (preg_match("/$v_group/", $v_op)) {
              $found = 1;
              $groupedopt[$k_group][$v_op] = $v_op;
              break;
            }
          }
          if (!$found && !key_exists('Other', $optgroup_by_pattern)) {
            $other['Other'][$v_op] = $v_op;
          }
        }
        ksort($groupedopt, SORT_STRING);
        $groupedopt = array_merge($groupedopt, $other);
        $options = $groupedopt;
      }
      $search_box = '';
      if ($this->search_box) {
          $js =
          "<script type=\"text/javascript\">
              function search_options_$id (txt) {
                var find = txt.value.toLowerCase();
                var select = document.getElementById('chado_search-filter-$search_name-$id-field');
                var options = select.getElementsByTagName('option');
                for (var i = 0; i < options.length; i ++) {
			     if (options[i].innerHTML.toLowerCase().includes(find)) {
                    options[i].style.display = 'block';
                 }
                 else {
                    options[i].style.display = 'none';
                 }
		        }
              }
           </script>";
          $search_box = "$js<input id=\"chado_search-filter-$search_name-$id-search_box\" class=\"form-control\" type=\"text\" autocomplete=\"off\" onkeyup=\"search_options_$id(this)\">";
      }
      if ($title) {
        $this->csform->addMarkup(Set::markup()->id($id_label)->text($title));
        $form[$id_label]['#prefix'] = Markup::create(
          "<div id=\"chado_search-filter-$search_name-$id\" class=\"chado_search-filter chado_search-widget\">
             <div id=\"chado_search-filter-$search_name-$id-label\" class=\"chado_search-filter-label form-item\" $width>");
        $form[$id_label]['#suffix'] =
          "  </div>";
        $this->csform->addSelect(Set::select()->id($id)->options($options)->multiple($multiple)->size($size));
        $form[$id]['#prefix'] = Markup::create(
          "<div id=\"chado_search-filter-$search_name-$id\" class=\"chado_search-filter chado_search-widget\">
            <div id=\"chado_search-filter-$search_name-$id-field\" class=\"chado_search-filter-field\">$search_box");
      } else {
        $this->csform->addSelect(Set::select()->id($id)->options($options)->multiple($multiple)->size($size));
        $form[$id]['#prefix'] = Markup::create(
          "<div id=\"chado_search-filter-$search_name-$id\" class=\"chado_search-filter chado_search-widget\">
             <div id=\"chado_search-filter-$search_name-$id-field\" class=\"chado_search-filter-field\">$search_box");
      }
      if ($title) {
        $form[$id]['#suffix'] =
        "   </div>
           </div>
          </div>";
      }
      else {
        $form[$id]['#suffix'] =
          "   </div>
             </div>";
      }
    } catch (\PDOException $e) {
      \Drupal::messenger()->addError(t("Unable to create SelectFilter form element. Please check your settings. " . $e->getMessage()));
    }
  }
}