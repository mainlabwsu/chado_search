<?php

namespace Drupal\chado_search\Csform\Combo;

class SequenceRetrieval extends Filter {

  public $title;
  public $desc;
  public $collapsible;
  public $collapsed;

  public function setForm (&$form, &$form_state) {
    $limit_sequence_retrieval = \Drupal::state()->get('chado_search_sequence_retrieval_limit', 50000);
    $limit_sequence_retrieval_base_digits = \Drupal::state()->get('chado_search_sequence_retrieval_base_digits', 4);
    $search_name = $this->search_name;
    $id = $this->id;
    $title = $this->title ? $this->title : 'Sequence retrieval';
    $desc = $this->desc ? $this->desc : '<i>Warning: this may take hours if too many sequences are being downloaded.
    Please do not start two sequence retrieval jobs at the same time.</i></br>
    To create a job, specify upstream and downstream bases to include with the sequences. The job won\'t be started until
    the Download \'Sequence Retrieval\' link is clicked. (Limit for job creation: ' . $limit_sequence_retrieval . ' records)';

    $form[$id] = array(
      '#id' => 'chado_search-id-' . $id,
      '#type' => 'details',
      '#collapsible' => isset($this->collapsible) ? $this->collapsible : TRUE,
      '#collapsed' => isset($this->collapsed) ? $this->collapsed : TRUE,
      '#title' => $title,
      '#description' => $desc,
      '#prefix' => "<div id=\"chado_search-$search_name-$id-widget\" class=\"chado_search-custom_output-widget chado_search-widget\">",
      '#suffix' => '</div>'
    );
    $form[$id]['sr_upstream'] = array(
      '#type' => 'textfield',
      '#title' => 'Upstream bases',
      '#size' => $limit_sequence_retrieval_base_digits,
      '#maxlength' => $limit_sequence_retrieval_base_digits,
      '#prefix' => "<div id=\"chado_search-$search_name-$id-upstream\" class=\"chado_search-filter-label form-item\">",
      '#suffix' => '</div>'
    );
    $form[$id]['sr_downstream'] = array(
      '#type' => 'textfield',
      '#title' => 'Downstream bases',
      '#size' => $limit_sequence_retrieval_base_digits,
      '#maxlength' => $limit_sequence_retrieval_base_digits,
      '#prefix' => "<div id=\"chado_search-$search_name-$id-downstream\" class=\"chado_search-filter-label form-item\">",
      '#suffix' => '</div>'
    );
  }
}