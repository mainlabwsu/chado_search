<?php

namespace Drupal\chado_search\Csform\Combo;

use Drupal\Core\Render\Markup;

class Fieldset extends Filter {

  public $id;
  public $title;
  public $start_widget;
  public $end_widget;
  public $desc;
  public $collapsed;
  public $clearboth;

  public function setForm (&$form, &$form_state) {
    $search_name = $this->search_name;
    $id = $this->id;
    $title = $this->title;
    $start_widget = $this->start_widget;
    $end_widget = $this->end_widget;
    $desc = $this->desc;
    $collapsed = $this->collapsed;
    
    $start_element = &find_first_form_element_by_id($form, $start_widget);
    $start_label_element = &find_first_form_element_by_id($form, $start_widget . '_label');
    $end_element = &find_first_form_element_by_id($form, $end_widget);

    $prefix = $start_label_element['#prefix'];
    $display = '';
    $style = '';

    if ($collapsed) {
      $style = 'style="border-left:0px;border-right:0px;border-bottom:0px;"';
      $display .= 'style="display:none;"';
    }

    $clearboth = "";
    if($this->clearboth) {
        $clearboth = "style=\"clear:both\"";
    }

    $append =
    "<fieldset id=\"chado_search-fieldset-$search_name-$id\" class=\"chado_search-fieldset\" $clearboth $style>";
    if ($title) {
      $append .=
        "<legend  id=\"chado_search-fieldset-$search_name-$id-legend\" class=\"chado_search-fieldset-legend\" onClick=\"chado_search_fieldset_toggle(this);return false;\">
            <a href=\"#\">$title</a>
         </legend>";
    }
    $append .=
      "<div id=\"chado_search-fieldset-$search_name-$id-content\" class=\"chado_search-fieldset-content\" $display>";
    if ($desc) {
      $append .=
        "<div id=\"chado_search-fieldset-$search_name-$id-description\" class=\"chado_search-fieldset-description\">
            $desc
         </div>";
    }
    $append .= $prefix;
    $append = Markup::create($append);

    // If the start_widget has a label, add the fieldset before the label.
    if (!$prefix) {
      $prefix = $start_element['#prefix'];
      $start_element['#prefix'] = $append;
    // Otherwise, add the fieldset before the start_widget
    } else {
      $start_label_element['#prefix'] = $append;
    }
    $suffix = $end_element['#suffix'];
    $end_element['#suffix'] = Markup::create($suffix . "</div></fieldset>");
  }
}