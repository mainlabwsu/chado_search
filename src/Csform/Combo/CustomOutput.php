<?php

namespace Drupal\chado_search\Csform\Combo;

class CustomOutput extends Filter {

  public $options;
  public $defaults;
  public $title;
  public $desc;
  public $collapsible;
  public $collapsed;
  public $group_selection;
  public $max_columns;
  public $row_counter;

  public function setForm (&$form, &$form_state) {
    $search_name = $this->search_name;
    $id = $this->id;
    $title = $this->title ? $this->title : 'Customize output';
    $desc = $this->desc ? $this->desc : 'Select columns to display in the result table.';
    $options = $this->options;
    $columns = $this->row_counter ? array('row-counter' => 'Row counter') : array();
    foreach ($options AS $k => $v) {
      $key = explode(':', $k);
      $col = $key[0];
      $columns[$col] = $v;
    }
    $defaults = $this->row_counter ? array('row-counter') : array();
    $defaults = is_array($this->defaults) ? array_merge($defaults, $this->defaults) : array_merge($defaults, array_keys($columns));
    $form[$id] = array(
      '#id' => 'chado_search-id-' . $id,
      '#type' => 'details',
      '#open' => isset($this->collapsed) ? $this->collapsed : FALSE,
      '#title' => $title,
      '#description' => $desc,
      '#prefix' => "<div id=\"chado_search-$search_name-$id-widget\" class=\"chado_search-custom_output-widget chado_search-widget\">",
      '#suffix' => '</div>'
    );
    $form[$id]['custom_output_options'] = array(
      '#type' => 'checkboxes',
      '#options' => $columns,
      '#default_value' => $defaults,
    );

    $form['#custom_output-group_selection'] = $this->group_selection;
    if ($this->max_columns) {
      $prefix = '';
      foreach ($this->max_columns AS $col) {
        $prefix .= "max($col) AS $col,";
      }
      $form['#custom_output-max_columns'] = $prefix;
    }
  }
}