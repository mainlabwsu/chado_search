<?php

namespace Drupal\chado_search\Csform\Combo;

use Drupal\Core\Render\Markup;

class DynamicFieldset extends Filter {

  public $depend_on_id;
  public $title;
  public $description;
  public $collapsible;
  public $collapsed;
  public $width;
  public $display;

  public function setForm (&$form, &$form_state) {
    $search_name = $this->search_name;
    $id = $this->id;
    $depend_on_id = $this->depend_on_id;
    $title = $this->title;
    $desc = $this->description;
    $collapsible = $this->collapsible;
    $collapsed = $this->collapsed;
    $style = 'style="';
    if ($this->width) {
      if (preg_match('/\d+%$/', $this->width)) {
        $style .=  "width:" . $this->width . ";";
      }
      else {
        $style .=  "width:" . $this->width . "px" . ";";
      }
    }

    if ($this->display) {
      $style .= 'display:' . $this->display;
    }
    $style .= '"';
    $js = '';
    if ($this->display) {
      $js =
        '<script type=\"text/javascript\">
          (function ($) {
             $(document).ready(
               function() {
                 $(\'#chado_search-filter-' . "$search_name-$id" . '-field\').css("display","' . $this->display . '");
               }
             )
          })(jQuery)</script>';
    }
    else {
      $default_fieldset = \Drupal::state()->get('chado_search_fieldset_behavior', 'default');
      if ($default_fieldset == 'accordion') {
        $js = "<script type=\"text/javascript\">
            (function ($) {
              function chado_search_fieldset_ready() {
                $('.chado_search-fieldset').each(function (index, object) {
                  var id = $(object).attr('id');
                  var content = '#' + id + '-content';
                  if (index == 0) {
                    $(content).show();
                    $(content).parent().css('border-bottom', '');
                    $(content).parent().css('border-left', '');
                    $(content).parent().css('border-right', '');
                    $(content).parent().css('padding-top', '');
                    $(content).parent().css('padding-bottom', '');
                  }
                  else {
                    $(content).hide();
                    $(content).parent().css('border-bottom', '0px');
                    $(content).parent().css('border-left', '0px');
                    $(content).parent().css('border-right', '0px');
                    $(content).parent().css('padding-top', '0px');
                    $(content).parent().css('padding-bottom', '0px');
                  }
                });
            }
            $(document).ready(function () {
              chado_search_fieldset_ready();
            });
          })(jQuery);
        </script>";
      }
      else if ($default_fieldset == 'open_first') {
        $js = "<script type=\"text/javascript\">
            (function ($) {
              function chado_search_fieldset_ready() {
                $('.chado_search-fieldset').each(function (index, object) {
                  var id = $(object).attr('id');
                  var content = '#' + id + '-content';
                  if (index == 0) {
                    $(content).show();
                    $(content).parent().css('border-bottom', '');
                    $(content).parent().css('border-left', '');
                    $(content).parent().css('border-right', '');
                    $(content).parent().css('padding-top', '');
                    $(content).parent().css('padding-bottom', '');
                  }
                  else {
                    $(content).hide();
                    $(content).parent().css('border-bottom', '0px');
                    $(content).parent().css('border-left', '0px');
                    $(content).parent().css('border-right', '0px');
                    $(content).parent().css('padding-top', '0px');
                    $(content).parent().css('padding-bottom', '0px');
                  }
                });
            }
            $(document).ready(function () {
              chado_search_fieldset_ready();
            });
          })(jQuery);
        </script>";
      }
      
    }

    $depend_on_element = &find_first_form_element_by_id($form, $depend_on_id);
    // Add Ajax to the depending element
    $depend_on_element['#ajax'] = array(
      'callback' => 'chado_search_ajax_form_update',
      'wrapper' => "chado_search-filter-$search_name-$id-field",
      'effect' => 'fade'
    );
    if(isset($depend_on_element['#attribute']['update'])) {
      $updates = $depend_on_element['#attribute']['update'];
      if (!is_array($updates)) {
        $updates = array($updates => array('wrapper' => "chado_search-filter-$search_name-$updates-field"));
      }
      $updates[$id] = array('wrapper' => "chado_search-filter-$search_name-$id-field");
      $depend_on_element['#attribute'] = array ('update' => $updates);
    }
    else {
      $depend_on_element['#attribute'] = array ('update' => $id);
    }
    $type = $collapsible ? 'details' : 'fieldset';
    $form [$id] = array(
      '#id' => 'chado_search-id-' . $id,
      '#type' => $type,
      '#title' => $title,
      '#description' => $desc,
      '#open' => !$collapsed,
      '#prefix' => Markup::create("$js<div id=\"chado_search-filter-$search_name-$id-field\" class=\"chado_search-filter chado_search-widget form-item\" $style>"),
      '#suffix' => "</div>",
      '#attributes' => array('class' => array('chado_search-filter-details', 'chado_search-widget', 'form-item'))
    );
  }
}