<?php

namespace Drupal\chado_search\Csform\Combo;

use Drupal\Core\Render\Markup;

use Drupal\chado_search\Core\Set;

class TextFilter extends Filter {

  public $title;
  public $required;
  public $label_width;
  public $size;
  public $maxlength;
  public $autocomplete_path;
  public $numeric;
  public $default_op;

  public function setForm (&$form, &$form_state) {
    $search_name = $this->search_name;
    $id = $this->id;
    $id_label = $id . "_label";
    $id_op = $id . "_op";
    $width = '';
    if ($this->label_width) {
      $width = "style=\"width:" . $this->label_width ."px\"";
    }
    $options = array();
    if ($this->numeric) {
      if (strtolower($this->title) == 'stop' && $this->default_op == NULL) {
         $this->default_op = '<';
      }
      $options = array('>' => '>', '=' => '=', '<' => '<');
    }
    else {
        $options = array ('contains' => 'contains', 'exactly' => 'exactly', 'starts' => 'starts with', 'ends' => 'ends with', 'not_contain' => 'does not contain', 'is_empty' => 'is empty');
    }
    $this->csform->addMarkup(Set::markup()->id($id_label)->text($this->title));
    if ($this->default_op) {
      $this->csform->addSelect(Set::select()->id($id_op)->options($options)->defaultValue('<'));
    }
    else {
      $this->csform->addSelect(Set::select()->id($id_op)->options($options));
    }
    $this->csform->addTextfield(Set::textField()->id($id)->required($this->required)->size($this->size)->maxlength($this->maxlength)->autoCompletePath($this->autocomplete_path));
    $form[$id_label]['#prefix'] = Markup::create(
      "<div id=\"chado_search-filter-$search_name-$id\" class=\"chado_search-filter chado_search-widget\">
         <div id=\"chado_search-filter-$search_name-$id-label\" class=\"chado_search-filter-label form-item\" $width>");
    $form[$id_label]['#suffix'] =
      "  </div>";
    $form[$id_op]['#prefix'] =
      "  <div  id=\"chado_search-filter-$search_name-$id-op\" class=\"chado_search-filter-op\">";
    $form[$id_op]['#suffix'] =
      "  </div>";
    $form[$id]['#prefix'] =
      "  <div id=\"chado_search-filter-$search_name-$id-field\" class=\"chado_search-filter-field\">";
    $form[$id]['#suffix'] =
      "  </div>
        </div>";
  }
}