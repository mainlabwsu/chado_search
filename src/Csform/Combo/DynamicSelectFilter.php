<?php

namespace Drupal\chado_search\Csform\Combo;

use Drupal\Core\Render\Markup;

use Drupal\chado_search\Core\Set;

class DynamicSelectFilter extends Filter {

  public $title;
  public $depend_on_id;
  public $callback;
  public $label_width;
  public $size;
  public $cacheTable;
  public $cacheColumns;
  public $reset_on_change_id;
  public $multiple;
  public $alsoDependOn;
  public $hidden;
  public $cached_opts;
  public $search_box;

  public function setForm (&$form, &$form_state) {

    $search_name = $this->search_name;
    $id = $this->id;

    $tname = "cache_" . $search_name . '_' . $id;
    $tname = substr($tname, 0, 63);
    // Create cache table if specified
    if ($this->cacheTable && count($this->cacheColumns) > 0) {
      $cols = '';
      $counter = 0;
      foreach ($this->cacheColumns AS $col) {
        $cols .= $col;
        if ($counter < count ($this->cacheColumns) - 1) {
          $cols .= ', ';
        }
        $counter ++;
      }
      //Tripal MView last updated time
      $lastupdate = 0;
      /* Remove the dependency on Tripal
      if (chado_search_table_exists('tripal_mviews')) {
        $lastupdate = chado_search_query(
          "SELECT last_update FROM tripal_mviews WHERE mv_table = :mv_table",
          array(':mv_table' => $this->cacheTable)
          )->fetchField();
      }
      */
      
      // MView updated time using the chado_search drush command
      $csupdate = \Drupal::state()->get('chado_search_time_' . $this->cacheTable, 0);
      $newer = $lastupdate > $csupdate ? $lastupdate : $csupdate;
      if (!chado_search_table_exists($tname)) { // If the cache table not exists, create it
        chado_search_query("SELECT distinct $cols INTO public.$tname FROM " . $this->cacheTable, array(), 'pubic,chado');
        \Drupal::state()->set("chado_search_cache_last_update_" . $tname, $newer);
      } else { // If cache table exists, check if it's up-to-date. If it's not up-to-date, recreate the cache table
        if (\Drupal::state()->get("chado_search_cache_last_update_" . $tname, "") != $lastupdate &&
            \Drupal::state()->get("chado_search_cache_last_update_" . $tname, "") != $newer
            )
        {
          chado_search_query("DROP table $tname");
          chado_search_query("SELECT distinct $cols INTO public.$tname FROM " . $this->cacheTable, array(), 'public,chado');
          \Drupal::state()->set("chado_search_cache_last_update_" . $tname, $newer);
        }
      }
    }
    // Remove the cache table if cache not specified
    else {
      if (chado_search_table_exists($tname)) {
        chado_search_query("DROP table $tname");
        \Drupal::state()->delete("chado_search_cache_last_update_" . $tname);
      }
    }

    $id_label = $id . '_label';
    $title = $this->title;
    $depend_on_id = $this->depend_on_id;
    $multiple = $this->multiple;
    $alsoDependOn = $this->alsoDependOn;
    $width = '';
    if ($this->label_width) {
      $width = "width:" . $this->label_width ."px";
    }
    $size = $this->size;
    $depend_on_element = &find_first_form_element_by_id($form, $depend_on_id);
    // Add Ajax to the depending element
    $selected = $form_state->getValue($depend_on_id);
    // If Clear button is clicked to clear all values
    $triggering_element = &$form_state->getTriggeringElement();
    if ($triggering_element && $triggering_element['#id'] == 'chado_search-id-clear-all-values' && $triggering_element['#type'] == 'button') {
      $selected = NULL;
    }
    $depend_on_element['#ajax'] = array(
      'callback' => 'chado_search_ajax_form_update',
      //'path' => 'chado_search_ajax_callback',
      'wrapper' => "chado_search-filter-$search_name-$id-field",
      'effect' => 'fade'
    );
    if(isset($depend_on_element['#attribute']['update'])) {
      $updates = $depend_on_element['#attribute']['update'];
      if (!is_array($updates)) {
        $updates = array($updates => array('wrapper' => "chado_search-filter-$search_name-$updates-field"));
      }
      $updates[$id] = array('wrapper' => "chado_search-filter-$search_name-$id-field");
      $depend_on_element['#attribute'] = array ('update' => $updates);
    }
    else {
      $depend_on_element['#attribute'] = array ('update' => $id);
    }

    // Also add Ajax to other dependent elements
    foreach ($alsoDependOn AS $did) {
      $did_element = &find_first_form_element_by_id($form, $did);
      $did_element['#ajax'] = array(
        'callback' => 'chado_search_ajax_form_update',
        //'path' => 'chado_search_ajax_callback',
        'wrapper' => "chado_search-filter-$search_name-$id-field",
        'effect' => 'fade'
      );
      if(isset($did_element['#attribute']['update'])) {
        $updates = $did_element['#attribute']['update'];
        if (!is_array($updates)) {
          $updates = array($updates => array('wrapper' => "chado_search-filter-$search_name-$updates-field"));
        }
        $updates[$id] = array('wrapper' => "chado_search-filter-$search_name-$id-field");
        $did_element['#attribute'] = array ('update' => $updates);
      }
      else {
        $did_element['#attribute'] = array ('update' => $id);
      }
    }

    // Add Ajax to reset values on change of another element
    $reset_on_change_id = $this->reset_on_change_id;
    $reset_on_element = &find_first_form_element_by_id($form, $reset_on_change_id);
    if ($reset_on_change_id) {
      $form_state->unsetValue($id);
      $updates = $reset_on_element['#attribute']['update'];
      if (!is_array($updates)) {
        $updates = array($updates => array('wrapper' => "chado_search-filter-$search_name-$updates-field"));
      }
      $updates[$id] = array('wrapper' => "chado_search-filter-$search_name-$id-field");
      $reset_on_element['#attribute'] = array ('update' => $updates);
    }

    $callback = $this->callback;
    $cached_opts = $this->cached_opts;
    $opt = []; 
    if(is_array($cached_opts)) {
      $col_val = $cached_opts[0];
      $col_filter = $cached_opts[1];
      $sort = $cached_opts[2];
      $default_empty = $cached_opts[3];
      $also_depend_filters = $cached_opts[4];
      if($triggering_element &&  key_exists($triggering_element['#name'], $also_depend_filters)) {
        $col_filter = $also_depend_filters[$triggering_element['#name']];
        $selected = $form_state->getValue($triggering_element['#name']);
      }
      if (\Drupal::database()->schema()->tableExists($tname) && (!$default_empty || $selected)) {
        $sql = "SELECT DISTINCT $col_val FROM $tname";
        if (is_array($selected) && count($selected) > 0) {
          if(!key_exists(0, $selected)) {
            $sql .= " WHERE $col_filter IN (:selected[])";
          }
        }
        else {
          if ($selected) {
            $sql .= " WHERE $col_filter = :selected";
          }
        }
        if ($sort) {
          $sql .= " ORDER BY $col_val";
        }
        $results = NULL;
        if (is_array($selected) && count($selected) > 0) {
          if(!key_exists(0, $selected)) {
            $results = \Drupal::database()->query($sql, [':selected[]' => $selected]);
          }
          else {
            $results = \Drupal::database()->query($sql, []);
          }
        }
        else {
          if ($selected) {
            $results = \Drupal::database()->query($sql, [':selected' => $selected]);
          }
          else {
            $results = \Drupal::database()->query($sql, []);
          }
        }
        if ($results != NULL) {
          if (!$multiple) {
            $opt[0] = 'Any';
          }
          while ($obj = $results->fetchObject()) {
            $opt[$obj->$col_val] = $obj->$col_val;
          }
        }
      }
    }
    else {
      $opt = function_exists($callback) ? $callback($selected, $form, $form_state) : [];
      if (!$opt) {
        $opt = [];
       }
    }
    $display = 'display:block';

    $js = '';
    if ($this->search_box) {
      $js = 
      "<script type=\"text/javascript\">
              function search_options_$id (txt) {
                var find = txt.value.toLowerCase();
                var select = document.getElementById('chado_search-filter-$search_name-$id-field');
                var options = select.getElementsByTagName('option');
                for (var i = 0; i < options.length; i ++) {
			     if (options[i].innerHTML.toLowerCase().includes(find)) {
                    options[i].style.display = 'block';
                 }
                 else {
                    options[i].style.display = 'none';
                 }
		        }
              }
           </script><input id=\"chado_search-filter-$search_name-$id-search_box\" class=\"form-control\" type=\"text\" autocomplete=\"off\" onkeyup=\"search_options_$id(this)\">";
    }
    
    // Add Label
    $this->csform->addMarkup(Set::markup()->id($id_label)->text($title));
    $form[$id_label]['#prefix'] = Markup::create(
      "<div id=\"chado_search-filter-$search_name-$id-label\" class=\"chado_search-filter-label form-item\" style=\"$display;$width\">");
    $form[$id_label]['#suffix'] =
      "</div>";
    // Add Select
    if (!is_array($opt) || count($opt) ==0) {
      $opt = array (0 => 'Any');
    }
    if (function_exists($callback)  || is_array($this->cached_opts)) {
      $this->csform->addSelect(Set::select()->id($id)->options($opt)->multiple($multiple)->size($size));
      $form[$id]['#prefix'] =
      Markup::create("<div id=\"chado_search-filter-$search_name-$id-field\" class=\"chado_search-filter-field chado_search-widget\"><div style=\"$display\">$js");
      $form[$id]['#suffix'] =
        "</div></div>";
    }
    else {
      \Drupal::messenger()->addError(t("Fatal Error: DynamicSelectFilter ajax function not implemented"));
    }
  }
}
