<?php

namespace Drupal\chado_search\Commands;

use Drush\Commands\DrushCommands;

/**
 * Drush commands
 */
class ChadoSearchCommands extends DrushCommands {

  /**
   * Reload the settings for the chado_search.
   *
   * @command chado_search:cs-settings-reload
   * @aliases csreload
   * @usage csreload
   *   Reload the settings for the chado_search.
   */
  public function csReload($options = []) {
    // Create MViews
    print "Checking MViews...\n";
    chado_search_create_enabled_mviews();

    // Update Drupal URL aliases
    print "Rebuilding URLs...\n";
    chado_search_rebuild();

    print "ChadoSearch 'settings.conf' reloaded.\n";
  }

  /**
   * Execute the 'chado_search_<search_id>_drush_run' function defined in any search.
   *
   * @command chado_search:cs-run
   * @aliases csrun
   * @usage csrun
   *   Execute the 'chado_search_<search_id>_drush_run' function defined in any search.
   */
  public function csRun($options = []) {
    $searches = chado_search_get_enabled_searches();
    foreach ($searches AS $search) {
      require_once \Drupal::service('extension.list.module')->getPath('chado_search') . '/' . $search['file'];
      $func = 'chado_search_' . $search['id'] . '_drush_run';
      if (function_exists($func)) {
        print "Calling '$func'...\n";
        $func();
      }
    }
    print "Done.\n";
  }

  /**
   * Drop a ChadoSearch MView.
   *
   * @command chado_search:cs-drop-mview
   * @aliases csdmv
   * @options name
   *   The MView name to drop.
   * @usage csdmv --name=[MVIEW]
   *   Drop a ChadoSearch MView
   */
  public function csMviewDrop($options = ['name' => NULL]) {
    $mview = $options['name'];
    if (!$mview) {
      print "Please specify the MView name by using --name option\n";
      return;
    }
    chado_search_drop_chado_mview($mview);
  }

    /**
   * Populate a ChadoSearch MView.
   *
   * @command chado_search:cs-populate-mview
   * @aliases cspmv
   * @options name
   *   The MView name to populate.
   * @usage csdmv --name=[MVIEW]
   *   Populate a ChadoSearch MView
   */
  public function csMviewPopulate($options = ['name' => NULL]) {
    $mview = $options['name'];
    if (!$mview) {
      print "Please specify the MView name by using --name option\n";
      return;
    }
    chado_search_populate_chado_mview($mview);
  }

  /**
   * List all MViews for enabled searches
   *
   * @command chado_search:cs-list-mviews
   * @aliases cslmvs
   * @usage cslmvs
   *   List all MViews for enabled searches
   */
  public function csMviewList($options = []) {
    print "The following are enabled MViews according to the 'settings.conf'\n";
    $mviews = chado_search_get_enabled_mviews();
    foreach($mviews AS $title => $mvs) {
      print "- MViews in '$title'\n";
      foreach ($mvs AS $mv) {
        $time = \Drupal::state()->get('chado_search_time_' . $mv);
        $rows = \Drupal::state()->get('chado_search_rows_' . $mv);
        print "  $mv";
        if ($rows) {
          print " \t ($rows rows)";
        }
        if ($time) {
          $time = date( "Y-m-d h:i:sa", $time);
          print " \t populated on $time";
        }
        print "\n";
      }
      print "\n";
    }
  }

  /**
   * Populate all MViews for enabled searches.
   *
   * @command chado_search:cs-populate-all
   * @aliases cspall
   * @usage cspall
   *   Populate all MViews for enabled searches.
   */
  public function csPopulateAllMviews($options = []) {
    chado_search_populate_enabled_mviews();
  }
  
  /**
   * List all indexes for a table
   *
   * @command chado_search:cs-list-idx
   * @aliases csidxl
   * @usage csidxl --table=chado_search_gene_search --schema=chado
   *   List all indexes for a table
   */
  public function csListIndexes($options = ['table' => NULL, 'schema' => 'chado']) {
    $table = $options['table'];
    $schema = $options['schema'];
    if ($table && $schema) {
      $idxes = chado_search_get_table_indexes($table, $schema);
      $counter = 1;
      foreach ($idxes AS $idx => $def) {
        print $counter . '. ' . $idx . ' => ' . $def . "\n";
        $counter ++;
      }
      if ($counter == 1) {
        print "No index found.\n";
      }
    }
    else {
      print "[Error] Please specify a table with --table option for listing indexes.\n";
    }
  }
  
  /**
   * Create indexes for a table
   *
   * @command chado_search:cs-create-idx
   * @aliases csidxc
   * @usage csidxc --table=chado_search_gene_search --schema=chado --columns=name:uniquename
   *   Create indexes for a table on specified columns. Separate columns with an colon (:) if creating multiple indexes is desired.
   */
  public function csCreateIndexes($options = ['table' => NULL, 'schema' => 'chado', 'columns' => NULL]) {
    $schema = $options['schema'];
    $table = $schema . '.' . $options['table'];
    $columns = explode(':', $options['columns']);
    if(\Drupal::database()->schema()->tableExists($table)) {
        chado_search_create_table_index($table, $columns);
    }
    else {
      print "[Error] Table not found.\n";
    }
  }
  
  /**
   * Drop all indexes for a table
   *
   * @command chado_search:cs-drop-all-idx
   * @aliases csidxda
   * @usage csidxda --table=chado_search_gene_search --schema=chado
   *   Drop all indexes for a table.
   */
  public function csDropIndexes($options = ['table' => NULL, 'schema' => 'chado']) {
    $table = $options['table'];
    $schema = $options['schema'];
    if ($table && $schema) {
      $idxes = chado_search_get_table_indexes($table, $schema);
      $dropped = chado_search_drop_all_table_indexes($table, $schema);
      if (count($dropped) == 0) {
        print "No index found.\n";
      }
      else {
        print count($dropped) . " index(es) dropped.\n";
      }
    }
    else {
      print "[Error] Please specify a table name with --table option.\n";
    }
  }
  
  /**
   * Drop specified index
   *
   * @command chado_search:cs-drop-idx
   * @aliases csidxd
   * @usage csidxd --index=cs_chado_search_marker_search_marker_name_marker_uniquename
   *   Drop specified index
   */
  public function csDropIndex($options = ['index' => NULL, 'schema' => 'chado']) {
    $index = $options['index'];
    $schema = $options['schema'];
    if ($index) {
      $drop = chado_search_drop_table_index($index, $schema);
      if ($drop) {
        print "Index '$index' dropped.\n";
      }
      else {
        print "Failed to drop $index.\n";
      }
    }
    else {
      print "[Error] Please specify an index name with --index option.\n";
    }
  }
  
  /**
   * Partition an MView by specified column
   *
   * @command chado_search:cs-mv-part
   * @aliases csmvpart
   * @usage csmvpart --name=chado_search_gene_search --column=organism_id --num_parts=5
   *   Partition an MView by specified column.
   */
  public function csMvPart($options = ['name' => NULL, 'column' => NULL, 'num_parts' => NULL]) {
    $name = $options['name'];
    $col = $options['column'];
    $num = $options['num_parts'];
    if ($name && $col && is_numeric($num)) {
      // Check if it's a Chado Search/Tripal MegaSearch MView
      $is_cs_mview = FALSE;
      $enabledSearch = chado_search_get_enabled_searches(FALSE);
      foreach ($enabledSearch AS $search) {
        $mview = isset($search['mview_name']) ? $search['mview_name'] : NULL ;
        if ($mview && \Drupal::database()->schema()->tableExists('chado.' . $mview) && $mview == $name) {
          $is_cs_mview = TRUE;
          break;
        }
      }
      if ($is_cs_mview || preg_match('/^chado_search_/', $name) || preg_match('/^tripal_megasearch_/', $name)) {
        $sql =
        "SELECT
          'CREATE TABLE ' || relname || E'\n(\n' ||
            array_to_string(
              array_agg(
                '    ' || column_name || ' ' ||  type || ' '|| not_null
              )
              , E',\n'
            ) || E'\n)\n'
          FROM
          (
            SELECT
              c.relname, a.attname AS column_name,
              pg_catalog.format_type(a.atttypid, a.atttypmod) as type,
              case
                when a.attnotnull
              then 'NOT NULL'
              else 'NULL'
              END as not_null
            FROM pg_class c,
             pg_attribute a,
             pg_type t
             WHERE c.relname = '$name'
             AND a.attnum > 0
             AND a.attrelid = c.oid
             AND a.atttypid = t.oid
           ORDER BY a.attnum
          ) T
          GROUP BY relname";
        $sql_create = \Drupal::database()->query($sql)->fetchField();
        if ($sql_create && \Drupal::database()->schema()->tableExists('chado.' . $name) && \Drupal::database()->schema()->fieldExists('chado.' . $name, $col)) {
          $drop_mview = readline("The existing MView '$name' will be dropped and all its data will be lost. Continue? (yes/no) ");
          if ($drop_mview == 'y' || $drop_mview == 'yes') {
            $sql = "DROP TABLE IF EXISTS $name";
            chado_search_query($sql, [], 'chado', FALSE);
            chado_search_query ($sql_create . "PARTITION BY HASH ($col)", [], 'chado', FALSE);
            $parts = [];
            for ($i = 0; $i < $num; $i ++) {
              $tpart = $name .  '_p' . ($i + 1);
              chado_search_query ("DROP TABLE IF EXISTS $tpart", [], 'chado', FALSE);
              chado_search_query ("CREATE TABLE  $tpart PARTITION OF $name FOR VALUES WITH (modulus $num" . ", remainder $i" . ')', [], 'chado', FALSE);
              $parts [] = $tpart;
            }
            \Drupal::state()->set('chado_search_parted_mview_' . $name, $parts);
            print "Partition table finished. You can now proceed to populate the MView.\n";
          }
        }
        else {
          print "[Error] Specified MView or column not available for partition.\n";
        }
      }
      else {
        print "[Error] Only Chado Search/Tripal MegaSearch Mviews are supported for partition.\n";
      }
    }
    else {
      print "[Error] Please specify MView name, a column, and a number for partition.\n";
    }
  }
  
  /**
   * Drop partitioned MView including all its partitions.
   *
   * @command chado_search:cs-part-mv-drop
   * @aliases cspartmvdrop
   * @usage cspartmvdrop --name=chado_search_gene_search
   *   Drop partitioned MView including all its partitions.
   */
  function csPartMvDrop($options = ['name' => NULL]) {
    $name = $options['name'];
    $parts = \Drupal::state()->get('chado_search_parted_mview_' . $name);
    if ($parts && count($parts) > 0) {
      foreach($parts AS $part) {
        $sql = "DROP TABLE IF EXISTS $part";
        chado_search_query($sql, [], 'chado', FALSE);
        chado_search_query("DROP TABLE IF EXISTS $name", [], 'chado', FALSE);
        \Drupal::state()->delete('chado_search_parted_mview_' . $name);
      }
      print "All table partitions have been dropped. You may want to recreate the default MView by running 'drush csreload'.\n";
    }
    else {
      print "[Error] MView not found or not a partitioned MView.\n";
    }
  }
}