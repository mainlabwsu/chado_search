<?php
namespace Drupal\chado_search\Form;

use Symfony\Component\HttpFoundation\RedirectResponse;

use Drupal\Core\Render\Markup;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\HTML;

use Drupal\chado_search\Csform\ChadoSearchForm;

class MainForm extends FormBase {

    /**
     * {@inheritdoc}
     */
     public function getFormId() {
        return 'chado_search_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $search_id = NULL, $show_result = NULL) {

      // Access control
      $access = \Drupal::state()->get('chado_search_access', array());
      if (in_array($search_id, $access)) {
        $access_roles = \Drupal::state()->get('chado_search_access_roles', array());
        $has_role = FALSE;
        $user = \Drupal::currentUser();
        $myRoles = $user->getRoles();
        foreach ($access_roles AS $rid) {
            if (in_array($rid, $myRoles)) {
              $has_role = TRUE;
                break;
            }
        }
        if (!$has_role) {
          return new RedirectResponse(\Drupal\Core\Url::fromRoute('chado_search.access_denied')->toString());
        }
      }

      // Set title
      $title = chado_search_get_setting_by_id($search_id, 'title');
      $route = \Drupal::routeMatch()->getCurrentRouteMatch()->getRouteObject();
      $route->setDefault('_title', $title);

      // Create new ChadoSearch form
      $path = chado_search_get_setting_by_id($search_id, 'path');
      $num_per_page = chado_search_get_setting_by_id($search_id, 'num_per_page');
      $num_per_page = $num_per_page ? $num_per_page : 20;

      $sbf = new ChadoSearchForm($form_state, $search_id, $path);
      $func = 'chado_search_' . $search_id . '_form';
      if (function_exists($func)) {
        $csform = $func($sbf);
        $form = $csform->getForm();
      }
      // Store form settings for use when validating/submitting the form
      $form['#search_id'] = $search_id;
      $form['#search_url'] = $path;
      $form['#number_per_page'] = $num_per_page;

      $storage = &$form_state->getStorage();
      if (isset($form['#custom_output-group_selection'])) {
        $storage['#custom_output-group_selection'] = $form['#custom_output-group_selection'];
      }

      if (isset($form['#custom_output-max_columns'])) {
        $storage['#custom_output-max_columns'] = $form['#custom_output-max_columns'];
      }

      $allowed = chado_search_get_setting_by_id($search_id, 'summary_allowed');
      if (($show_result == 'summary' || $show_result == 'list') && $allowed) {
        $title = chado_search_get_setting_by_id($search_id, 'summary_title');
        if ($title) {
            $route->setDefault('_title', $title);
        }
        $storage['#show_all_results'] = TRUE;
        $storage['#show_all_results_scroll'] = FALSE;
        if (key_exists('scroll', $_GET)) {
          $storage['#show_all_results_scroll'] = TRUE;
        }
        $hasResult = $search_id . "-result";
        $noResult = $search_id . "-no-result";
        if ($show_result == 'summary') {
          $form['wait']['#prefix'] = Markup::create(
            "<style type=\"text/css\">
                body {display: none;}
             </style>
             <script type=\"text/javascript\">
               (function ($) {;
                  $(document).ready(function(){
                    var bad_url = location.toString().includes('%27') || location.toString().includes('%3B') || location.toString().includes(';');
                    if (bad_url) {
                      $('body').show();
                    }
                    else {
                      $('.chado_search-widget').hide();
                      if(!document.getElementById('$hasResult') && !document.getElementById('$noResult')){
                        document.getElementById('chado_search-id-submit').click();
                      }
                    }
                  })
               })(jQuery);
              </script>");
        } else if ($show_result == 'list') {
          $form['wait']['#prefix'] = Markup::create(
            "<style type=\"text/css\">
                body {display: none;}
              </style>
              <script type=\"text/javascript\">
                (function ($) {;
                   $(document).ready(function(){
                    var bad_url = location.toString().includes('%27') || location.toString().includes('%3B') || location.toString().includes(';');
                    if (bad_url) {
                      $('body').show();
                    }
                    else {
                      if(!document.getElementById('$hasResult') && !document.getElementById('$noResult')){
                        document.getElementById('chado_search-id-submit').click();
                      }
                    }
                   })
                 })(jQuery);
               </script>");
               $build_info = $form_state->getBuildInfo();
          unset ($build_info['args'][3]); // unset 'list' so the page will not be hidden when resubmitting the form
        }
      }
      $form['#prefix'] = '<div id=chado_search_form>';
      $form['#suffix'] = '</div>';
      
      $default_fieldset = \Drupal::state()->get('chado_search_fieldset_behavior', 'default');
      $libs = $form['#attached']['library'];
      if ($default_fieldset == 'accordion') {
        $form['#attached']['library'] = array_merge($libs, ['chado_search/chado_search_accordion']);
      } else if ($default_fieldset == 'open_first') {
        $form['#attached']['library'] = array_merge($libs, ['chado_search/chado_search_fiedset_open_first']);
      }
      return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        // To allow getting values from $_GET
        $inputs = $form_state->getUserInput();
        $input_keys = array_keys($inputs);
        foreach ($input_keys AS $k) {
          if ($k != 'form_build_id' && $k != 'form_id' && $k != 'form_token') {
            if (key_exists($k, $_GET) && !$form_state->getValue($k)) {
              $form_state->setValue($k, HTML::escape($_GET[$k]));
            }
          }
        }
        $func = 'chado_search_' . $form['#search_id'] . '_form_validate';
        if (function_exists($func)) {
          try {
            $func($form, $form_state);
          } catch (\Exception $e) {
            \Drupal::messenger()->addError($e->getMessage());
          }
        }
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {
        // To allow getting values from $_GET
        $inputs = $form_state->getUserInput();
        $input_keys = array_keys($inputs);
        foreach ($input_keys AS $k) {
          if ($k != 'form_build_id' && $k != 'form_id' && $k != 'form_token') {
            if (key_exists($k, $_GET) && !$form_state->getValue($k)) {
              $form_state->setValue($k, HTML::escape($_GET[$k]));
            }
          }
        }
        $func = 'chado_search_' . $form['#search_id'] . '_form_submit';
        if (function_exists($func)) {
          //$start = microtime();
          try {
            $func($form, $form_state);
          } catch (\Exception $e) {
            \Drupal::messenger()->addError($e->getMessage());
          } catch (\Error $e) {
            \Drupal::messenger()->addError($e->getMessage());
          }
          //$stop= microtime();
          //dpm((($stop - $start) * 1000) . ' milliseconds');
        }
    }
}
