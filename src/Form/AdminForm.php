<?php
namespace Drupal\chado_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

class AdminForm extends FormBase {

    /**
     * {@inheritdoc}
     */
     public function getFormId() {
        return 'chado_search_admin_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state, $search_id = NULL) {

        $form = array();
        $searches = chado_search_get_all_searches (FALSE);
        $access = \Drupal::state()->get('chado_search_access', array());
        $headers = array('Title', 'ID', 'URL', 'PHP', 'Enabled', 'Action', 'Access');
        $rows = array();
        foreach ($searches AS $search) {
            $rows[] = array(
                $search['title'],
                $search['id'],
                $search['enabled'] ?
                    Link::fromTextAndUrl($search['path'], Url::fromRoute('chado_search.form', array('search_id' => $search['id'])))->toString() :
                    $search['path'],
                $search['file'],
                $search['enabled'] ? 'Yes' : 'No',
                $search['enabled'] ?
                    Link::fromTextAndUrl('Disable', Url::fromRoute('chado_search.admin_action', array('search_id' => $search['id'], 'action' => 'disable')))->toString() :
                    Link::fromTextAndUrl('Enable', Url::fromRoute('chado_search.admin_action', array('search_id' => $search['id'], 'action' => 'enable')))->toString(),
                key_exists($search['id'], $access) ?
                    Link::fromTextAndUrl('Restricted by Roles', Url::fromRoute('chado_search.admin_action', array('search_id' => $search['id'], 'action' => 'allow')))->toString() :
                    Link::fromTextAndUrl('All Granted', Url::fromRoute('chado_search.admin_action', array('search_id' => $search['id'], 'action' => 'restrict')))->toString(),
            );
        }
        if (count($rows) == 0) {
            $rows[] = array('No search available.', array('colspan' => 6));
        }

        $form['instructions'] = array(
            '#type' => 'item',
            '#markup' => 'List of configured search interfaces'
        );
        $form['table'] = array(
            '#type' => 'table',
            '#header' => $headers,
            '#rows' => $rows,
            '#attributes' => array(),
            '#sticky' => FALSE,
            '#caption' => '',
            '#colgroups' => array(),
            '#empty' => '',
        );

        $access_roles = \Drupal::state()->get('chado_search_access_roles', array());
        if (function_exists('user_roles')) {
          $user_roles = user_roles ();
        }
        else {
          $user_roles = \Drupal\user\Entity\Role::loadMultiple();
        }
        $roles = array();
        foreach ($user_roles AS $k => $r) {
            $roles [$k] = $k;
        }
        $form['restriction'] = array(
            '#type' => 'details',
            '#title' => 'Restrict Access',
            '#description' => "Effective only when access is switched from 'All Granted' to 'Restricted by Roles' in above search listing table",
            '#open' => FALSE
        );
        $form['restriction']['access'] = array(
        '#type' => 'checkboxes',
        '#title' => 'Roles',
        '#description' => "Allow access only for the selected roles.",
        '#options' => $roles,
        '#default_value' => $access_roles
        );
        $access_denied_message = \Drupal::state()->get('chado_search_access_denied_message', 'You are not authorized to use this seach function.');
        $form['restriction']['access_denied_message'] = array(
            '#type' => 'textarea',
            '#title' => 'Access Denied Message',
            '#description' => 'Message shown to the user when access is denied.',
            '#default_value' => $access_denied_message,
            '#size' => 150
        );
        $form['upload'] = array(
            '#type' => 'details',
            '#title' => 'Upload',
            '#open' => FALSE
        );
        $default_uplimit = \Drupal::state()->get('chado_search_upload_limit', '0');
        $form['upload']['upload_limit'] = array(
          '#type' => 'textfield',
          '#title' => 'Upload Limit',
          '#description' => 'The line limit for each upload file. Limit to read only this many lines in the uploaded file. (Default = 0. No limit.)',
          '#default_value' => $default_uplimit
        );
        $form['download'] = array(
            '#type' => 'details',
            '#title' => 'Download',
            '#open' => FALSE
        );
        $default_limit = \Drupal::state()->get('chado_search_download_limit', '0');
        $form['download']['download_limit'] = array(
          '#type' => 'textfield',
          '#title' => 'Download Limit',
          '#description' => 'The download limit on each query. Increase this number may add to the server load. Set to 0 for unlimited download. (Default = 0)',
          '#default_value' => $default_limit
        );
        $default_fasta_limit = \Drupal::state()->get('chado_search_fasta_download_limit', '0');
        $form['download']['fasta_download_limit'] = array(
          '#type' => 'textfield',
          '#title' => 'FASTA Download Limit',
          '#description' => 'The limit for downloading FASTA records on each query. Increase this number may add to the server load.  Set to 0 for unlimited FASTA records. (Default = 0)',
          '#default_value' => $default_fasta_limit
        );
        $download_file_header = \Drupal::state()->get('chado_search_download_file_header', '');
        $form['download']['download_file_header'] = array(
            '#type' => 'textarea',
            '#title' => 'Download File Header',
            '#description' => 'Add a header message to all download files. Default: no header message.',
            '#default_value' => $download_file_header,
            '#size' => 150
        );

        $form['sequence_retrieval'] = array(
            '#type' => 'details',
            '#title' => 'Sequence Retrieval',
            '#collapsible' => TRUE
        );
        $limit_sequence_retrieval = \Drupal::state()->get('chado_search_sequence_retrieval_limit', 50000);
        $limit_sequence_retrieval_base_digits = \Drupal::state()->get('chado_search_sequence_retrieval_base_digits', 4);
        $form['sequence_retrieval']['sequence_retrieval_limit'] = array(
            '#type' => 'textfield',
            '#title' => 'Sequence Retrieval Record Limit',
            '#description' => 'Limit for a Sequence Retrieval job to be created. If a search returns more results than this limit, Sequence Retrieval will be disabled to prevent server overload. (default=50000)',
            '#default_value' => $limit_sequence_retrieval,
            '#size' => 10,
            '#maxlength' => 10
        );
        $form['sequence_retrieval']['sequence_retrieval_base_digits'] = array(
            '#type' => 'textfield',
            '#title' => 'Sequence Retrieval Upstream/Downstream Bases Limit',
            '#description' => 'Apply a digit limit for the upstream/downstream bases. By default, it is set to 4 digits which allow 9999 bases at most. (default=4)',
            '#default_value' => $limit_sequence_retrieval_base_digits,
            '#size' => 10,
            '#maxlength' => 10
        );

        $hb_search_enabled = chado_search_get_setting_by_id ('haplotype_block_search', 'enabled');
        if($hb_search_enabled && chado_search_table_exists('chado_search_haplotype_block_marker_location', 'chado') && chado_search_table_exists('chado_search_haplotype_block_marker_position', 'chado')) {
            $sql = "SELECT DISTINCT project_id, project FROM chado_search_haplotype_block_search";
            $result = chado_search_query($sql, array(), 'chado');
            $projects = array();
            while ($obj = $result->fetchObject()) {
                $projects[$obj->project_id] = $obj->project;
            }
            $sql = "SELECT DISTINCT genome_analysis_id, genome FROM chado_search_haplotype_block_marker_location";
            $result = chado_search_query($sql, array(), 'chado');
            $genomes = array();
            while ($obj = $result->fetchObject()) {
                $genomes[$obj->genome_analysis_id] = $obj->genome;
            }
            $sql = "SELECT DISTINCT featuremap_id, map FROM chado_search_haplotype_block_marker_position";
            $result = chado_search_query($sql, array(), 'chado');
            $maps = array();
            while ($obj = $result->fetchObject()) {
                $maps[$obj->featuremap_id] = $obj->map;
            }
            $form['haplotype_block_search'] = array(
                '#title' => t('Haplotype Block Search Settings'),
                '#description' => t('The following are settings specific to the Haplotype Block Search'),
                '#type' => 'details',
                '#open' => FALSE
            );
            $default_genomes = \Drupal::state()->get('chado_search_haplotype_search_genomes', array());
            $default_maps = \Drupal::state()->get('chado_search_haplotype_search_maps', array());

            foreach ($projects AS $project_id => $project) {
                $form['haplotype_block_search'][$project_id] = array(
                    '#title' => $project,
                    '#type' => 'details',
                    '#open' => FALSE
                );
                $form['haplotype_block_search'][$project_id]['genomes_' . $project_id] = array(
                    '#title' => t('Genomes for showing marker genome location'),
                    '#description' => t('Select genome(s) for showing marker genome location. Only locations on selected genomes will be displayed. If a marker is on more than one selected genome, only one of them will be shown.'),
                    '#type' => 'checkboxes',
                    '#options' => $genomes,
                  '#default_value' => isset($default_genomes[$project_id]) ? $default_genomes[$project_id] : []
                );
                $form['haplotype_block_search'][$project_id]['maps_' . $project_id] = array(
                    '#title' => t('Maps for showing marker map position'),
                    '#description' => t('Select map(s) for showing marker map position. Only positions on selected maps will be displayed. If a marker is on more than one selected map, only one of them will be shown.'),
                    '#type' => 'checkboxes',
                    '#options' => $maps,
                  '#default_value' => isset($default_maps[$project_id]) ? $default_maps[$project_id] : []
                );
            }
        }
        $form['ui'] = array(
          '#type' => 'details',
          '#title' => 'User Interface',
          '#collapsible' => TRUE
        );
        $default_fieldset = \Drupal::state()->get('chado_search_fieldset_behavior', 'default');
        $form['ui']['fieldset_behavior'] = array(
          '#type' => 'radios',
          '#title' => 'Fieldset Behavior',
          '#description' => 'Chado Search Fieldset behavior',
          '#options' => ['default' => 'Default. Obey each fieldset\'s default setting.', 'accordion' => 'Accordion. Only one of the fieldsets will be opened at a time while others stay closed.', 'open_first' => 'Open First. Only the first fieldset will be opened while the other will be closed by default.'],
          '#default_value' => $default_fieldset
        );
        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => 'Save'
        );
        return $form;
    }

    /**
     * {@inheritdoc}
     */
     public function validateForm(array &$form, FormStateInterface $form_state) {
        $values = $form_state->getValues();
        
        $limit_download = $values['download_limit'];
        if (!is_numeric($limit_download)) {
          $form_state->setErrorByName('download_limit', 'Please input a number');
        }
        
        $limit_fasta = $values['fasta_download_limit'];
        if (!is_numeric($limit_fasta)) {
          $form_state->setErrorByName('fasta_download_limit', 'Please input a number');
        }
        
        $limit_sequence_retrieval = $values['sequence_retrieval_limit'];
        if (!is_numeric($limit_sequence_retrieval)) {
            $form_state->setErrorByName('sequence_retrieval_limit', 'Please input a number');
        }

        $limit_sequence_retrieval_base_digits = $values['sequence_retrieval_base_digits'];
        if (!is_numeric($limit_sequence_retrieval_base_digits)) {
            $form_state->setErrorByName('sequence_retrieval_base_digits', 'Please input a number');
        }
     }

     /**
     * {@inheritdoc}
     */
     public function submitForm(array &$form, FormStateInterface $form_state) {
        $values = $form_state->getValues();
        $access = $values['access'];
        $access_roles = array();
        foreach ($access AS $k => $v) {
            if ($v) {
                $access_roles[$k] = $v;
            }
        }
        \Drupal::state()->set('chado_search_access_roles', $access_roles);
        $access_denied_message = $values['access_denied_message'];
        \Drupal::state()->set('chado_search_access_denied_message', $access_denied_message);

        $limit_upload = (int) $values['upload_limit'];
        \Drupal::state()->set('chado_search_upload_limit', trim($limit_upload));

        $limit_download = (int) $values['download_limit'];
        \Drupal::state()->set('chado_search_download_limit', trim($limit_download));
        
        $limit_fasta = (int) $values['fasta_download_limit'];
        \Drupal::state()->set('chado_search_fasta_download_limit', trim($limit_fasta));
        
        $download_file_header = $values['download_file_header'];
        \Drupal::state()->set('chado_search_download_file_header', trim($download_file_header));

        $limit_sequence_retrieval = (int) $values['sequence_retrieval_limit'];
        \Drupal::state()->set('chado_search_sequence_retrieval_limit', trim($limit_sequence_retrieval));

        $limit_sequence_retrieval_base_digits = (int) $values['sequence_retrieval_base_digits'];
        \Drupal::state()->set('chado_search_sequence_retrieval_base_digits', trim($limit_sequence_retrieval_base_digits));

        $var_genome_ids = array();
        $var_map_ids = array();

        foreach ($values AS $key => $value) {
            if (preg_match('/genomes_/', $key)) {
                $pkey = explode('_', $key);
                foreach ($value AS $k => $v) {
                  if ($v > 0) {
                    $var_genome_ids [$pkey[1]][] = $k;
                  }
                }
            }
            else if (preg_match('/maps_/', $key)) {
                foreach ($value AS $k => $v) {
                  if ($v > 0) {
                      $var_map_ids [$pkey[1]][] = $k;
                  }
                }
            }
        }
        \Drupal::state()->set('chado_search_haplotype_search_genomes', $var_genome_ids);
        \Drupal::state()->set('chado_search_haplotype_search_maps', $var_map_ids);
        \Drupal::state()->set('chado_search_fieldset_behavior', $values['fieldset_behavior']);
    }
}