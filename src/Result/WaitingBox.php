<?php

namespace Drupal\chado_search\Result;

class WaitingBox extends Source {
  
  public function __construct($search_id) {
    $html = $this->htmlWaitingBox($search_id);
    $this->src = $html; 
  }
  
  private function htmlWaitingBox($search_id) {
    $waitingBox =
      "<div id=\"chado_search-$search_id-waiting-box-overlay\" class=\"chado_search-waiting-overlay\">
        </div>
        <div id=\"chado_search-$search_id-waiting-box-message\" class=\"chado_search-waiting-box-message chado_search-$search_id-waiting-box\">
          <h3>Please wait... <div id=\"chado_search-$search_id-waiting-box-progress\" class=\"chado_search-waiting-box-progress\"></div></h3>
        </div>";
    return $waitingBox;
  }
}