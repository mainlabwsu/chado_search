<?php

namespace Drupal\chado_search\Result;

use Drupal\chado_search\Core\SessionVar;

require_once 'Source.php';

class SequenceRetrieval extends Source {

  public $search_id;
  public $path;

  public function __construct($search_id, $path) {
    $this->search_id = $search_id;
    $this->path = $path;
    $js = $this->jsSequenceRetrieval();
    $this->src = $js;
  }

  private function jsSequenceRetrieval() {
    $search_id = $this->search_id;
    $path = $this->path;
    $progress_path = '';
    $path = "chado_search/$search_id/sequence_retrieval";
    $progress_path = "chado_search/$search_id/download/progress";
    $dpost = "form_build_id=" . $_POST['form_build_id'];
    global $base_url;
    $js =
      "<script type=\"text/javascript\">
          (function ($) {
            function " . $search_id . "_sequence_retrieval () {
              var link = '$base_url';
              link += '/$path';
              $('.chado_search-$search_id-waiting-box').show();
              $('.chado_search-waiting-overlay').show();
              var check_progress = setInterval(function(){
                // Check the progress
                $.ajax({
                  url: '$base_url' + '/' + '$progress_path',
                  dataType: 'json',
                  success: function(data){
                    $('#chado_search-$search_id-waiting-box-progress').show();
                    $('#chado_search-$search_id-waiting-box-progress').text(data.progress + ' %');
                  }
                });
              }, 2000);
              $.ajax({
                url: link,
                data: '$dpost',
                dataType: 'json',
                type: 'POST',
                success: function(data){
                  window.location = data.path;
                  $('.chado_search-$search_id-waiting-box').hide();
                  $('#chado_search-$search_id-waiting-box-progress').text('0 %');
                  $('#chado_search-$search_id-waiting-box-progress').hide();
                  clearInterval(check_progress);
                  $('.chado_search-waiting-overlay').hide();
                }
              });
            }
            window." . $search_id . "_sequence_retrieval = " . $search_id . "_sequence_retrieval;
          })(jQuery);
        </script>";
    $js .=
      "<div id=\"$search_id-sequence_retrieval-download\" class=\"chado_search-download-links\">
          <a href=\"javascript:void(0)\" onClick=\"" . $search_id . "_sequence_retrieval();return false;\">
            Sequence Retrieval
          </a>
        </div>";
    return $js;
  }

  public function createSequenceRetrieval () {

    $search_id = $this->search_id;
    $path = $this->path;

    ini_set('max_execution_time', 6000);
    $sql = SessionVar::getSessionVar($search_id, 'sql');
    if (!$sql) {
      return array('path' => "/$path");
    }

    $sequence_retrieval = SessionVar::getSessionVar($search_id, 'sequence-retrieval');
    $feature_id = $sequence_retrieval['feature_id'];
    $name = $sequence_retrieval['name'];
    $landmark_id = $sequence_retrieval['landmark_id'];
    $fmin = $sequence_retrieval['fmin'];
    $fmax = $sequence_retrieval['fmax'];
    $upstream = $sequence_retrieval['upstream'];
    $downstream = $sequence_retrieval['downstream'];
    $distinct = $sequence_retrieval['distinct'];
    $fmin_coordinate = $sequence_retrieval['fmin_coordinate'];
    $col_strand = $sequence_retrieval['strand'];
    $append1 = '';
    $append2 = '';
    if ($col_strand) {
      $append1 = ", max($col_strand) AS $col_strand";
      $append2 = ", $col_strand";
    }

    $sid = 's_' .session_id();
    $file = $search_id . '_sequence_retrieval.fa.gz';
    $dir = 'sites/default/files/chado_search/' . $sid;
    if (!file_exists($dir)) {
      mkdir ($dir, 0777);
    }
    $path = $dir . "/" . $file;
    $handle = gzopen($path, 'w9');
    $csql = "SELECT count(*) FROM (SELECT $distinct FROM ($sql) T WHERE $landmark_id IS NOT NULL GROUP BY $distinct) C";
    if (!$distinct) {
      $csql = "SELECT count(*) FROM ($sql) T WHERE $landmark_id IS NOT NULL";
    }
    $total_items = chado_search_query($csql, array(), 'chado')->fetchField();

    $fsql = "SELECT max($feature_id) AS $feature_id, max($name) AS $name, max($landmark_id) AS $landmark_id, max($fmin) AS $fmin, max($fmax) AS $fmax $append1 FROM ($sql) T WHERE $landmark_id IS NOT NULL GROUP BY $distinct";
    if (!$distinct) {
      $fsql = "SELECT $feature_id, $name, $landmark_id, $fmin, $fmax $append2 FROM ($sql) T WHERE $landmark_id IS NOT NULL";
    }

    // If user selected rows using the checkbox in the results table
    $result = NULL;
    $checkboxFilter = SessionVar::getSessionVar($search_id, 'checkbox-filter');
    $collectedData = SessionVar::getSessionVar($search_id, 'checkbox-filter-data');
    if ($checkboxFilter && is_array($collectedData) && count($collectedData) > 0) {
      $fsql = "SELECT * FROM ($fsql) T WHERE $checkboxFilter IN (:collection[])";
      $result = chado_search_query($fsql, [':collection[]' => $collectedData], 'chado');
      $total_items = count($collectedData);
    }
    else {
      $result = chado_search_query($fsql, array(), 'chado');
    }

    $progress_var = 'chado_search-'. session_id() . '-' . $search_id . '-download-progress';
    $progress = 0;
    $counter = 1;
    $num_seqs = 0;
    while ($feature = $result->fetchObject()) {
      $current = round ($counter / $total_items * 100);
      if ($current != $progress) {
        $progress = $current;
        \Drupal::state()->set($progress_var, "$counter processed. $progress");
      }
      // Get sequence aligning information
      $seq_fid = $feature->$feature_id;
      $seq_name = $feature->$name;
      $seq_landmark_id = $feature->$landmark_id;
      $seq_fmin = $feature->fmin;
      $seq_fmax = $feature->fmax;
      $start = $feature->fmin - $upstream - $fmin_coordinate + 1;
      $stop = $feature->fmax + $downstream;
      $length = $stop - $start + 1;
      $strsql = $col_strand . ',';
      if (!$col_strand) {
        $strsql = "(SELECT strand FROM chado.featureloc WHERE feature_id = $seq_fid AND srcfeature_id = $seq_landmark_id AND fmin = ($seq_fmin - $fmin_coordinate) AND fmax = $seq_fmax LIMIT 1) AS strand,";
      }
      if ($start > 0 && $length > 0) {
        // Seq SQL
        $ssql = "
          SELECT
            $strsql
            uniquename,
            substring(residues, $start, $length) AS residues
          FROM chado.feature
          WHERE feature_id = $seq_landmark_id";
        $obj = chado_search_query($ssql)->fetchObject();
        $landmark = $obj->uniquename;
        $residues = $obj->residues;
        $strand = $obj->strand;
        $strandness = '+';
        if ($strand == '-1') {
          $residues = chado_search_reverse_complement($residues);
          $strandness = '-';
        }

        // Write ID
        fwrite($handle, '>' . $seq_name . " ($landmark:$start..$stop $strandness)\n");
        // Write sequences
        fwrite($handle, wordwrap($residues, 80, "\n", TRUE) . "\n");
        // Debug
        //fwrite($handle, $ssql . "\n");
        $num_seqs ++;
      }
      $counter ++;
    }
    if ($num_seqs == 0) {
      fwrite($handle, "Sequences not available.\n");
    }

    gzclose($handle);
    chmod($path, 0777);
    $url = "/sites/default/files/chado_search/$sid/$file?" . time();
    // Reset progress bar
    \Drupal::state()->delete($progress_var);
    return array ('path' => $url);
  }
}