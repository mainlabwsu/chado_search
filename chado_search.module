<?php

use Drupal\chado_search\Core\ChadoSearch;

require_once 'api/chado_search.api.php';

// Require callback files
$enabledSearch = chado_search_get_enabled_searches ();
foreach ($enabledSearch as $search) {
  $file = key_exists('file', $search) ? $search ['file'] : NULL;
  $mod_path = \Drupal::service('extension.list.module')->getPath('chado_search');
  if ($file && file_exists($mod_path . '/'. $file)) {
    require_once ($mod_path . '/'. $file);
  }
  else {
    \Drupal::messenger()->addError($file . ' not exists. Please check your settings.conf');
  }
}


// Executed only after cache is cleared
function chado_search_rebuild() {
  // create URL alias for the path in settings.conf
  $searches = chado_search_get_all_searches ();
  $entity = \Drupal::entityTypeManager()->getStorage('path_alias');
  foreach ($searches as $search) {
    $search_id = key_exists('id', $search) ? $search ['id'] : NULL;
    $enabled = key_exists('enabled', $search) ? $search ['enabled'] : 0;
    $summary_allowed = key_exists('summary_allowed', $search) ? $search ['summary_allowed'] : 0;
    $path = key_exists('path', $search) ? $search ['path'] : NULL;
    // Recreate URL alias for enabled searches
    if ($enabled) {
      if ($path) {        
        // Search path
        $old_aliases = $entity->getQuery()->accessCheck(TRUE)->condition('path', '/chado_search/' . $search_id)->execute();
        $entity->delete($entity->loadMultiple($old_aliases));
        $path_alias = $entity->create([
          'path' => '/chado_search/' . $search_id,
          'alias' => '/' . $path,
        ]);
        $path_alias->save();

        if ($summary_allowed) {
          // Search result list
          $old_aliases = $entity->getQuery()->accessCheck(TRUE)->condition('path', '/chado_search/' . $search_id . '/list')->execute();
          $entity->delete($entity->loadMultiple($old_aliases));
          $path_alias = $entity->create([
            'path' => '/chado_search/' . $search_id . '/list',
            'alias' => '/' . $path . '/list',
          ]);
          $path_alias->save();
  
          // Search result summary
          $old_aliases = $entity->getQuery()->accessCheck(TRUE)->condition('path', '/chado_search/' . $search_id . '/summary')->execute();
          $entity->delete($entity->loadMultiple($old_aliases));
          $path_alias = $entity->create([
            'path' => '/chado_search/' . $search_id . '/summary',
            'alias' => '/' . $path . '/summary',
          ]);
          $path_alias->save();
        }
      }
    }
    else {
      // Remove path alias for disabled searches
      if ($path) {
        // Search path
        $old_aliases = $entity->getQuery()->accessCheck(TRUE)->condition('path', '/chado_search/' . $search_id)->execute();
        $entity->delete($entity->loadMultiple($old_aliases));

        // Search result list
        $old_aliases = $entity->getQuery()->accessCheck(TRUE)->condition('path', '/chado_search/' . $search_id . '/list')->execute();
        $entity->delete($entity->loadMultiple($old_aliases));

        // Search result summary
        $old_aliases = $entity->getQuery()->accessCheck(TRUE)->condition('path', '/chado_search/' . $search_id . '/summary')->execute();
        $entity->delete($entity->loadMultiple($old_aliases));

      }
    }
  }
}
